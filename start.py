#! /usr/bin/env python3

import os
import imp

print('Checking and installing necessary components to run seqplot.')

install_dict = {'ginga': 'git+https://github.com/ejeschke/ginga',
                'photutils': 'photutils',
                'PyQt5': 'pyqt5',
                'skimage': 'scikit-image',
                'watchdog': 'watchdog'}

for key in install_dict.keys():
    try:
        imp.find_module(key)
    except ImportError:
        print(f'---> could not import {key}: installing')
        os.system(f'pip3 install {install_dict[key]}')

ginga_config_path = '/home/telops/sciops/gempyops/scripts/seqplot_ginga'

log_path = '/tmp/seqplot.log'
local_bin = '/home/telops/.local/bin'

PATH = os.getenv('PATH')
if local_bin not in PATH:
    os.environ['PATH'] = f'{PATH}:{local_bin}'

print('Starting Seqplot...')
start_cmd = (f'ginga --modules=Seqplot --log={log_path} --loglevel=20 '
             f'--basedir {ginga_config_path}')
os.system(start_cmd)
