Ginga may be installed from git, apt, conda, or source download and build.  Full instructions are here:
https://ginga.readthedocs.io/en/stable/install.html


Source acquisition instructions are available here:
https://ejeschke.github.io/ginga/


Source:

Clone the source:
$ git clone https://github.com/ejeschke/ginga.git

Build and install
$ python setup.py build
$ python setup.py install

Pip:
$ pip3 install ginga

Conda:
$ conda install ginga -c conda-forge

Debian/Ubuntu linux:
$ apt install python3-ginga


In each case, the python bin directory containing ginga must be included in the path:
$ export PATH=$PATH:<path/to/Python/3.8/bin/>



Requirements:
python 3.7+
setuptools-scm
numpy v1.14+
astropy
pillow
scipy
PyQt5
matplotlib
watchdog    (for seqplot use)
