"""
This is an implementation of Seqplot for use with the Ginga image viewer.

If you register this plugin using the setup.py script it will be
picked up in the reference viewer. If you want to test it without
registering it, put it somewhere in your PYTHONPATH and run the
reference viewer with this option:

    $ ginga --modules=Seqplot

it should become active in the right panel.

TBD
-----
- See C Figura's evernote page

Changelog
___________
- 2022.12.08: initial release candidate
- 2022.12.12: GRACES read capability added.
- 2023.01.11: added HISTORY cards for new file writes, intro mosaic_IGRINS
- 2023.01.19: tested/modified autoload functions
- 2023.02.21: GUI improvements & restructuring
- 2023.05.10: New functionality zoom, slice, examine, profile, setQA, pix2iq
- 2023.06.01: GNIRS/NIFS IFU dual view capability introduced, small bugfixes
- 2023.08.08: GMOS tile gaps, expanded extra info
- 2023.09.01: Image review (slideshow) implemented
              Autoload pause implemented -- images that come in during pause
              are not queued: only the last image will be shown.
- 2023.09.22: MEF/cube handling for unrecognised instruments and IGRINS-2
              SVC images.
- 2024.04.04: Improve/streamline autoload
"""

__version__ = '20240404'
__author__ = 'cfigura'
__all__ = ['Seqplot']

import copy
import glob
import os
import psutil
import re
import sys

from ginga import GingaPlugin, RGBImage, colors
from ginga.gw import Widgets, Viewers
from ginga.AstroImage import AstroImage

import seqplot_components.seq_utils as seq_utils
import seqplot_components.mywidgets as mywidgets

# Qt direct
from PyQt5 import QtGui
from PyQt5.QtCore import QMutex, QObject, QThread, pyqtSignal, Qt
from PyQt5.QtWidgets import QFileDialog

# Astropy functions
import numpy as np
from astropy.io import fits
from astropy.coordinates import Angle
from astropy import units as u
from astropy.wcs import WCS

# warnings
import warnings
from astropy.io.fits.verify import VerifyWarning, AstropyUserWarning
from astropy.wcs import FITSFixedWarning

# time related
from datetime import datetime, timedelta
from dateutil.parser import parse as du_parse
from pytz import timezone
import time

# for Examine/Pick
from ginga import AutoCuts
try:
    from ginga.gw import Plot
    from ginga.util import plots
    have_mpl = True
except ImportError:
    have_mpl = False

# For histogram
from astropy.visualization import ZScaleInterval

# For autoload
try:
    from watchdog.observers.polling import PollingObserver
    from watchdog.events import PatternMatchingEventHandler
    from watchdog.events import FileSystemEventHandler
    have_watchdog = True
except ImportError:
    have_watchdog = False

LINUX_PLATFORM = 'linux' in sys.platform
if False:  # LINUX_PLATFORM:
    import inotify.adapters
    from inotify.constants import IN_CLOSE_WRITE
    have_inotify = True
else:
    have_inotify = False

# cleanIR setup
try:
    import seqplot_components.cleanIR3 as cleanIR3
    have_clean = True
    IR_INSTRUMENTS = ["GNIRS", "NIRI", "NIFS"]
except ImportError:
    have_clean = False
    print("#Warning -- failed to import cleanIR3\n")
    IR_INSTRUMENTS = []

# IFU setup
try:
    import seqplot_components.IFUcollapse as IFU
    have_IFUcollapse = True
except ImportError:
    print('#Warning -- Failed to import IFUcollapse\n')
    have_IFUcollapse = False
IFU_params = {}
IFU_axis = {"GNIRS": 0, "NIFS": 1}
IFU_INSTRUMENTS = ["GNIRS-HR", "GNRIS-LR", "NIFS"]

# SeqplotExamine setup
try:
    import seqplot_components.seq_examine as examine
    HAVE_EXAMINE = True
except ImportError:
    HAVE_EXAMINE = False

# SeqplotCuts setup
try:
    import seqplot_components.seq_cuts as cuts
    HAVE_CUTS = True
except ImportError:
    HAVE_CUTS = False

# Warnings
warnings.filterwarnings('ignore', category=VerifyWarning, append=True)
warnings.filterwarnings('ignore', category=AstropyUserWarning, append=True)
warnings.filterwarnings('ignore', category=FITSFixedWarning, append=True)

# GLOBALS

LTZ = datetime.now().astimezone().tzinfo

AUTOLOAD_CADENCE = 0.1  # time in seconds to check for autoload

SATURATION_THRESHOLD = 2  # set saturation warning threshold at 2 %.

ALPHA_CURSOR = 0.4
ALPHA_MARKER = 1.0

CHNAME_A = 'seqplot'
CHNAME_B = 'sqplot2'
CHNAME_C = 'sqplot3'

CHNAME_A = 'Image'
CHNAME_B = 'Image0'
CHNAME_C = 'Image1'

CHNAME_LIST = [CHNAME_A, CHNAME_B, CHNAME_C]

DATE_WINDOW = 7

SLICETAG_C = 'HVslice_cursor'
SLICETAG_M = 'HVslice_line_marker'
SATTAG = 'saturation'
STATSTAG = 'statistics'
TEMPTAG = 'temporary_draw'

SCIEXT = 1  # nominal extension for science data in FITS images

DEFAULT_COLORMAP = 'viridis'
DEFAULT_DRAW_COLOR = 'orange'
COLORLIST = ['red', 'orange', 'yellow', 'green', 'cyan', 'blue']
SLICECOLOR = 'purple'

if LINUX_PLATFORM:
    APP_STYLE_SHEET = ("""
                       QPushButton {min-width: 45 px}
                       QPushButton:checked{background-color:green}
                       QPushButton[objectName^='smol']{max-width: 45px;}
                       """)
else:
    APP_STYLE_SHEET = ("""
                       QPushButton {border-radius: 4px;
                                    padding: 2px;
                                    background-color: #555555}
                       QPushButton:checked{background-color:green}
                       """)
# max-width: 80px;
# SMOL_SIZE = "60px"
# smol_button_style_sheet = f"max-width: {SMOL_SIZE}"

###############################################################################
# INSTRUMENTS

IGRINS2 = 'IGRINS-2'
INSTRUMENTS = ["GMOS-N", "GNIRS", "GRACES", IGRINS2, "NIFS", "NIRI", "GRACES",
               "GMOS-S", "F2", "GNAOI", "IGRINS"]

IGRINS_EXTS = [1, 2, 3]

# PATHS

SITE = os.environ['GEMINI_SITE'] if 'GEMINI_SITE' in os.environ else 'LOCAL'
if SITE == 'MK':
    #DATA_PATHS = {'NORMAL': "/net/mko-nfs/sci/dhs/",
    DATA_PATHS = {'NORMAL': "/gemsoft/dhs/perm/",
                  'ARCHIVE': "/net/mko-nfs/sci/dataflow/"}
    VISITOR_PATHS = {"GRACES": '/net/mko-nfs/sci/graces/telops/data/perm/'}
elif SITE == 'CP':
    DATA_PATHS = {'NORMAL': "/net/cpostonfs-nv1/tier1/dhs/sto/perm/",
                  'ARCHIVE': "/net/cpostonfs-nv1/dataflow/"}
    VISITOR_PATHS = {}
    IFUparampath = "/tmp/"
else:
    DATA_PATHS = {}
    VISITOR_PATHS = {}
    DATA_PATHS = {'NORMAL': os.path.expanduser("~/FITS/dhs"),
                  'ARCHIVE': os.path.expanduser("~/FITS/dataflow")}
    """
    VISITOR_PATHS = {"GRACES": os.path.expanduser("~/FITS/graces"),
                     "IGRINS": os.path.expanduser("~/FITS/igrins")}
    """

INSTALL_PATH = os.path.dirname(__file__)
IFUparampath = os.path.join(INSTALL_PATH, "seqplot_components/IFUparams/")
sys.path += [INSTALL_PATH]

WORK_PATH = "/tmp/Seqplot/"
ICON_PATH = os.path.join(INSTALL_PATH, 'seqplot_components/Seqplot_icon.png')

for key in VISITOR_PATHS.keys():
    if os.path.exists(VISITOR_PATHS[key]):
        DATA_PATHS[key] = VISITOR_PATHS[key]

if LINUX_PLATFORM:
    sys.path.insert(0, os.path.expanduser(INSTALL_PATH))


# QA ASSESSMENT
QA_COLOURS = ["white", "green", "yellow", "red", "white"]
QA_STATES = ["UNDEFINED", "PASS", "USABLE", "FAIL", "CHECK"]

# RANDOM
DISPLAY_MUTEX = QMutex()
WCS_CARDS = ['CRPIX1', 'CRVAL1', 'CDELT1', 'CTYPE1', 'CD1_1', 'CD1_2',
             'CRPIX2', 'CRVAL2', 'CDELT2', 'CTYPE2', 'CD2_1', 'CD2_2',
             'FRAME']

APP_NAME = __all__[0]


###############################################################################


class Seqplot(GingaPlugin.GlobalPlugin):

    def __init__(self, fv):
        """
        This method is called when the plugin is loaded for the  first
        time.  ``fv`` is a reference to the Ginga (reference viewer) shell.

        You need to call the superclass initializer and then do any local
        initialization.
        """
        super(Seqplot, self).__init__(fv)

        # set icon and application and window names
        self.fv._qtapp.setWindowIcon(QtGui.QIcon(ICON_PATH))
        self.fv._qtapp.setApplicationName(APP_NAME)
        self.fv._qtapp.setApplicationDisplayName(APP_NAME)
        window = self.fv._qtapp.allWindows()[0]
        window.setTitle(APP_NAME)

        self.fv._qtapp.setStyleSheet(APP_STYLE_SHEET)

        # Read preferences for this plugin
        prefs = self.fv.get_preferences()

        pref_dict = prefs.get_dict()
        pref_dict['general']['numImages'] = 1

        self.settings = prefs.create_category('plugin_Seqplot')

        self.datapath = None
        if 'NORMAL' in DATA_PATHS.keys():
            self.datapath = DATA_PATHS['NORMAL']

        # Make sure that the work path exists
        if not os.path.exists(WORK_PATH):
            os.makedirs(WORK_PATH)

        utcNow = datetime.now(timezone('UTC'))
        self.im_UTCdate = utcNow.strftime("%Y%m%d")

        self.im_frame = 1
        self.last_frame = None

        self.display_prefix = None
        self.obsID = None
        self.inst = None

        # Autoload items
        self.autoload = False
        self.autoload_paths = []
        if 'NORMAL' in DATA_PATHS.keys():
            self.autoload_paths = [DATA_PATHS['NORMAL']]
        self.autoload_fitspath = None
        self.autoload_tasks = {}
        self.autoload_threads = {}
        self.autoload_queue = {}  # queue[fitspath] = observer

        self.dual_fitslist = []
        self.dual_fitsqueue = []

        self.sub = False
        self.sub_fitspath = {key: "" for key in INSTRUMENTS}  # sub frame dict
        self.sub_fitsname = {key: "" for key in INSTRUMENTS}  # sub frame dict

        self.subFits = {key: generate_chname_dict(class_name=FrameObject)
                        for key in INSTRUMENTS}  # sub frame dictionary

        self.collapse = False
        self.clean = False
        self.visitor = False
        self.integIFU = False
        self.IFUinst = "NOT IFU"
        self.IFUoffset = {key: 0 for key in IFU_INSTRUMENTS}
        self.IFUoverride = False

        self.datashape = None
        self.display_hdu = None          # fits HDU for display
        self.display_fitspath = None

        self.viewmode = 'tabs'           # viewmode for single/dual display

        # Create some variables to keep track of what is happening
        # with which channel
        self.active = None  # CHNAME_LIST[0]
        self.gui_up = False
        self.tile = False
        self.timeSomethingTimer = 0

        # Channel-specific handles
        # self.viewStruct = generate_chname_dict(class_name=ViwStruct)
        self.channel = generate_chname_dict()
        self.fitsimage = generate_chname_dict()
        self.viewport_dim = generate_chname_dict()

        # For saturation canvas:
        self.canvas = generate_chname_dict()
        self.canvas_img = generate_chname_dict()

        self.HVslice_visible = False
        self.liveslice = False
        self.slice_axis = 'horizontal'
        self.slice_coords = None           # (x1, y1, x2, y2)
        self.slice_chname = CHNAME_A       # track cursor-active chname

        # parameters for current fits image
        self.currentFrame = generate_chname_dict(class_name=FrameObject)

        # Subscribe to some interesting callbacks that will inform us
        # of channel events.  You may not need these depending on what
        # your plugin does
        fv.set_callback('add-channel', self.add_channel)
        fv.set_callback('delete-channel', self.delete_channel)
        fv.set_callback('channel-change', self.focus_cb)
        fv.set_callback('active-image', self.focus_cb)

        ##############################
        # For aux displays:

        self._wd = 400
        self._ht = 400
        self.xmargin = self._wd/2
        self.ymargin = self._ht/2

        ##############################
        # For histogram:

        _sz = max(self._wd, self._ht)
        # hack to set a reasonable starting position for the splitter
        self._split_sizes = [_sz, _sz]

        self.drawcolour = DEFAULT_DRAW_COLOR

        prefs = self.fv.get_preferences()
        self.settings = prefs.create_category('plugin_Seqplot')
        self.settings.add_defaults(draw_then_move=True,
                                   hist_color='aquamarine')
        self.settings.load(onError='silent')

        # If True, limits X axis to lo/hi cut levels
        self.xlimbycuts = True
        # percentage to adjust plotting X limits when xlimbycuts is True
        self.lim_adj_pct = 0.03
        self._split_sizes = [400, 500]

        self.histcolor = self.settings.get('hist_color', 'aquamarine')
        self.numbins = 1024  # 2048
        self.autocuts = AutoCuts.Histogram(self.logger)

        # percentage to adjust cuts gap when scrolling in histogram
        self.scroll_pct = 0.10
        self.bbox_max = None
        self.bbox_min = None
        self.box_data = None  # generate_chname_dict()

        self.viewstats = (None, None, None, None)

        ##############################
        # For zoom:

        self.zoomimage = None
        self.default_radius = 30
        self.default_zoom = 3
        self.zoom_x = 0
        self.zoom_y = 0
        self.t_abszoom = True
        self.zoomtask = fv.get_backend_timer()
        self.zoomtask.set_callback('expired', self.showzoom_timer_cb)
        self.fitsimage_focus = None
        self.layer_tag = 'shared-canvas'
        self.update_time = time.time()

        spec = self.fv.get_plugin_spec(str(self))

        # read preferences for this plugin
        prefs = self.fv.get_preferences()
        self.settings = prefs.create_category('plugin_Zoom')
        self.settings.add_defaults(zoom_amount=self.default_zoom,
                                   closeable=not spec.get('hidden', False),
                                   refresh_interval=0.02)
        self.settings.load(onError='silent')

        self.zoom_amount = self.settings.get('zoom_amount', self.default_zoom)
        self.xmargin = self._wd/2 / self.zoom_amount
        self.ymargin = self._ht/2 / self.zoom_amount

        self.refresh_interval = self.settings.get('refresh_interval', 0.02)
        self.copy_attrs = ['transforms', 'cutlevels', 'rotation', 'rgbmap',
                           'icc']  # , 'interpolation']

        _sz = max(self._wd, self._ht)
        # hack to set a reasonable starting position for the splitter
        self._split_sizes = [_sz, _sz]

        fv.add_callback('add-channel', self.add_channel)

        # For threading:
        self.signal_progress = pyqtSignal(int)

        # For Examine/Pick
        self.examine = None
        self.cuts = None
        self.activetab = CHNAME_A  # "seqplot"

        # For review
        self.review = False
        self.reviewDelay = 5
        self.reviewBegin = 1
        self.reviewEnd = 999
        self.reviewTask = None
        self.reviewThread = None

    def build_gui(self, container):
        """
        This method is called when the plugin is invoked.  It builds the
        GUI used by the plugin into the widget layout (vbox) passed as
        ``container``.
        This method could be called several times if the plugin is opened
        and closed.  The method may be omitted if there is no GUI for the
        plugin.

        This specific example uses the GUI widget set agnostic wrappers
        to build the GUI, but you can also just as easily use explicit
        toolkit calls here if you only want to support one widget set.
        """

        # The following size determined by experimentation
        # container.widget.setStyleSheet("max-width: 383px")

        orientation = self.settings.get('orientation', None)
        paned = Widgets.Splitter(orientation=orientation)

        seqplot_gui = self.build_seqplot_gui()

        if HAVE_EXAMINE or HAVE_CUTS:

            tabs = Widgets.TabWidget(tabpos='top', detachable=True)
            self.tabs = tabs
            tabs.add_callback('page-switch', self.change_tooltab)

            tabs.add_widget(seqplot_gui, title="SeqControl")

            if HAVE_EXAMINE:
                examine_tab = self.build_examine_gui()
                tabs.add_widget(examine_tab, title="Examine")

            if HAVE_CUTS:
                cuts_tab = self.build_cuts_gui()
                tabs.add_widget(cuts_tab, title="Profile")
        else:
            tabs = seqplot_gui

        # Add our GUI to the container
        paned.add_widget(tabs)

        # Text widget to show some instructions status and error messages
        statusframe = self.build_gui_status()
        paned.add_widget(statusframe)

        # These sizes are experimentally determined.  We seem to want a big
        # first number and a small second number to push the status window
        # down
        paned.set_sizes([5000, 1])

        container.add_widget(paned, stretch=0)

        self.gui_up = True

        self.toggle_stats_monitor()

    def change_tooltab(self, tabwidget, tab):

        if self.examine is None:
            return
        self.activetab = tab.name

        self.set_cursor_mode(tab.name)

    def build_cuts_gui(self):

        """
        The examine gui began life as a local plugin, and needs a fitsimage
        to be established before instantiation.  This provides a placeholder
        location that will be filled later by a call to redo() once an
        image is loaded.
        """

        # needs to have a fitsimage established
        cuts_vbox = Widgets.VBox()
        cuts_vbox.name = "cuts"
        self.cuts_vbox = cuts_vbox

        return cuts_vbox

    def build_examine_gui(self):

        """
        The examine gui began life as a local plugin, and needs a fitsimage
        to be established before instantiation.  This provides a placeholder
        location that will be filled later by a call to redo() once an
        image is loaded.
        """

        # needs to have a fitsimage established
        examine_vbox = Widgets.VBox()
        examine_vbox.name = "examine"
        self.examine_vbox = examine_vbox

        return examine_vbox

    def build_seqplot_gui(self):

        top = Widgets.VBox()
        top.set_border_width(4)
        top.set_spacing(2)
        top.name = "seqplot"

        # this is a little trick for making plugins that work either in
        # a vertical or horizontal orientation.  It returns a box container,
        # a scroll widget and an orientation ('vertical', 'horizontal')

        orientation = self.settings.get('orientation', None)

        paned = Widgets.Splitter(orientation=orientation)

        lockbox = Widgets.VBox()
        lockbox.set_spacing(2)

        # Set up basic control panel
        controlframe = self.build_gui_control()
        top.add_widget(controlframe, stretch=0)

        # Set up image information panel
        hbox = Widgets.HBox()
        hbox.set_border_width(0)
        hbox.set_spacing(2)

        infoframe = self.build_gui_info()
        hbox.add_widget(infoframe, stretch=0)

        viewframe = self.build_gui_view()
        hbox.add_widget(viewframe, stretch=0)

        top.add_widget(hbox, stretch=0)

        # Set up image stats
        statsframe = self.build_gui_stats()
        paned.add_widget(statsframe)

        top.add_widget(paned)
        return top

    def build_gui_control(self):

        # Set up control panel

        controlframe = Widgets.Frame('Control')
        controlbox = Widgets.VBox()
        controlbox.set_spacing(3)

        dateframebox = Widgets.HBox()
        dateframebox.set_spacing(10)

        combobox_source = Widgets.ComboBox()
        for item in DATA_PATHS.keys():
            combobox_source.append_text(item)
        combobox_source.add_callback('activated', self.change_data_source)
        combobox_source.set_tooltip("Choose data source")
        combobox_source.widget.setCurrentText('NORMAL')
        self.combobox_source = combobox_source

        label_date = Widgets.Label(text="Date:", halign='left',
                                   style='bold')
        entry_date = Widgets.TextEntry(editable=True, text=self.im_UTCdate)
        entry_date.add_callback('activated', lambda w: self.set_date())
        entry_date.set_tooltip("type a date YYYYMMDD or"
                               "combination YYYYMMDD NNN")
        self.entry_date = entry_date

        label_frame = Widgets.Label(text="Frame:", halign='left',
                                    style='bold')
        entry_frame = Widgets.TextEntry(editable=True,
                                        text=f'{self.im_frame:03d}')
        entry_frame.set_length(4)
        entry_frame.add_callback('activated',
                                 lambda w: self.set_frame_number())
        entry_frame.set_tooltip("type a frame number NNN")

        self.entry_frame = entry_frame

        widgets = [combobox_source, label_date, entry_date, label_frame,
                   entry_frame]
        for widget in widgets:
            dateframebox.add_widget(widget)
        controlbox.add_widget(dateframebox)

        # Set up frame advance control

        hbox = Widgets.HBox()
        hbox.set_spacing(3)

        btn = Widgets.ToggleButton(text="Auto")
        btn.add_callback('activated', self.toggle_autoload)
        btn.set_tooltip('toggle autodisplay mode')
        btn.set_enabled(have_watchdog)
        hbox.add_widget(btn, stretch=1)
        self.btn_auto = btn

        btn = Widgets.ToggleButton(text="Pause")
        btn.set_enabled(True)
        btn.add_callback('activated', self.pause_display)
        btn.set_tooltip("pause autoload")
        hbox.add_widget(btn, stretch=1)
        self.btn_autoPause = btn

        btn = Widgets.Button(text="Prev")
        btn.add_callback('activated', self.prev_image)
        btn.set_tooltip('select previous sequential frame')
        hbox.add_widget(btn, stretch=1)

        """
        btn = Widgets.Button(text="Today")
        btn.add_callback('activated', lambda w: self.set_today())
        btn.set_tooltip('set date to now, first frame available.')
        hbox.add_widget(btn)
        """

        btn = Widgets.Button(text="Next")
        btn.add_callback('activated', self.next_image)
        btn.set_tooltip('select next sequential frame')
        hbox.add_widget(btn, stretch=1)

        btn = Widgets.Button(text="Last")
        btn.add_callback('activated', self.last_image)
        btn.set_tooltip('select most recent available image')
        hbox.add_widget(btn, stretch=1)

        controlbox.add_widget(hbox)

        # Set up basic processing box

        hbox = Widgets.HBox()
        hbox.set_spacing(3)

        btn = Widgets.ToggleButton(text="Clean")
        btn.set_enabled(False)
        btn.add_callback('activated', self.toggle_cleanIR)
        btn.set_tooltip("toggle pattern noise cleaning for IR images")
        hbox.add_widget(btn)
        self.btn_cleanIR = btn

        btn = Widgets.ToggleButton(text="IFU")
        btn.add_callback('activated', self.toggle_dialogue_IFU)
        btn.set_tooltip("Open IFU control window")
        hbox.add_widget(btn)
        self.btn_IFUmenu = btn

        hbox.add_widget(Widgets.Label(""), stretch=0)

        btn = Widgets.Button(text='Load')
        btn.add_callback('activated', self.load_file)
        btn.set_tooltip("Load arbitrary FITS image")
        hbox.add_widget(btn)

        btn = Widgets.Button(text='Add Path')
        btn.add_callback('activated', self.add_path)
        btn.set_tooltip("Add FITS path")
        hbox.add_widget(btn)

        controlbox.add_widget(hbox)

        # Set up frame subtraction control

        hbox = Widgets.HBox()
        hbox.set_spacing(3)

        btn = Widgets.ToggleButton(text="Subtraction")
        btn.add_callback('activated', self.toggle_sub)
        btn.set_tooltip('toggle sky subtraction')
        hbox.add_widget(btn, stretch=1)
        self.btn_sub_toggle = btn

        entry = Widgets.TextEntry(editable=False)
        # entry.set_font(self.boldFont,size=20)
        entry.add_callback('activated', self.set_sub_entry)
        entry.set_tooltip("Date & frame of image to be subtracted")
        hbox.add_widget(entry, stretch=1)
        self.entry_sub = entry

        sub_entry_set = Widgets.Button(text="SET")
        sub_entry_set.add_callback('activated', self.set_current_as_sub)
        sub_entry_set.set_tooltip("Use the current image for sky subtraction")
        hbox.add_widget(sub_entry_set, stretch=0)

        controlbox.add_widget(hbox)

        controlframe.set_widget(controlbox)

        """
        # create a test dialog to see if this happens all the time...
        testbtn = Widgets.ToggleButton(text="TEST")
        testbtn.add_callback('activated', self.toggle_testmenu)
        testbtn.set_tooltip("test dialog creation")
        self.testbtn = testbtn

        controlbox.add_widget(self.testbtn)

        test_dialogue = Widgets.Dialog(title="Test",parent=self.btn_IFUmenu,
                                       flags=1,buttons=[['Ok',1]])
        dbox = test_dialogue.get_content_area()
        vtbox = Widgets.VBox()
        dbox.set_border_width(6)

        btnA = Widgets.ToggleButton(text = "A")
        vtbox.add_widget(btnA)
        btnB = Widgets.ToggleButton(text = "B")
        vtbox.add_widget(btnB)
        btnC = Widgets.ToggleButton(text = "C")
        vtbox.add_widget(btnC)
        btnD = Widgets.ToggleButton(text = "D")
        vtbox.add_widget(btnD)

        dbox.add_widget(vtbox)

        test_dialogue.widget.clearFocus()

        self.test_dialogue = test_dialogue

        print("test_dialogue.widget:")
        print_dir(test_dialogue.widget)
        print(help(test_dialogue.widget.clearFocus))
        """

        ##########

        # Create the IFU dialogue
        self.dialogue_IFU = self.build_dialogue_IFU(controlbox)

        # Create the HV cuts dialogue
        self.dialogue_hvslice = self.build_dialogue_HVslice(controlbox)

        # Create the zoom dialogue
        self.dialogue_zoom = self.build_dialogue_zoom(controlbox)

        # Create the review dialogue
        self.dialogue_review = self.build_dialogue_review(controlbox)

        # Create the MEF dialogue.
        self.dialogue_MEF = self.build_dialogue_MEF(controlbox)

        self.logger.info("GUI CREATION FINISHED")


        return controlframe

    def build_gui_info(self):
        # Set up image information panel

        infoframe = Widgets.Frame("Image Information")

        captions = (('Image Name:', 'label', 'imname', 'llabel'),
                    ('Observation ID:', 'label', 'obsid', 'llabel'),
                    ('source', 'llabel', 'target', 'llabel'),
                    ('Instrument:', 'label', 'instrument', 'llabel'),
                    ('Offsets (p,q):', 'label', 'pqoffsets', 'llabel'),
                    ('QA State:', 'label', 'qa', 'combobox')
                    )
        w, b = mywidgets.build_info(captions, align=True)
        self.w.update(b)
        self.wInfo = b

        infoframe.set_widget(w)

        model = b.qa.widget.model()
        for row, (item, colour) in enumerate(zip(QA_STATES, QA_COLOURS)):
            b.qa.widget.addItem(item)
            model.setData(model.index(row, 0),
                          QtGui.QColor(colour), Qt.ForegroundRole)
            # or Qt.BackgroundRole

        if seq_utils.COOKIES['gemini_api_authorization'] == '':
            b.qa.set_enabled(False)

        b.qa.add_callback('activated', self.set_QA)
        b.qa.set_tooltip("set quallity assessment state for this image")

        return infoframe

    def build_gui_view(self):
        # set up view controls

        # Create the info dialogue
        viewbox = Widgets.VBox()
        viewbox.set_spacing(0)
        self.w_info_dialogue = self.build_dialogue_info(viewbox)
        self.w_info_dialogue.add_callback('close', lambda w, state=False:
                                          self.toggle_dialogue_info(w, state))

        viewframe = Widgets.Frame("View")
        viewbox.add_widget(viewframe)

        viewgrid = Widgets.GridBox(rows=3, columns=2)
        viewgrid.set_spacing(3)
        viewgrid.set_margins(0, 0, 0, 0)

        btn = Widgets.ToggleButton(text="Info")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.add_callback('activated', self.toggle_dialogue_info)
        btn.set_tooltip('view extra frame information')
        viewgrid.add_widget(btn, 0, 0)
        self.w_info_btn = btn

        btn = Widgets.Button(text="Colour")
        btn.add_callback('activated', self.cycle_drawcolour)
        btn.set_tooltip('cycle box colour')
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")

        if LINUX_PLATFORM:
            style_sheet = (f"background-color:{self.drawcolour}; color:black"
                           # f"max-width: {SMOL_SIZE}"
                           )
        else:
            style_sheet = (f"background-color:{self.drawcolour};"
                           "border-radius: 4px;"
                           "padding: 2px;"
                           "min-width: 55px;"
                           # f"max-width: {SMOL_SIZE};"
                           "color:black")
        btn.widget.setStyleSheet(style_sheet)
        viewgrid.add_widget(btn, 0, 1)

        btn = Widgets.ToggleButton(text="Slice")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.add_callback('activated', self.toggle_slice_dialogue)
        btn.set_tooltip("Open hvslice window")
        viewgrid.add_widget(btn, 1, 0)
        self.w_hvslice_btn = btn

        btn = Widgets.ToggleButton(text="Zoom")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.set_tooltip("Open zoom window")
        btn.add_callback('activated', self.toggle_dialogue_zoom)
        viewgrid.add_widget(btn, 1, 1)
        self.btn_zoom = btn

        btn = Widgets.ToggleButton(text="Review")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.set_tooltip("Open review window")
        btn.add_callback('activated', self.toggle_dialogue_review)
        viewgrid.add_widget(btn, 2, 1)
        self.btn_review = btn

        w_satmask_btn = Widgets.ToggleButton(text="Mask")
        w_satmask_btn.widget.setAccessibleName("smol button")
        w_satmask_btn.widget.setObjectName("smol button")
        w_satmask_btn.set_tooltip("Toggle saturation mask")
        w_satmask_btn.add_callback('activated', self.toggle_satmask)
        viewgrid.add_widget(w_satmask_btn, 3, 1)
        self.w_satmask_btn = w_satmask_btn

        btn = Widgets.Button(text="Fit")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.add_callback('activated', self.fit_channels)
        btn.set_tooltip('zoom image(s) to fit viewer')
        viewgrid.add_widget(btn, 3, 0)

        viewframe.set_widget(viewgrid)

        return viewbox

    def build_gui_stats(self):
        # Set up image stats
        statsframe = Widgets.Frame("Image Statistics")

        vbox_stats = Widgets.VBox()
        vbox_stats.set_spacing(0)

        hbox = Widgets.HBox()
        hbox.set_spacing(5)

        btn = Widgets.Button(text="Full")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.add_callback('activated', self.select_fullframe)
        btn.set_tooltip('Set full frame statistics')
        hbox.add_widget(btn)
        self.w_fullfrm_btn = btn

        btn = Widgets.ToggleButton(text="Stats")
        btn.widget.setAccessibleName("smol button")
        btn.widget.setObjectName("smol button")
        btn.add_callback('activated', self.toggle_statsdraw)
        btn.set_tooltip("Activate stats mode for draw/move/edit")
        hbox.add_widget(btn)
        self.statsdraw_btn = btn

        hbox.add_widget(Widgets.Label(''), stretch=1)

        radbtn1 = Widgets.RadioButton("Draw")
        radbtn1.set_state(True)
        radbtn1.add_callback('activated',
                             lambda w, val: self.set_mode_cb('draw', val))
        radbtn1.set_tooltip("Choose this to draw a replacement box")
        self.w_drawbox_radio = radbtn1
        hbox.add_widget(radbtn1)

        radbtn2 = Widgets.RadioButton("Move", group=radbtn1)
        radbtn2.set_state(False)
        radbtn2.add_callback('activated',
                             lambda w, val: self.set_mode_cb('move-hist', val))
        radbtn2.set_tooltip("Choose this to position box")
        self.w_movebox_radio = radbtn2
        hbox.add_widget(radbtn2)

        radbtn3 = Widgets.RadioButton("Edit", group=radbtn1)
        radbtn3.set_state(False)
        radbtn3.add_callback('activated',
                             lambda w, val: self.set_mode_cb('edit', val))
        radbtn3.set_tooltip("Choose this to edit a box")
        self.w_editbox_radio = radbtn3
        hbox.add_widget(radbtn3)

        hbox.add_widget(Widgets.Label(''), stretch=1)

        w_cut_btn = Widgets.Button(text="Cut")
        w_cut_btn.widget.setAccessibleName("smol button")
        w_cut_btn.widget.setObjectName("smol button")
        w_cut_btn.add_callback('activated', self.set_bbox_cuts)
        w_cut_btn.set_tooltip('autoscale based on stats box')
        hbox.add_widget(w_cut_btn)

        self.w_movebox_radio.set_enabled(False)

        vbox_stats.add_widget(hbox)

        #######################################################################
        # Histogram portion below, taken from histogram plugin

        self.histPlot = plots.Plot(logger=self.logger, width=400, height=400)
        self.histPlot.fontsize = 7

        ax = self.histPlot.add_axis()
        ax.grid(True)

        if 'button-press' in self.histPlot.cb.keys():
            self.histPlot.add_callback('button-press', self.set_cut_by_click)
            self.histPlot.add_callback('scroll', self.adjust_cuts_scroll)
            tip = ("brown and green indicate low/high cut values.\n"
                   "left and right mouse buttons set low/high cut values.\n"
                   "yellow and orange indicate mean and median values.")
        else:
            self.logger.info("No 'button-press' callback available in plot, "
                             "histogram cut-by-click will not be available.")
            tip = ("brown and green indicate low/high cut values.\n"
                   "yellow and orange indicate mean and median values.")

        histWidget = Plot.PlotWidget(self.histPlot)
        histWidget.set_tooltip(tip)

        self.histPlot.connect_ui()
        histWidget.resize(400, 400)
        # self.histPlot.get_figure().tight_layout()
        # self.histPlot.fig.set_tight_layout(False)
        vbox_stats.add_widget(histWidget)

        # Histogram portion above, taken from histogram plugin
        #######################################################################

        grid_stats = Widgets.GridBox(rows=2, columns=3)
        grid_stats.set_spacing(1)
        # set_margins(left, right, top, bottom)
        grid_stats.set_margins(2, 2, 2, 2)
        statstext = ["Mean", "Median", "Maximum"]
        statsvars = ["mean", "med", "max"]

        statslabels = {}
        statswidgts = {}

        for column, (text, var) in enumerate(zip(statstext, statsvars)):
            statslabels[var] = Widgets.Label(text=f'{text}:', halign='center',
                                             style='bold')
            statswidgts[var] = Widgets.Label(text='', halign='center',
                                             style='bold')
            # statswidgts[var].set_font(self.boldFont,size=20)
            grid_stats.add_widget(statslabels[var], 0, column)
            grid_stats.add_widget(statswidgts[var], 1, column)

        statslabels['med'].set_color('orange')
        statslabels['mean'].set_color('yellow')
        statslabels['max'].set_color('red')

        self.statslabels = statslabels
        self.statswidgts = statswidgts

        vbox_stats.add_widget(grid_stats)

        grid_sat = Widgets.GridBox(rows=1, columns=4)
        grid_sat.set_spacing(1)
        grid_sat.set_margins(2, 2, 2, 2)

        sattext = ["Sat Limit", "Sat Pixels"]
        satvars = ["sat_limit", "nsat"]
        sattips = ["Non-linear/saturation Limit", "Number of saturated pixels"]
        satlabels = {}
        satwidgts = {}
        row = 0
        for i, (text, var, tip) in enumerate(zip(sattext, satvars, sattips)):
            satlabels[var] = Widgets.Label(text=f'{text}:', halign='center',
                                           style='bold')
            satlabels[var].set_tooltip(tip)
            satwidgts[var] = Widgets.Label(text='', halign='center',
                                           style='bold')
            satwidgts[var].set_tooltip(tip)
            # satwidgts[var].set_font(self.boldFont,size=20)
            grid_sat.add_widget(satlabels[var], row, 2*i)
            grid_sat.add_widget(satwidgts[var], row, 2*i+1)

        self.satlabels = satlabels
        self.satwidgts = satwidgts

        vbox_stats.add_widget(grid_sat)
        statsframe.set_widget(vbox_stats)
        return statsframe

    def build_gui_status(self):

        statusframe = Widgets.Frame()
        statusWidget = Widgets.TextArea(wrap=True, editable=False)
        self.statusWidget = statusWidget

        widget = statusWidget.widget
        widget.setVerticalScrollBarPolicy(2)
        widget.heightMin = 100

        statusframe.set_widget(statusWidget)
        return statusframe

    def build_gui_xit(self):

        hbox = Widgets.HBox()
        hbox.set_spacing(0)

        btn = Widgets.Button(text="Fit View")
        btn.set_tooltip('zoom image(s) to fit viewer')
        btn.add_callback('activated', self.fit_channels)
        hbox.add_widget(btn, stretch=0)

        hbox.add_widget(Widgets.Label(''), stretch=1)

        # Add a close button for the convenience of the user
        btn = Widgets.Button(text="Close")
        btn.add_callback('activated', lambda w: self.close())
        btn.set_tooltip('Close Seqplot tool')

        hbox.add_widget(btn, stretch=0)

        return hbox

    def build_dialogue_IFU(self, parent):

        # Create the IFU dialogue

        dialogue_IFU = Widgets.Dialog(title="Seqplot IFU Controls",
                                      parent=parent)
        dialogue_IFU.widget.setWindowFlag(Qt.WindowStaysOnTopHint)
        dialogue_IFU.add_callback('close', lambda w, state=False:
                                  self.toggle_dialogue_zoom(w, state))

        dialogueBox = dialogue_IFU.get_content_area()
        dialogueBox.set_border_width(6)

        """
        I'm having a problem where the first button in the dialogue is
        highlighted as if it is in an on state *independent* of the
        actual state.
        I'm creating a dud button as 'btn' to slot in first to take that
        highlight anomaly, and hide it.  This is a kludge, but I can't
        figure out what's going on here.
        """

        btn = Widgets.ToggleButton(text='')
        dialogueBox.add_widget(btn)
        btn.hide()

        IFUMenuFrame = Widgets.Frame(title='IFU Controls')
        IFUMenuBox = Widgets.VBox()
        IFUMenuBox.set_spacing(3)

        # IFU controls

        hbox = Widgets.HBox()
        hbox.set_spacing(1)

        btn = Widgets.ToggleButton(text="Swap")
        btn.add_callback('activated', self.toggle_integIFU)
        btn.set_tooltip('toggle IFU integration')
        hbox.add_widget(btn)
        self.btn_integIFU = btn
        btn.set_enabled(False)

        btn = Widgets.Button(text="Analyse flat")
        btn.add_callback('activated', self.analyse_IFU)
        btn.set_tooltip("measure integration parameters from current IFU flat")
        hbox.add_widget(btn)
        self.btn_analyseIFU = btn
        btn.set_enabled(False)

        btn = Widgets.Button(text="Defaults")
        btn.add_callback('activated', lambda w=True:
                         self.read_IFU_params(redisplay=w))
        btn.set_tooltip('restore built-in IFU parameters')
        hbox.add_widget(btn)
        self.btn_defaultIFU = btn
        btn.set_enabled(False)

        combobox = Widgets.ComboBox()
        for item in ["OVERRIDE"] + IFU_INSTRUMENTS:
            combobox.append_text(item)
        combobox.add_callback('activated', self.set_IFU_override)
        combobox.set_tooltip('Set IFU overrides')
        combobox.set_text('OVERRIDE')
        self.combobox_IFUoverride = combobox
        hbox.add_widget(combobox, stretch=0)

        IFUMenuBox.add_widget(hbox)

        # IFU offsets

        offsetLabel = Widgets.Label(text="IFU Integration offsets",
                                    halign='center', style='bold')
        offsetLabel.set_color('gray')
        self.offsetLabel = offsetLabel
        IFUMenuBox.add_widget(offsetLabel)

        hbox = Widgets.HBox()
        hbox.set_spacing(1)
        IFUoffsets = [+5, +1, -1, -5]
        btn_dict = {}
        for offset in IFUoffsets:
            btn = Widgets.Button(text=f'{offset:+d}')
            hbox.add_widget(btn)
            btn.set_enabled(False)
            btn.add_callback('activated',
                             lambda state, x=offset: self.adjust_IFUoffset(x))
            btn_dict[f'{offset:+d}'] = btn
        hbox.set_tooltip('pixel shifts for IFU integration')
        self.btns_IFUoffset = btn_dict

        IFUMenuBox.add_widget(hbox)

        IFUMenuFrame.set_widget(IFUMenuBox)
        dialogueBox.add_widget(IFUMenuFrame)

        # Create Preview display

        IFUimage = Viewers.CanvasView(logger=None)
        wd = 350
        ht = 350
        IFUimage.set_desired_size(wd, ht)
        # IFUimage.enable_autozoom('off')
        IFUimage.enable_autocuts('on')
        IFUimage.set_color_map(DEFAULT_COLORMAP)
        self.IFUimage = IFUimage
        viewWidget = Viewers.GingaViewerWidget(IFUimage)
        dialogueBox.add_widget(viewWidget)

        # IFU menu close controls

        hbox = Widgets.HBox()
        hbox.set_spacing(3)

        hbox.add_widget(Widgets.Label(''), stretch=1)

        btn = Widgets.Button(text="Close")
        btn.set_tooltip('Close IFU controls')
        btn.add_callback('activated', self.close_IFU_menu)
        hbox.add_widget(btn, stretch=0)

        dialogueBox.add_widget(hbox)

        return dialogue_IFU

    def build_dialogue_info(self, parent):

        # Create the extra info dialogue

        dialogue = Widgets.Dialog(title="Image Info", buttons=[['Close', 1]],
                                  parent=parent)
        dialogue.widget.setWindowFlag(Qt.WindowStaysOnTopHint)
        dialogue.add_callback('close', lambda w, state=False:
                              self.toggle_dialogue_info(w, state))

        dialogue.add_callback('activated', lambda w, b:
                              self.toggle_dialogue_info(w, False))

        dialogueBox = dialogue.get_content_area()
        dialogueBox.set_border_width(6)

        self.infoDialogueBox = dialogueBox

        return dialogue

    def build_dialogue_MEF(self, parent):

        # Create a popup to allow selection of image extension or
        # cube plane

        dialogue = Widgets.Dialog(title="MEF/Cube Image Selection",
                                  # buttons=[['Select', 0], ['Close', 1]],
                                  parent=parent)

        dialogue.add_callback('close', lambda w, state=False:
                              self.toggle_dialogue_MEF(w, state))
        dialogue.widget.setWindowFlag(Qt.WindowStaysOnTopHint)
        dialogue.add_callback('close', lambda w, state=False:
                              self.toggle_dialogue_MEF(w, state))

        dialogue.add_callback('activated', lambda w, b:
                              self.toggle_dialogue_MEF(w, False))

        dialogueBox = dialogue.get_content_area()
        dialogueBox.set_border_width(6)

        hbox = Widgets.HBox()
        hbox.set_border_width(0)
        hbox.set_spacing(2)

        def set_extension(w, idx):

            # if IGRINS, only change SVC channel (CHNAME_C)
            channel = CHNAME_C if self.inst == IGRINS2 else self.active
            cF = self.currentFrame[channel]

            ext = cF.image_exts[idx]
            cF.ext = ext
            self.configure_MEF_cube(ext, plane=0)
            select_image(changed=True)

        hbox.add_widget(Widgets.Label('Select extension:'), stretch=0)
        combobox = Widgets.ComboBox()
        self.MEF_extCombobox = combobox
        hbox.add_widget(combobox)

        combobox.add_callback('activated', set_extension)

        dialogueBox.add_widget(hbox)

        hbox = Widgets.HBox()
        hbox.set_border_width(0)
        hbox.set_spacing(2)

        hbox.add_widget(Widgets.Label('Select cube plane:'), stretch=0)

        def set_plane(value=None):

            self.MEF_cubeSpinbox.widget.setValue(value)
            self.MEF_cubeSlider.widget.setValue(value)

            select_image()

        slider_vbox = Widgets.VBox()
        slider_vbox.set_margins(0, 0, 0, 0)
        slider = Widgets.Slider()

        # callbacks are a *little* complicated.
        # slider move updates spinbox
        # slider *release* sets plane.
        # slider.widget.sliderReleased.connect(set_plane)
        # lambda w, val: set_plane(val))
        slider.add_callback('value-changed', lambda w, val: set_plane(val))
        slider.widget.sliderMoved.connect(lambda val: spinbox.set_value(val))

        self.MEF_cubeSlider = slider

        slider_vbox.add_widget(slider)
        slider_hbox = Widgets.HBox()
        slider_hbox.set_margins(0, 0, 0, 0)
        slider_hbox.add_widget(Widgets.Label(text='0'))
        slider_hbox.add_widget(Widgets.Label(), stretch=1)
        label = Widgets.Label(text='H')
        self.MEF_cubeMaxLabel = label
        slider_hbox.add_widget(label)
        slider_vbox.add_widget(slider_hbox)
        hbox.add_widget(slider_vbox, stretch=0)

        spinbox = Widgets.SpinBox()

        # spinbox change triggers a set_plane update
        spinbox.widget.valueChanged.connect(set_plane)

        self.MEF_cubeSpinbox = spinbox
        hbox.add_widget(spinbox, stretch=0)

        dialogueBox.add_widget(hbox)

        hbox = Widgets.HBox()

        def select_image(changed=False):

            # if IGRINS, only change SVC channel (CHNAME_C)
            channel = CHNAME_C if self.inst == IGRINS2 else self.active
            cF = self.currentFrame[channel]

            if self.MEF_extCombobox.widget.isEnabled():
                ext = int(self.MEF_extCombobox.get_text().split(':')[0])
                if cF.ext != ext:
                    changed = True
                    cF.ext = ext
            if self.MEF_cubeSpinbox.widget.isEnabled():
                plane = self.MEF_cubeSpinbox.get_value()
                if cF.plane != plane:
                    changed = True
                    cF.plane = plane

            if changed:
                self.display_image()

        hbox.add_widget(Widgets.Label(), stretch=1)
        btn = Widgets.Button(text='Select')
        btn.add_callback('activated', lambda w: select_image)
        hbox.add_widget(btn)

        dialogueBox.add_widget(hbox)

        return dialogue

    def build_dialogue_review(self, parent):

        # Create the review dialogue

        dialogue = Widgets.Dialog(title="Review", buttons=[['Close', 1]],
                                  parent=parent)
        dialogue.add_callback('close', lambda w, state=False:
                              self.toggle_dialogue_review(w, state))
        dialogue.widget.setWindowFlag(Qt.WindowStaysOnTopHint)
        dialogue.add_callback('close', lambda w, state=False:
                              self.toggle_dialogue_review(w, state))

        dialogue.add_callback('activated', lambda w, b:
                              self.toggle_dialogue_review(w, False))

        dialogueBox = dialogue.get_content_area()
        dialogueBox.set_border_width(6)

        hbox = Widgets.HBox()
        hbox.set_border_width(0)
        hbox.set_spacing(2)

        def set_reviewBegin(widget, value):
            self.reviewBegin = value

        hbox.add_widget(Widgets.Label(text='Begin:'))
        spinbox = Widgets.SpinBox()
        spinbox.set_limits(1, 999)
        spinbox.set_value(self.reviewBegin)
        spinbox.add_callback('value-changed', set_reviewBegin)
        hbox.add_widget(spinbox)
        self.spinbox_reviewBegin = spinbox

        def set_reviewEnd(btn, value):
            self.reviewEnd = value

        hbox.add_widget(Widgets.Label(text='End:'))
        spinbox = Widgets.SpinBox()
        spinbox.set_limits(1, 999)
        spinbox.set_value(self.reviewEnd)
        spinbox.add_callback('value-changed', set_reviewEnd)
        hbox.add_widget(spinbox)

        dialogueBox.add_widget(hbox)

        hbox = Widgets.HBox()
        hbox.set_border_width(0)
        hbox.set_spacing(2)

        def set_reviewDelay(widget, value):
            self.reviewDelay = value

        hbox.add_widget(Widgets.Label(text='Delay:'))
        spinbox = Widgets.SpinBox()
        spinbox.set_value(self.reviewDelay)
        spinbox.add_callback('value-changed', set_reviewDelay)
        spinbox.widget.setSuffix(' s')
        spinbox.set_tooltip('Set delay time before advance to next')
        hbox.add_widget(spinbox)

        hbox.add_widget(Widgets.Label(), stretch=1)

        dialogueBox.add_widget(hbox)

        hbox = Widgets.HBox()
        hbox.set_border_width(0)
        hbox.set_spacing(2)

        btn = Widgets.ToggleButton(text="Run")
        btn.add_callback('activated', self.toggle_review)
        btn.set_tooltip('Toggle review.  Deactivates autoload')
        hbox.add_widget(btn)
        self.btn_reviewOn = btn
        btn.set_enabled(True)

        btn = Widgets.ToggleButton(text="Pause")
        btn.add_callback('activated', self.pause_display)
        btn.set_tooltip('Pause review')
        hbox.add_widget(btn)
        self.btn_reviewPause = btn
        btn.set_enabled(True)

        dialogueBox.add_widget(hbox)

        self.reviewDialogueBox = dialogueBox

        return dialogue

    def ickfoo(self, *args):
        print("ickfoo!")
        print(args)

    ###########################################################################
    # Zoom specific thingies

    def build_dialogue_zoom(self, parent):

        # Show a zoom popup.  Kitbashed from Zoom.py plugin.

        dialogue_zoom = Widgets.Dialog(title="Seqplot zoom display",
                                       parent=parent)
        dialogue_zoom.widget.setWindowFlag(Qt.WindowStaysOnTopHint)
        dialogue_zoom.add_callback('close', lambda w, state=False:
                                   self.toggle_dialogue_zoom(w, state))

        dialogueBox = dialogue_zoom.get_content_area()
        dialogueBox.set_border_width(6)

        zi = Viewers.CanvasView(logger=None)
        wd = 300
        ht = 300
        zi.set_desired_size(wd, ht)
        zi.enable_autozoom('off')
        zi.enable_autocuts('off')
        zi.zoom_to(self.default_zoom)
        settings = zi.get_settings()
        settings.get_setting('zoomlevel').add_callback(
            'set', self.zoomset, zi)
        zi.set_bg(0.4, 0.4, 0.4)
        zi.show_pan_mark(True)
        # for debugging
        zi.set_name('zoomimage')
        self.zoomimage = zi

        bd = zi.get_bindings()
        bd.enable_zoom(False)
        bd.enable_pan(False)
        bd.enable_cmap(False)

        iw = Viewers.GingaViewerWidget(zi)
        iw.resize(self._wd, self._ht)

        dialogueBox.add_widget(iw)

        captions = (("Zoom Amount:", 'label', 'Zoom Amount', 'hscale',
                     'Zoom', 'label'),)
        w, b = Widgets.build_info(captions)
        self.w.update(b)
        b.zoom.set_text(self.fv.scale2text(zi.get_scale()))

        dialogueBox.add_widget(w, stretch=0)

        self.w.zoom_amount.set_limits(-20, 30, incr_value=1)
        self.w.zoom_amount.set_value(self.zoom_amount)
        self.w.zoom_amount.add_callback('value-changed', self.set_amount_cb)
        self.w.zoom_amount.set_tracking(True)

        return dialogue_zoom

    def set_amount_cb(self, widget, val):
        """This method is called when 'Zoom Amount' control is adjusted.
        """
        self.zoom_amount = val
        zoomlevel = self.fitsimage_focus.get_zoom()
        self._zoomset(self.fitsimage_focus, zoomlevel)

    def prepare_zoom(self, fitsimage):
        fitssettings = fitsimage.get_settings()
        fitsimage.add_callback('cursor-changed', self.motion_zoom_cb)
        fitsimage.add_callback('redraw', self.redraw_zoom_cb)
        fitssettings.get_setting('zoomlevel').add_callback(
            'set', self.zoomset_cb, fitsimage)

    def showzoom_timer_cb(self, timer):
        if not self.gui_up:
            return
        data = timer.data
        print(f"showzoom_timer_cb: panning to ({data.data_x},{data.data_y})")
        self._zoom_data(self.zoomimage, data.data_x, data.data_y)

    def _zoom_data(self, fitsimage, data_x, data_y):
        fitsimage.set_pan(data_x, data_y)

    def _zoomset(self, fitsimage, zoomlevel):
        if fitsimage != self.fitsimage_focus:
            return True
        if self.t_abszoom:
            # Did user set to absolute zoom?
            myzoomlevel = self.zoom_amount

        else:
            # Amount of zoom is a relative amount
            myzoomlevel = zoomlevel + self.zoom_amount

        self.logger.debug("zoomlevel=%d myzoom=%d" % (
            zoomlevel, myzoomlevel))
        self.zoomimage.zoom_to(myzoomlevel)
        return True

    def showzoom(self, data_x, data_y):
        # set the zoom image if we're not too close to the edge

        self.fv.gui_do(self._zoom_data, self.zoomimage, data_x, data_y)

    def magnify_xy(self, fitsimage, data_x, data_y):
        # Show zoom image in zoom window
        self.zoom_x, self.zoom_y = data_x, data_y

        # If this is a new source, then update our widget with the
        # attributes of the source
        if self.fitsimage_focus != fitsimage:
            chname = self.fv.get_channel_name(fitsimage)
            channel = self.fv.get_channel(chname)
            self.update_zoomviewer(channel)

        # If the refresh interval has expired then update the zoom image;
        # otherwise (re)set the timer until the end of the interval.
        cur_time = time.time()
        elapsed = cur_time - self.update_time
        if elapsed > self.refresh_interval:
            # margin 'd' is set by experimentation.
            d = 100 / self.zoom_amount
            xlo, ylo, xhi, yhi = self.currentFrame[self.active].dim
            if ((xhi is not None
                 and (0+d < data_x < xhi-d)
                 and (0+d < data_y < yhi-d))):

                self.zoomtask.clear()
                # cancel timer
                self.showzoom(data_x, data_y)

            canvas = self.canvas[self.fv.get_channel_name(fitsimage)]
            self.fv.gui_do(self.motion_slice_cb, canvas, None, data_x, data_y)
        else:
            # store needed data into the timer
            self.zoomtask.data.setvals(data_x=data_x, data_y=data_y)
            # calculate delta until end of refresh interval
            period = self.refresh_interval - elapsed
            # set timer
            self.zoomtask.cond_set(period)
        return True

    def motion_zoom_cb(self, fitsimage, button, data_x, data_y):

        if not self.gui_up:
            return
        self.magnify_xy(fitsimage, data_x, data_y)
        return False

    def motion_slice_cb(self, canvas, event, data_x, data_y):

        # update HV slice while moving over the image
        if not self.HVslice_visible:
            return

        # if we've switched channels, remove the cursor from the prior
        if canvas.name != self.slice_chname:
            canvas_old = self.canvas[self.slice_chname]
            if canvas_old is not None:
                if canvas_old.has_tag(SLICETAG_C):
                    canvas_old.delete_object_by_tag(SLICETAG_C)
            self.slice_chname = canvas.name

        # get the data shape
        y_size, x_size = self.fitsimage[self.active].get_vip().get_shape()
        if data_x < 0 or data_x > x_size or data_y < 0 or data_y > y_size:
            # out of bounds, remove line
            if canvas.has_tag(SLICETAG_C):
                canvas.delete_object_by_tag(SLICETAG_C)
            return

        if self.slice_axis == 'horizontal':
            # horizontal slice
            x1, x2 = 0, x_size-1
            y1, y2 = data_y, data_y
            xpos, ypos = x_size/2, data_y
        else:
            # vertical slice
            x1, x2 = data_x, data_x
            y1, y2 = 0, y_size-1
            xpos, ypos = data_x, y_size/2

        # create a new cursor if we don't have one,
        # otherwise move it.
        if not canvas.has_tag(SLICETAG_C):
            line = self.dc.Line(x1, y1, x2, y2, color=self.drawcolour,
                                showcap=False, alpha=ALPHA_CURSOR)
            canvas.add(line, tag=SLICETAG_C)
        else:
            line = canvas.get_object_by_tag(SLICETAG_C)
            line.move_to(xpos, ypos)

        canvas.redraw(whence=5)

        if self.liveslice:
            chname = canvas.name
            fitsimage = self.fitsimage[chname]
            self.slice_coords = (x1, y1, x2, y2)
            self.plot_slice(fitsimage)

        return

    def redo(self, channel, image):

        if not self.gui_up:
            return
        elif self.fv.get_current_channel() is not None:
            if self.examine is None:
                self.examine = self.load_examine() if HAVE_EXAMINE else False
            #    if self.examine:
            #        self.examine.set_status = self.set_status
            if self.cuts is None:
                self.cuts = self.load_seq_cuts() if HAVE_CUTS else False
            return

        fitsimage = channel.fitsimage
        if fitsimage != self.fv.getfocus_viewer():
            return True

    def redraw_zoom_cb(self, fitsimage, whence):

        if not self.gui_up:
            return
        if self.fitsimage_focus != fitsimage:
            return
        self.fitsimage_focus = None
        data_x, data_y = fitsimage.get_last_data_xy()[:2]
        self.magnify_xy(fitsimage, data_x, data_y)
        return False

    def update_zoomviewer(self, channel):

        fitsimage = channel.fitsimage
        self.fitsimage_focus = fitsimage
        # Reflect transforms, colormap, etc.
        fitsimage.copy_attributes(self.zoomimage, self.copy_attrs)

        p_canvas = self.zoomimage.get_private_canvas()
        try:
            p_canvas.delete_object_by_tag(self.layer_tag)
        except Exception:
            pass
        canvas = fitsimage.get_canvas()
        p_canvas.add(canvas, tag=self.layer_tag)
        # NOTE: necessary for zoom viewer to correctly handle some settings
        # TODO: see if there is a cleaner way to do this
        self.zoomimage._imgobj = fitsimage._imgobj

        self.zoomimage.redraw(whence=0)

    def zoomset(self, setting, zoomlevel, fitsimage):
        text = self.fv.scale2text(self.zoomimage.get_scale())
        self.w.zoom.set_text(text)

    def zoomset_cb(self, setting, zoomlevel, fitsimage):
        """This method is called when a main FITS widget changes zoom level.
        """
        if not self.gui_up:
            return
        fac_x, fac_y = fitsimage.get_scale_base_xy()
        fac_x_me, fac_y_me = self.zoomimage.get_scale_base_xy()
        if (fac_x != fac_x_me) or (fac_y != fac_y_me):
            alg = fitsimage.get_zoom_algorithm()
            self.zoomimage.set_zoom_algorithm(alg)
            self.zoomimage.set_scale_base_xy(fac_x, fac_y)
        return self._zoomset(self.fitsimage_focus, zoomlevel)

    ###########################################################################
    # Cuts bits

    def load_seq_cuts(self):
        cuts_tool = cuts.Cuts(self.fv, self.fitsimage, self.canvas,
                              self.active)

        # build the widget
        cuts_tool.build_gui(self.cuts_vbox)

        return cuts_tool

    def plot_slice(self, fitsimage):

        # plot the H or V slice

        # Get data
        image = fitsimage.get_vip()
        points = np.array(image.get_pixels_on_line(*self.slice_coords))

        rgb = colors.lookup_color("blue")
        self.hv_plot.clear()

        # adjustments for vertical or horizontal slice
        chname = fitsimage.name.split(':')[1]
        dim = self.viewport_dim[chname]
        lo, hi = ((dim[0], dim[2]) if self.slice_axis == 'horizontal'
                  else (dim[1], dim[3]))
        self.hv_plot.ax.set_xlim([lo, hi], auto=False)

        # plot the slice
        points_sub = points[int(lo):int(hi)]
        vlo, vhi = np.min(points_sub), np.max(points_sub)
        margin = 0.05*(vhi-vlo)
        self.hv_plot.ax.set_ylim([vlo-margin, vhi+margin], auto=False)
        self.hv_plot.cuts(points, xtitle="Line Index", ytitle="Pixel Value",
                          color=rgb)

    ###########################################################################
    # Examine bits

    def load_examine(self):

        examine_tool = examine.Examine(self.fv, self.fitsimage, self.canvas,
                                       self.active, self.currentFrame,
                                       set_status=self.set_status,
                                       logger=self.logger)

        # build the widget
        examine_tool.build_gui(self.examine_vbox)

        return examine_tool

    def build_dialogue_HVslice(self, parent):

        # Show a popup for HVslice

        hvslices_dialogue = Widgets.Dialog(title="Seqplot HV slice display",
                                           parent=parent)
        hvslices_dialogue.widget.setWindowFlag(Qt.WindowStaysOnTopHint)
        hvslices_dialogue.add_callback('close', lambda w, state=False:
                                       self.toggle_slice_dialogue(w, state))

        dialogueBox = hvslices_dialogue.get_content_area()
        dialogueBox.set_border_width(6)

        # stupid hack to keep the first button from being highlighted.
        btn = Widgets.ToggleButton(text='blah')
        dialogueBox.add_widget(btn)
        btn.hide()

        vbox = Widgets.VBox()

        dialogueBox.add_widget(vbox)

        size = 200  # 150
        self.hv_plot = plots.CutsPlot(logger=self.logger, width=size,
                                      height=size)
        self.hv_plot.fontsize = 7
        self.hv_plotWidget = Plot.PlotWidget(self.hv_plot)
        self.hv_plotWidget.resize(size, size)
        ax = self.hv_plot.add_axis()
        ax.grid(True)

        vbox.add_widget(self.hv_plotWidget, stretch=1)

        hbox = Widgets.HBox()

        w_oneshotslice_btn = Widgets.ToggleButton(text="One shot")
        w_oneshotslice_btn.add_callback('activated', self.toggle_slice_oneshot)
        w_oneshotslice_btn.set_tooltip('slice data on left mouse click')
        hbox.add_widget(w_oneshotslice_btn)
        self.oneshotslice_btn = w_oneshotslice_btn

        w_liveslice_btn = Widgets.ToggleButton(text="Live")
        w_liveslice_btn.add_callback('activated', self.toggle_slice_live)
        w_liveslice_btn.set_tooltip('Toggle live slice display mode.\n'
                                    'May experience some lag.')
        hbox.add_widget(w_liveslice_btn)
        self.liveslice_btn = w_liveslice_btn

        w_sliceguide_btn = Widgets.ToggleButton(text="Guides")
        w_sliceguide_btn.add_callback('activated', lambda w, s:
                                      self.toggle_slice_guide(s))
        w_sliceguide_btn.set_tooltip('Toggle slice guides.')
        w_sliceguide_btn.set_state(self.HVslice_visible)
        hbox.add_widget(w_sliceguide_btn)
        self.sliceguide_btn = w_sliceguide_btn

        hvmode = 'horizontal'

        hbox.add_widget(Widgets.Label(), stretch=1)
        btn = Widgets.RadioButton("Horizontal")
        btn.set_state(hvmode == 'horizontal')
        btn.add_callback('activated',
                         lambda w, v: self.set_slice_axis_cb('horizontal', v))
        btn.set_tooltip("Set quick plot for horizontal cut")
        hbox.add_widget(btn, stretch=0)
        self.slice_hor_btn_btn = btn

        btn = Widgets.RadioButton("Vertical")
        btn.set_state(hvmode == 'vertical')
        btn.add_callback('activated', lambda w, val:
                         self.set_slice_axis_cb('vertical', val))
        btn.set_tooltip("Set quick plot for vertical cut")
        hbox.add_widget(btn, stretch=0)
        self.slice_ver_btn_btn = btn

        hbox.add_widget(btn, stretch=0)
        close_btn = Widgets.Button(text="Close")
        close_btn.add_callback('activated', lambda w, state=False:
                               self.toggle_slice_dialogue(w, state))

        close_btn.set_tooltip('Close HV slice dialogue window.')
        hbox.add_widget(close_btn)

        vbox.add_widget(hbox)

        return hvslices_dialogue

    ###########################################################################
    # GUTS AND CALLBACKS

    def add_channel(self, viewer, channel):
        """
        Callback from the reference viewer shell when a channel is added.
        """

        if not self.gui_up:
            return
        self.prepare_zoom(channel.fitsimage)

        channel.settings['autocuts'] = 'on'
        channel.settings['autozoom'] = 'off'
        channel.settings['autocenter'] = 'off'

        return True

    def add_path(self, button):

        path = str(QFileDialog.getExistingDirectory(None, "Select Directory"))
        if os.path.exists(path):
            prefix = os.path.split(path)[-1]
            if path not in self.autoload_paths:
                self.autoload_paths.append(path)
                # If we are in autoload, start monitoring this path
                if self.autoload:
                    self.toggle_autoload(self.btn_auto, True)
            if prefix not in DATA_PATHS.keys():
                DATA_PATHS[prefix] = path

            index = self.combobox_source.widget.findText(prefix)
            if index < 0:
                self.combobox_source.append_text(prefix)

            # If this is the first path, set it as default.
            if len(DATA_PATHS) == 1:
                self.datapath = path

    def adjust_cuts_scroll(self, plot, event):
        """Adjust the width of the histogram by scrolling.
        """
        fitsimage = self.fitsimage[self.active]
        fitsimage = self.channel[self.active].fitsimage
        bm = fitsimage.get_bindings()
        pct = -self.scroll_pct
        if event.step > 0:
            pct = -pct
        bm.cut_pct(fitsimage, pct)

    def adjust_IFUoffset(self, offset):
        # set offset for IFU collapse
        self.IFUoffset[self.IFUinst] = self.IFUoffset[self.IFUinst] + offset
        self.display_image()

    def analyse_IFU(self, *args):

        # Plugin the IFU analysis module to analyse the current IFU flat
        # and write the parameters and plot to file.

        # we have to analyse the un-collapsed image.
        # Get the raw filepath:
        raw_prefix, raw_path, raw_exists = self.get_fitspath()

        if raw_exists:
            self.IFUoffset[self.IFUinst] = 0
            axis = IFU_axis[self.inst]
            task = Task(funcname="Analyse IFU",
                        initmsg=f"Analysing IFU flat {raw_prefix}",
                        finimsg=f"Analysis of IFU flat {raw_prefix} complete.",
                        function=IFU.analyse_IFU_flat,
                        input_path=raw_path,
                        parampath=IFUparampath,
                        collapse_axis=axis,
                        results=IFU_params[self.IFUinst],
                        completed=self.analyse_IFU_complete,
                        logger=self.logger,
                        IFU_name=self.IFUinst,
                        plots=False  # can't do matplotlib plots in a thread?
                        )
            self.spawn_thread(task)
        return

    def analyse_IFU_complete(self):
        self.btn_integIFU.set_state(True)
        self.integIFU = True
        self.display_image(reset_display=True)

    def change_channel(self, chname):
        self.fv.change_channel(chname)
        if self.inst is None:
            return
        if self.viewmode == 'grid':
            self.wInfo.imname.set_text(self.currentFrame[self.active].imname)

            try:
                if ((type(self.sub_fitspath[self.inst]) is list and
                     len(self.sub_fitspath[self.inst]) == 2)):
                    idx = CHNAME_LIST.index(self.active)
                    prefix = get_fitsprefix(self.sub_fitspath[self.inst][idx])
                    self.entry_sub.set_text(prefix)
            except Exception as e:
                self.logger.warning(f'Cannot change channel: {e}')

    def change_data_source(self, widget, index):
        source = widget.get_text()

        if self.datapath == DATA_PATHS[source]:
            # no change, do nothing
            return

        self.datapath = DATA_PATHS[source]
        self.visitor = source if source in VISITOR_PATHS.keys() else False

        # First check to see if we can switch to the same number -
        name, path, exists = self.get_fitspath()
        if not exists:
            # see if we can switch to last image for that date
            fitslist = self.find_fits(key=f"*{self.im_UTCdate}")
            if len(fitslist) > 0:
                candidate = fitslist[-1]
            else:
                # else switch to last image available.
                fitslist = self.find_fits()
                if len(fitslist) > 0:
                    candidate = fitslist[-1]
            self.im_UTCdate, self.im_frame = get_date_frame(candidate)

        if self.visitor:
            # Because visitor instrument names can be weird
            self.display_prefix = get_fitsprefix(candidate)
            self.display_fitspath = candidate

        self.display_image()

    def clean_IR(self, hdul):

        # Set up the task to perform an IR clean on the image.
        # Return False if a) waiting for clean or b) error.
        # Return full path of cleaned image otherwise

        prefix = self.currentFrame[self.active].prefix

        # We need to check for the right header cards
        hdr = hdul[0].header
        if (((hdr["INSTRUME"] == "GNIRS" and ("DECKER" not in hdr or
                                              "CAMERA" not in hdr)) or
             (hdr["INSTRUME"] == "NIRI" and ("FPMASK" not in hdr)))):

            self.clean = False
            self.btn_cleanIR.set_state(False)
            errmsg = (f"Disabling clean: {prefix} has incomplete header.")
            self.logger.warning(errmsg)
            self.set_status(errmsg)
            return False

        # put a copy of raw into cln_hdul for processing and re-writing
        cF = self.currentFrame[self.active]
        cF.cln_hdul = copy_FITS(hdul)

        # Load the cleaned fits image file if it exists,
        # if not, trigger the clean algorithm and return.
        # Clean will trigger a display_image when it completes.

        task = Task(funcname="Clean IR",
                    initmsg=f"Cleaning IR image {prefix}",
                    finimsg=f"Cleaning IR image {prefix} complete.",
                    function=cleanIR3.cleanFits_hdu,
                    hdulist=cF.cln_hdul,
                    output_hdul=cF.cln_hdul,
                    quadlevel=True,
                    rowfilter=True,
                    return_hdul=True,
                    logger=self.logger,
                    completed=self.display_image,
                    progMax=0
                    )
        self.spawn_thread(task)

        # return to event loop, thread will trigger display when finished
        return False

    def close(self):
        self.autoload = False

        if self.examine is not None:
            if self.examine.db is not None:
                self.examine.db.close()

        self.dialogue_IFU.widget.hide()
        self.fv.stop_global_plugin(str(self))
        return True

    def close_IFU_menu(self, button):
        # Close the IFU menu and toggle the IFU menu state button False

        self.dialogue_IFU.widget.hide()
        self.btn_IFUmenu.set_state(False)

    def configure_canvas(self, channel):

        self.logger.debug(f"configure_canvas: channel.name = {channel.name}")
        try:
            fitsimage = channel.fitsimage

            canvas = self.dc.DrawingCanvas()
            canvas.name = channel.name

            canvas.enable_draw(True)
            canvas.enable_edit(True)

            # add an event to check status
            canvas.add_callback('button-press', self.set_active_cb)

            # callback for self.motion_slice_cb has been removed.  I've added
            # the call insde the magnify_xy call, so that the updates for the
            # zoom and slice cursor happen at the same time.

            fitsimage.add_callback('redraw', self.scroll_pan_cb)

            canvas.add_draw_mode('move-hist', down=self.drag,
                                 move=self.drag, up=self.update)
            canvas.add_draw_mode('move-slice', down=self.take_slice_cb,
                                 drag=None, up=None)
            if self.examine is not None:
                canvas.add_draw_mode('move-examine',
                                     down=self.examine.btn_down,
                                     move=self.examine.btn_drag,
                                     up=self.examine.btn_up)

            if self.cuts is not None:
                canvas.add_draw_mode('move-cuts', down=self.cuts.buttondown_cb,
                                     move=self.cuts.motion_cb,
                                     up=self.cuts.buttonup_cb,
                                     key=self.cuts.keydown)

            canvas.add_draw_mode('navigate', down=self.navigate_down,
                                 move=self.navigate_drag, up=self.navigate_up)

            canvas.register_for_cursor_drawing(fitsimage)
            canvas.set_surface(fitsimage)

            if channel.name == CHNAME_A:
                self.set_cursor_mode('histogram', canvas=canvas)
                self.statsdraw_btn.set_state(True)
            else:
                self.set_cursor_mode(self.cursor_mode, canvas=canvas)

            canvas.ui_set_active(True, viewer=fitsimage)

        except Exception as e:
            self.logger.error(f"canvas configuration error: {e}")

        return canvas

    def configure_MEF_cube(self, ext, plane=None):
        # Configure the MEF dialogue cube slider and entry

        cF = self.currentFrame[self.active]
        data = cF.raw_hdul[ext].data
        cube = False
        len_axis_3 = 1
        if cF.raw_hdul[ext].data.ndim > 2:
            cube = True
            if 'NAXIS3' in cF.headers[ext]:
                len_axis_3 = cF.headers[ext]['NAXIS3']
            else:
                len_axis_3 = np.ma.size(data, axis=0)
        else:
            cF.plane = None

        self.MEF_cubeSlider.set_enabled(len_axis_3 > 1)
        self.MEF_cubeSpinbox.set_enabled(len_axis_3 > 1)
        self.MEF_cubeSpinbox.set_limits(0, len_axis_3-1)
        upper_limit = len_axis_3-1 if len_axis_3 > 1 else 2
        self.MEF_cubeSlider.set_limits(0, upper_limit)

        # Take keyword over cF
        plane = plane if plane is not None else cF.plane
        self.MEF_cubeSlider.set_value(plane)
        self.MEF_cubeSpinbox.set_value(plane)

        return cube

    def configure_MEF_exts(self, cF):

        # Check for image extensions

        for ext, hdu in enumerate(cF.raw_hdul):
            if ((type(hdu) == fits.hdu.image.ImageHDU
                 or type(hdu) == fits.hdu.compressed.CompImageHDU)):
                if self.inst == IGRINS2 and hdu.data.ndim < 3:
                    # if IGRINS, only include the cubes here.
                    continue
                cF.image_exts.append(ext)
        if len(cF.image_exts) == 0:
            msg = ('No extensions of type ImageHDU found.')
            raise NoImageExtensionException(msg)

        MEF = False
        if len(cF.image_exts) > 1:
            MEF = True
            # Configure the combobox
            self.MEF_extCombobox.clear()
            for ext in cF.image_exts:
                t = str(type(cF.raw_hdul[ext])).split('.')[-1]
                t = t.replace("\'>", '')
                shape = cF.raw_hdul[ext].shape
                entry = f'{ext}: {t} {shape}'
                self.MEF_extCombobox.append_text(entry)

                if len(shape) == 3:
                    upper_limit = shape[0] - 1
                    self.MEF_cubeSpinbox.set_limits(0, upper_limit)
                    self.MEF_cubeSlider.set_limits(0, upper_limit)
                    self.MEF_cubeMaxLabel.set_text(f'{upper_limit}')

        self.MEF_extCombobox.set_enabled(MEF)
        self.toggle_dialogue_MEF(None, MEF)

        return

    def btn_drag(self, canvas, event, data_x, data_y, viewer):

        if self.pick_obj is not None:
            obj = self.pick_obj
            if obj.kind != 'compound':
                return False

            shape = obj.objects[0]
            shape.move_to(data_x, data_y)
            self.canvas.update_canvas()

            return True

        return False

    def clear_slices(self):
        # Clear all slices from channels, typically when fitsimage is changed

        for canvas in self.canvas.values():
            if canvas is not None:
                for tag in [SLICETAG_C, SLICETAG_M]:
                    if canvas.has_tag(tag):
                        canvas.delete_object_by_tag(tag)

        # Clear the slice plot
        self.hv_plot.clear()

    def clear_tags(self):
        # Clear all canvas objects that are not slices or statsbox

        keeptags = [SATTAG, STATSTAG]
        for canvas in self.canvas.values():
            if canvas is not None:
                try:
                    for tag in canvas.tags:
                        if tag not in keeptags:
                            try:
                                canvas.delete_object_by_tag(tag)
                            except Exception as e:
                                self.logger.error(f"Error deleting tags: {e}")
                except Exception as e:
                    self.logger.error(f"Error deleting tags: {e}")

    def configure_multi_display(self, multi_channel):
        # Create or delete a second channel when we switch back and forth from
        # dual display mode.  We could just create a second channel and leave
        # it, but that leaves a spare channel lying around and that's untidy.

        multi_channel = [0] if multi_channel is False else multi_channel
        num_chan = len(multi_channel)-1

        for idx, chname in enumerate(CHNAME_LIST[1:]):
            if idx < num_chan:
                if self.channel[chname] is None:
                    channel = self.fv.add_channel(chname)
                    channel.viewer.set_color_map(DEFAULT_COLORMAP)
                    # Configure a drawing canvas
                    self.fitsimage[chname] = channel.fitsimage
                    self.canvas[chname] = self.configure_canvas(channel)
                    fimage = self.fitsimage[chname]
                    self.canvas[chname].ui_set_active(True, viewer=fimage)
                    self.channel[chname] = channel
            else:
                # Delete the second channel
                if self.channel[chname] is not None:
                    self.fv.delete_channel(chname)
                    self.channel[chname] = None
                    self.fitsimage[chname] = None

    def configure_gui(self, display_prefix, hdr, instrument):

        # autoload isn't setting states for visitor instruments:
        # do a check here to make sure GUI reflects appropriate vis status

        ################################
        # Enable/disable options based on instrument.
        # Set options false when deactivated.

        # activate correct sky subtraction options on instrument change
        if instrument != self.inst:
            self.sub = False
            self.btn_sub_toggle.set_state(False)
            self.btn_sub_toggle.widget.setText(f"{instrument} subtraction")

            if instrument in VISITOR_PATHS.keys():
                self.combobox_source.widget.setCurrentText(instrument)

        if instrument not in self.subFits.keys():
            subFitsDict = generate_chname_dict(class_name=FrameObject)
            self.subFits[instrument] = subFitsDict

        self.entry_sub.set_text(self.subFits[instrument][self.active].prefix)

        self.inst = instrument

        # activate correct IR clean options
        if ((any(inst in instrument for inst in ['NIRI', 'GNIRS'])
             and have_clean and not self.visitor)):

            self.btn_cleanIR.set_enabled(True)
        else:
            self.btn_cleanIR.set_enabled(False)
            self.btn_cleanIR.set_state(False)

        # activate correct IFU integration options
        self.IFUinst = "NOT IFU"
        if self.IFUoverride is not False:
            self.IFUinst = self.IFUoverride
            self.set_IFU_enabled(True)
        elif 'NIFS' in instrument and have_IFUcollapse:
            if 'OBSMODE' in hdr and "IFU" in hdr['OBSMODE']:
                self.set_IFU_enabled(True)
                self.IFUinst = "NIFS"
        elif 'GNIRS' in instrument and have_IFUcollapse:
            try:
                decker = hdr['DECKER']
            except Exception:
                decker = ""
            if "HR-IFU" in decker:
                self.IFUinst = "GNIRS-HR"
                self.set_IFU_enabled(True)
            elif "LR-IFU" in decker:
                self.IFUinst = "GNIRS-LR"
                self.set_IFU_enabled(True)
            else:
                # it's GNIRS but NOT ifu
                self.set_IFU_enabled(False)
        else:
            # if it's neither NIFS or GNIRS:
            self.set_IFU_enabled(False)

        if (self.IFUinst != "NOT IFU"
            and (self.IFUinst not in IFU_params.keys()
                 or IFU_params[self.IFUinst] is None)):

            self.read_IFU_params()

        # only enable the IFU analyse if it's an IFU flat or on override
        flat = False if 'OBSTYPE' not in hdr else (hdr['OBSTYPE'] == "FLAT")
        IFUflat = (((self.IFUinst != "NOT IFU") and flat) or
                   (self.IFUoverride is not False))
        self.btn_analyseIFU.set_enabled(IFUflat)

        return

    def cycle_drawcolour(self, button):

        index = COLORLIST.index(self.drawcolour)
        index = (index+1) % len(COLORLIST)
        self.drawcolour = COLORLIST[index]
        if LINUX_PLATFORM:
            button.widget.setStyleSheet(f"background-color:{self.drawcolour};"
                                        "color:black")
        else:
            button.widget.setStyleSheet(f"background-color:{self.drawcolour};"
                                        "border-radius: 4px;"
                                        "padding: 2px;"
                                        "min-width: 65px;"
                                        "color:black")
        for chname in CHNAME_LIST:
            if self.channel[chname] is None:
                continue
            canvas = self.canvas[chname]
            self.draw_statsbox_cb(canvas, STATSTAG,
                                  inactive=(chname != self.active),
                                  update_stats=False, check_canvas=False,
                                  chname=chname)

            # Slice tag
            self.redraw_slice(canvas)

    def delete_channel(self, viewer, channel):
        """
        Callback from the reference viewer shell when a channel is deleted.
        """

        return True
    """
    def display_image(self, hdul=None, reset_display=False):

        ## Display_image will act like the master controller.
        ## hdul is used to pass on an integrated IFU image

        chname = CHNAME_A
        set_grid = False

        cF = self.currentFrame[chname]

        ## start by making sure we're on one of the seqplot channels
        if self.active in CHNAME_LIST:
            self.change_channel(chname)

        ### Update image info -- if d_i is getting called, these are legit.
        date = self.im_UTCdate if self.im_UTCdate is not None else 'NONE'
        self.w_date_entry.set_text(date)
        frame = f'{self.im_frame:04d}' if self.im_frame is not None else 'NONE'
        self.w_frame_entry.set_text(frame)
            
        ## read in the header so that we can determine how to proceed
        if self.im_UTCdate is not None and self.im_frame is not None:
            display_prefix,display_fitspath,fits_exists = self.get_fitspath()
        else:
            display_fitspath = self.autoload_fitspath
            display_prefix = get_fitsprefix(display_fitspath)

        now = datetime.strftime(datetime.now(),"%H:%M:%S")

        old_prefix = ''
        ## if we've changed images, clear the raw and cleaned hduls from memory
        if display_prefix != cF.prefix:
            old_prefix = cF.prefix
            mode = self.canvas[chname].get_draw_mode()
            cF.reset()
            cF.prefix = display_prefix
            cF.fitspath = display_fitspath
            self.currentFrame[CHNAME_B].reset()

            self.w_override_combo.widget.setCurrentText('OVERRIDE')
            if self.IFUoverride:
                self.set_IFU_override(self.w_override_combo, 0, update=False)
            self.set_status(f"Loading image {cF.prefix}")
            self.logger.info(f"Loading image {cF.prefix}")

            if self.cuts is not None:
                self.cuts.delete_all_cb(None)

            self.canvas[chname].set_draw_mode(mode)

            self.clear_slices()
            
            if self.examine is not None:
                self.examine.clear_all()
            
        hdr0 = fits.getheader(display_fitspath)
        
        path = os.path.split(display_fitspath)[0]
        if 'INSTRUME' in hdr0:
            instrument = hdr0['INSTRUME']
        else:
            if path == VISITOR_PATHS['GRACES']:
                instrument = 'GRACES'
        ## switch datapath to path if it's changed (for autoload)
        if path != self.datapath:
            self.logger.info(f"Changing datapath = {path}")
            self.datapath = path

        obsID = None if "OBSID" not in hdr0 else hdr0["OBSID"]
        self.configure_gui(display_prefix, hdr0, instrument)

        ## If the observation or instrument or datashape has changed,
        ## reset the cuts, contrast, and histogram
        if (obsID != self.obsID or obsID is None or instrument != self.inst):
            reset_display = True
            self.obsID = obsID

        ext_list = [SCIEXT]

        ## here we're going to  iterate over HDUs or over files if dual
        ## (igrins/igrins-2)
        ## probably break into two loops so we can do two reads separately
        ## than one read.
        ## maybe do an initial readheader to determine instrument?
        
        if instrument == "IIWI":
            ext_list = [1, 2, 3]
            set_grid = True
            for chname in CHNAME_LIST:
                self.currentFrame[chname].fitspath = display_fitspath
        elif instrument == "IGRINS":
            ## If we haven't switched to grid yet, don't set until ready
            set_grid = True
            ext_list = [0, 0]
            
            ## Create a list of H and K images,  then recursively call
            ## display_image to plot the frames
            self.timeSomething()
            searchkey = display_prefix.replace('SDCH', 'SDC*')
            searchkey = searchkey.replace('SDCK', 'SDC*')
            filelist = self.find_fits(key=searchkey,
                                      path=VISITOR_PATHS['IGRINS'])

            ## sort the fitspaths in the right order:
            for fitspath in filelist:
                ## H Filter in A, K filter in B
                chname = CHNAME_A if 'SDCH' in fitspath else CHNAME_B
                self.currentFrame[chname].fitspath = fitspath
                self.currentFrame[chname].prefix = get_fitsprefix(fitspath)
        else:
            ## We're no longer doing IGRINS or another dual-file instrument
            if self.channel[CHNAME_B] is not None:
                ## If the B channel still exists, disable it.
                self.configure_multi_display(False)

        ## set tab viewmode as appropriate
        if not set_grid and self.viewmode == 'grid':
            self.fv.ds.get_ws('channels').configure_wstype('tabs')
            self.viewmode = 'tabs'
            self.channel[CHNAME_A].viewer.zoom_fit()

        ## 'channel change' callback is calling focus_cb unnecessarily,
        ## block until after image is displayed.
        self.fv.block_callback('channel-change')
        self.fv.block_callback('active-image')

        ### Display loop
        for chname, ext in zip(CHNAME_LIST[:len(ext_list)], ext_list):

            ## Remove the existing image
            if old_prefix != '':
                try:
                    self.fv.remove_image_by_name(chname, old_prefix)
                    self.logger.info(f'Image {old_prefix} removed from {chname}')
                except Exception as e:
                    self.logger.error(f'Error trying to remove {old_prefix} '
                                      f'from {chname}: {e}')

            cF = self.currentFrame[chname]
            
            msg = (f"displaying {instrument} image {cF.fitspath} extension "
                   f"{ext} on {chname}")
            self.logger.info(msg)
##            
            ## Read in the HDUL if no hdul specified or if we're on a higher
            ## channel and the fitspath is the same.
            if hdul is None or (self.currentFrame[chname].fitspath !=
                                self.currentFrame[CHNAME_A]):
                hdul_file = fits.open(self.currentFrame[chname].fitspath)
                cF.raw_hdul = copy_FITS(hdul_file)
                hdul_file.close()

                ## Copy the headers
                headers = []
                for hdu in cF.raw_hdul:
                    headers.append(hdu.header)
                cF.headers = headers

            else:
                self.logger.info("display_image: using hdul from memory")
                
            ## bailout for single-extension files
            if len(cF.raw_hdul) == 1:
                ext = 0
            
            ## Preprocessing
            self.preprocess_fits(cF, instrument, ext)
            self.logger.info(f"read and preprocessed {display_fitspath}")
            
            ## additional optional processing
            dat_hdu = self.process_fits(cF, instrument, chname=chname)
            if dat_hdu is False:
                ## this means that the process is churning, and will call
                ## display_image when it's done.  Return to the event loop.
                return

            ## Configure for grid display if we have a B Channel:
            if chname == CHNAME_B: # and self.viewmode != 'grid':
                self.configure_multi_display(ext_list)

            ### Display the image
            dat_img = AstroImage(logger=self.logger)
            dat_img.load_hdu(dat_hdu[ext])
            dat_img.set(name=display_prefix, path=None, nothumb=True)
            
            self.fv.add_image(display_prefix,dat_img, chname=chname)
            self.logger.info("Image displayed")
            
            ### Save image dimensions
            width, height = self.fitsimage[chname].get_vip().get_size()
            cF.dim = (0, 0, width-1, height-1)

            ### Display the overlay mask
            self.display_saturation_overlay(cF.sat_arr,cF.sat_val,chname=chname)
            self.canvas[chname].update_canvas(whence=0)

            if len(self.canvas[chname].get_objects()) == 0:
                ## if there are no objects, set up the full-frame.
                self.full_image_cb(chname=chname)

        ## set single (tab) viewmode as appropriate
        if set_grid and self.viewmode == 'tabs':
            self.fv.ds.get_ws('channels').configure_wstype('grid')
            self.viewmode = 'grid'

        self.update_image_info()

        self.fv.unblock_callback('channel-change')
        self.fv.unblock_callback('active-image')

        datashape = cF.data.shape
        if reset_display or datashape != self.datashape:
            self.clear_slices()
            self.zoom_to_fit()
            self.datashape = datashape
            self.clear_tags()
        else:
            self.update_stats()




    """
    def display_image(self, reset_display=False):

        # Display_image will act like the master controller.
        # hdul is used to pass on an integrated IFU image

        chname = CHNAME_A
        set_grid = False

        cF = self.currentFrame[chname]

        # start by making sure we're on one of the seqplot channels
        if self.active in CHNAME_LIST:
            self.change_channel(chname)

        # Update image info --
        # Either we will get info from self.im_UTCdate and self.im_frame, or
        # we get fitspath from self.autoload
        date = self.im_UTCdate if self.im_UTCdate is not None else 'NONE'
        self.entry_date.set_text(date)
        frame = f'{self.im_frame:04d}' if self.im_frame is not None else 'NONE'
        self.entry_frame.set_text(frame)

        # read in the header so that we can determine how to proceed
        if self.im_UTCdate is not None and self.im_frame is not None:
            display_prefix, display_fitspath, fits_exists = self.get_fitspath()
        else:
            display_fitspath = self.autoload_fitspath
            display_prefix = get_fitsprefix(display_fitspath)

        old_prefix = ''
        newimage = False
        # if we've changed images, clear the raw and cleaned hduls from memory

        if display_prefix != cF.prefix:
            newimage = True
            old_prefix = cF.prefix
            mode = self.canvas[chname].get_draw_mode()
            cF.reset()
            cF.prefix = display_prefix
            cF.fitspath = display_fitspath
            self.currentFrame[CHNAME_B].reset()

            self.combobox_IFUoverride.widget.setCurrentText('OVERRIDE')
            if self.IFUoverride:
                self.set_IFU_override(self.combobox_IFUoverride, 0,
                                      update=False)
            self.set_status(f"Loading image {cF.prefix}")
            self.logger.info(f"Loading image {cF.prefix}")

            if self.cuts is not None:
                self.cuts.delete_all_cb(None)

            self.canvas[chname].set_draw_mode(mode)

            self.clear_slices()

            if self.examine is not None:
                self.examine.clear_all()

        hdr0 = fits.getheader(display_fitspath)

        path = os.path.split(display_fitspath)[0]
        if 'INSTRUME' in hdr0:
            instrument = hdr0['INSTRUME']
        else:
            if 'GRACES' in VISITOR_PATHS and path == VISITOR_PATHS['GRACES']:
                instrument = 'GRACES'
            else:
                instrument = 'UNKNOWN'
        # switch datapath to path if it's changed (for autoload)
        if path != self.datapath:
            self.logger.info(f"Changing datapath = {path}")
            self.datapath = path

        obsID = None if "OBSID" not in hdr0 else hdr0["OBSID"]
        self.configure_gui(display_prefix, hdr0, instrument)

        # If the observation or instrument or datashape has changed,
        # reset the cuts, contrast, and histogram
        if (obsID != self.obsID or obsID is None or instrument != self.inst):
            reset_display = True
            self.obsID = obsID

        ext_list = [SCIEXT]

        # here we're going to  iterate over HDUs or over files if dual
        # (igrins/igrins-2)
        # probably break into two loops so we can do two reads separately
        # than one read.
        # maybe do an initial readheader to determine instrument?

        if instrument == IGRINS2:
            mef = True
            ext_list = IGRINS_EXTS
            set_grid = True
            for chname in CHNAME_LIST:
                self.currentFrame[chname].fitspath = display_fitspath
        elif instrument == "IGRINS":
            mef = False
            # If we haven't switched to grid yet, don't set until ready
            set_grid = True
            ext_list = [0, 0]

            # Create a list of H and K images,  then recursively call
            # display_image to plot the frames
            self.timeSomething()
            searchkey = display_prefix.replace('SDCH', 'SDC*')
            searchkey = searchkey.replace('SDCK', 'SDC*')
            filelist = self.find_fits(key=searchkey,
                                      path=VISITOR_PATHS['IGRINS'])

            # sort the fitspaths in the right order:
            for fitspath in filelist:
                # H Filter in A, K filter in B
                chname = CHNAME_A if 'SDCH' in fitspath else CHNAME_B
                self.currentFrame[chname].fitspath = fitspath
                self.currentFrame[chname].prefix = get_fitsprefix(fitspath)
        else:
            # We're no longer doing IGRINS or another dual-file instrument
            mef = False
            if self.channel[CHNAME_B] is not None:
                # If the B channel still exists, disable it.
                self.configure_multi_display(False)

        # set tab viewmode as appropriate
        if not set_grid and self.viewmode == 'grid':
            self.fv.ds.get_ws('channels').configure_wstype('tabs')
            self.viewmode = 'tabs'
            self.channel[CHNAME_A].viewer.zoom_fit()

        # 'channel change' callback is calling focus_cb unnecessarily,
        # block until after image is displayed.
        self.fv.block_callback('channel-change')
        self.fv.block_callback('active-image')

        # Display loop
        for chname, ext in zip(CHNAME_LIST[:len(ext_list)], ext_list):

            # Remove the existing image
            if old_prefix != '':
                try:
                    self.fv.remove_image_by_name(chname, old_prefix)
                    msg = f'Image {old_prefix} removed from {chname}'
                    self.logger.info(msg)
                except Exception as e:
                    self.logger.error(f'Error trying to remove {old_prefix} '
                                      f'from {chname}: {e}')

            cF = self.currentFrame[chname]

            msg = (f"displaying {instrument} image {cF.fitspath} extension "
                   f"{ext} on {chname}")
            self.logger.info(msg)

            # We only want to read in a new image if we haven't done it already
            # - An HDUL might have been passed to display_image
            # - We might be re-processing a previously loaded image
            # - We might be looking at another extension of an MEF

            if newimage:
                # read in the new image
                try:
                    hdul_file = fits.open(cF.fitspath)
                    cF.raw_hdul = copy_FITS(hdul_file)
                    hdul_file.close()

                    # Copy the headers
                    headers = []
                    for hdu in cF.raw_hdul:
                        headers.append(hdu.header)
                    cF.headers = headers

                    # bailout for single-extension files
                    if len(cF.raw_hdul) == 1:
                        ext = 0

                except Exception as e:
                    self.set_status(f'Unable to process {cF.fitspath}: {e}',
                                    error=True)
                    return

                self.preprocess_fits(cF, instrument, ext)

                self.configure_MEF_exts(cF)

                self.logger.info(f"read and preprocessed {display_fitspath}")
            else:
                self.logger.info("display_image: using hdul from memory")

            # Do preprocessing if it's a new image, or a new extension or
            # plane has been selected.

            # if this isn't a new image, check if extension has been changed
            if not newimage and ext != cF.ext:
                ext = cF.ext

            # Additional optional processing.
            # We always start with the raw data, and will store the
            # processed data in cF.data
            dat_hdu = self.process_fits(cF, instrument, chname=chname)
            if dat_hdu is False:
                # this means that the process is churning, and will call
                # display_image when it's done.  Return to the event loop.
                return

            # Configure for grid display if we have a B Channel:
            if chname == CHNAME_B:  # and self.viewmode != 'grid':
                self.configure_multi_display(ext_list)

            # Display the image
            dat_img = AstroImage(logger=self.logger)
            dat_img.load_hdu(dat_hdu[ext])
            dat_img.set(name=display_prefix, path=None, nothumb=True)

            self.fv.add_image(display_prefix, dat_img, chname=chname)
            self.logger.info("Image displayed")

            # Save image dimensions
            width, height = self.fitsimage[chname].get_vip().get_size()
            cF.dim = (0, 0, width-1, height-1)

            # Display the overlay mask
            self.display_saturation_overlay(cF.sat_arr, cF.sat_val,
                                            chname=chname)
            self.canvas[chname].update_canvas(whence=0)

            if len(self.canvas[chname].get_objects()) == 0:
                # if there are no objects, set up the full-frame.
                self.full_image_cb(chname=chname)

            # if this is an MEF file that we're displaying on a grid,
            # set flags so that we do not reread.
            if mef:
                newimage = False

        # set single (tab) viewmode as appropriate
        if set_grid and self.viewmode == 'tabs':
            self.fv.ds.get_ws('channels').configure_wstype('grid')
            self.viewmode = 'grid'

        self.update_image_info()

        self.fv.unblock_callback('channel-change')
        self.fv.unblock_callback('active-image')

        datashape = cF.data.shape
        if reset_display or datashape != self.datashape:
            self.clear_slices()
            self.zoom_to_fit()
            self.datashape = datashape
            self.clear_tags()
        else:
            self.update_stats()


    def display_saturation_overlay(self, sat_arr, sat_val, chname=CHNAME_A):

        # Always show mask by default
        self.w_satmask_btn.set_state(True)

        if sat_arr is None:
            self.currentFrame[chname].sat = ('HI', 0, 0)
            # create ... an array of np.NaN
            return
        else:
            nsat = np.count_nonzero(sat_arr)
            sat_perc = nsat/sat_arr.size * 100
            self.currentFrame[chname].sat = (sat_val, nsat, sat_perc)

        opacity = 0.6
        hi_color = 'red'
        rgb = colors.lookup_color(hi_color)
        rgba = (np.array(rgb + (opacity,)) * 255).astype(np.uint8)

        if self.sub:
            # if we are subtracting, then overlay saturation maps.
            sub_sat_arr = self.subFits[self.inst][chname].sat_arr
            sat_arr = np.logical_or(sat_arr, sub_sat_arr)
            nsat = np.count_nonzero(sat_arr)
            sat_perc = nsat/sat_arr.size
            self.currentFrame[chname].sat = (sat_val, nsat, sat_perc)

        rgb_obj = construct_sat_image(sat_arr, rgba)

        # set up the canvas and canvas object
        canvas = self.canvas[chname]

        # Check for image
        fitsimage = self.channel[chname].fitsimage
        image = fitsimage.get_vip()
        if image is None:
            return
        canvas.set_surface(fitsimage)

        # inserted from start
        p_canvas = fitsimage.get_canvas()
        try:
            p_canvas.get_object_by_tag(SATTAG)
        except KeyError:
            # Add canvas layer
            p_canvas.add(canvas, tag=SATTAG)

        if self.canvas_img[chname] is None:
            self.logger.debug("Adding saturation image to canvas")
            self.canvas_img[chname] = self.dc.Image(0, 0, rgb_obj)
        else:
            self.logger.debug("Updating canvas image")
            self.canvas_img[chname].set_image(rgb_obj)

        if not canvas.has_tag(SATTAG):
            canvas.add(self.canvas_img[chname], tag=SATTAG)

    def drag(self, canvas, event, data_x, data_y, viewer):

        if self.cursor_mode != 'histogram':
            return

        obj = canvas.get_object_by_tag(STATSTAG)
        if obj.kind == 'compound':
            bbox = obj.objects[0]
        elif obj.kind == 'rectangle':
            bbox = obj
        else:
            return True

        # calculate center of bbox
        wd = bbox.x2 - bbox.x1
        dw = wd // 2
        ht = bbox.y2 - bbox.y1
        dh = ht // 2
        x, y = bbox.x1 + dw, bbox.y1 + dh

        # calculate offsets of move
        dx = (data_x - x)
        dy = (data_y - y)

        # calculate new coords
        x1, y1, x2, y2 = bbox.x1 + dx, bbox.y1 + dy, bbox.x2 + dx, bbox.y2 + dy

        if obj.kind == 'compound':
            try:
                canvas.delete_object_by_tag(STATSTAG)
            except Exception:
                pass

            canvas.add(self.dc.Rectangle(x1, y1, x2, y2, color=self.drawcolour,
                                         linestyle='dash'), tag=STATSTAG)
        else:
            bbox.x1, bbox.y1, bbox.x2, bbox.y2 = x1, y1, x2, y2
            canvas.redraw(whence=3)

        return True

    def navigate_down(self, canvas, event, data_x, data_y, viewer):
        # reset the panning coordinate for drag
        self.pan_coord = [data_x, data_y]
        return

    def navigate_up(self, canvas, event, data_x, data_y, viewer):
        return

    def navigate_drag(self, canvas, event, data_x, data_y, viewer):
        dx = self.pan_coord[0]-data_x
        dy = self.pan_coord[1]-data_y
        if hasattr(viewer, 'pan_delta_px'):
            viewer.pan_delta_px(dx, dy)
        return

    def draw_statsbox_cb(self, canvas, tag, check_canvas=True,
                         inactive=False, update_stats=True, **kwargs):

        def get_dim(canvas, tag):
            obj = canvas.get_object_by_tag(tag)
            bbox = obj.objects[0] if obj.kind == 'compound' else obj
            x1, y1, x2, y2 = bbox.get_llur()
            return (x1, y1, x2, y2)

        chname = kwargs['chname'] if 'chname' in kwargs.keys() else self.active
        if inactive:
            update_stats = False

        # remove the box and tag that was just drawn
        if not canvas.has_tag(tag):
            self.logger.error(f"channel {canvas.name} chname {chname} "
                              "new tag {tag} does not exist")
            return

        # we might be called while in slice mode, so switch do draw.
        # save draw_mode to change back.
        draw_mode = canvas.get_draw_mode()
        if self.cursor_mode != 'histogram':
            self.set_drawtype('histogram', canvas)

        x1, y1, x2, y2 = get_dim(canvas, tag)
        canvas.delete_object_by_tag(tag)

        # if the box is too small, return
        if abs(x2-x1) < 4 or abs(y2-y1) < 4:
            return

        if not inactive:
            if self.currentFrame[chname].dim is not None:
                dim = self.currentFrame[chname].dim
            else:
                width, height = self.fitsimage[chname].get_vip().get_size()
                dim = (0, 0, width-1, height-1)
            ix1, iy1, ix2, iy2 = dim
            if x1 < ix1 or x2 > ix2 or y1 < iy1 or y2 > iy2:
                # If box is outside the dimensions, then return
                return

        # Remove the statstag object if possible

        if canvas.has_tag(STATSTAG):
            canvas.delete_object_by_tag(STATSTAG)

        # plot the new statsbox
        alpha = 1.0 if chname == self.active else 0.4
        lw = 2 if chname == self.active else 1
        canvas.add(self.dc.CompoundObject(
            self.dc.Rectangle(x1, y1, x2, y2, color=self.drawcolour,
                              linewidth=lw, alpha=alpha),
            self.dc.Text(x1, y2, "Statistics", color=self.drawcolour,
                         alpha=alpha, fontsize=10)),
                   tag=STATSTAG)
        self.w_movebox_radio.set_enabled(True)

        if self.active == chname and update_stats:
            self.update_stats(chname=chname)

        # Restore the original draw mode as appropriate
        if self.cursor_mode != 'histogram':
            self.set_drawtype('histogram', canvas)
            canvas.set_draw_mode(draw_mode)

        return

    def edit_cb(self, canvas, obj):

        if obj.kind != 'rectangle':
            return True

        # Get the compound object that sits on the canvas.
        # Make sure edited rectangle was our histogram rectangle.
        c_obj = canvas.get_object_by_tag(STATSTAG)
        if ((c_obj.kind != 'compound') or (len(c_obj.objects) < 2) or
                (c_obj.objects[0] != obj)):
            return False

        # reposition other elements to match
        x1, y1, x2, y2 = obj.get_llur()
        text = c_obj.objects[1]
        text.x, text.y = x1, y2 + 4
        self.fitsimage[self.active].redraw(whence=3)

        return self.update_stats()

    def edit_select_box(self):
        canvas = self.canvas[self.active]
        if canvas.has_tag(STATSTAG):
            obj = canvas.get_object_by_tag(STATSTAG)
            if obj.kind != 'compound':
                return True
            # drill down to reference shape
            bbox = obj.objects[0]
            canvas.edit_select(bbox)
        else:
            canvas.clear_selected()
        canvas.update_canvas()

    def find_fits(self, key=None, path=None):

        # We want to find all fits images matching a particular string
        # as of 2022/12/06, GN's 'dataflow' directory has some 200k files,
        # so we're going to streamline this.

        # Searchkey is DATE ONLY -- no site testing.

        path = self.datapath if path is None else path

        done = False
        utcNow = datetime.now(timezone('UTC'))

        while not done:
            if key is not None:
                fitskey = key
                done = True
            else:
                # search by day until we go and find a live file.
                datekey = utcNow.strftime('%Y%m%d')
                fitskey = (f"*{datekey}")

            searchkey = os.path.join(path, fitskey+"*.fits")
            list_of_files = glob.glob(searchkey)
            sorted_list = sorted(filter(os.path.isfile, list_of_files))

            if not done and len(sorted_list) > 0:
                done = True
            else:
                # go back to the previous month and try again.
                utcNow = utcNow - timedelta(days=1)

        return sorted_list

    def fit_channels(self, btn):

        for channel in self.channel.values():
            if channel is not None:
                channel.viewer.zoom_fit()

    def focus_cb(self, gingashell, channel):
        """
        Callback from the reference viewer shell when the focus changes
        between channels.
        """

        chname = channel.name
        skip = True if self.active is None else False
        if self.active != chname:
            # focus has shifted to a different channel than our idea
            # of the active one
            self.active = chname
            if not skip:
                self.set_inactive_channel()

        # change the displayed subtraction fitspath as appropriate
        # when instrument changes.

        try:
            prefix = self.subFits[self.inst][self.active].prefix
        except Exception:
            prefix = ""
        prefix = prefix if prefix is not None else ""

        if self.gui_up:
            self.entry_sub.set_text(prefix)

        return True

    def full_image_cb(self, **kwargs):
        # reset the statbox to be full frame and update stats.

        chname = kwargs['chname'] if 'chname' in kwargs.keys() else self.active
        canvas = self.canvas[chname]

        try:
            canvas.delete_object_by_tag(STATSTAG)
        except Exception:
            pass

        self.w_movebox_radio.set_enabled(False)

        canvas.add(self.dc.Rectangle(*self.currentFrame[chname].dim,
                                     color=self.drawcolour, linestyle='dash'),
                   tag=TEMPTAG)
        self.draw_statsbox_cb(canvas, TEMPTAG, chname=chname,
                              check_canvas=False)

    def get_channel_info(self, fits_image):
        chname = self.fv.get_channel_name(fits_image)
        channel = self.fv.get_channel(chname)

        return channel

    def get_data(self, image, x1, y1, x2, y2, z=0):
        tup = image.cutout_adjust(x1, y1, x2+1, y2+1, z=z)
        return tup[0]

    def get_fitspath(self, name=None, date=None, frame=None, path=None,
                     folder=None, visitor=False):

        # Construct filepath out of date and frame.
        if name is not None:
            date, frame = get_date_frame(name)

        date = self.im_UTCdate if date is None else date
        frame = self.im_frame if frame is None else frame

        if path is None:
            path = self.datapath

        if self.visitor == "IGRINS":
            fitsname = (f"SDCH_{date}_{frame:04d}.fits")
        else:
            fitsname = (f"*{date}*{frame:04d}.fits")
        fitspath = os.path.join(path, fitsname)

        fitslist = glob.glob(fitspath)
        if len(fitslist) == 1:
            fitspath = fitslist[0]
            fitsname = get_fitsprefix(fitspath)
            fits_exists = True
        else:
            fits_exists = False

        return (fitsname, fitspath, fits_exists)

    def histogram_data(self, data, pct=1.0, numbins=2048):
        return self.autocuts.calc_histogram(data, pct=pct, numbins=numbins)

    def integrate_IFU(self, hdul):

        # Check for IFU collapse parameters, and load if we haven't already
        if self.IFUinst not in IFU_params:
            self.read_IFU_params()

        # Only collapse if we have parameters
        if self.IFUinst in IFU_params:

            # Always re-collapse, even if an image already exists:
            # Adjusting offset will require overwriting.
            self.timeSomething(None)
            hdul = IFU.integrateIFU(hdulist=hdul, output_path=None,
                                    IFUoffset=self.IFUoffset[self.IFUinst],
                                    logger=self.logger,
                                    set_status=self.set_status,
                                    parameters=IFU_params[self.IFUinst])
            self.timeSomething("Time to integrate cube: {} s")

        return hdul

    def invoke_override_menu(self, btn_w):
        # Activate the override popup menu

        menu = Widgets.Menu()
        for item in ["NIFS", "GNIRS-LR", "GNIRS-HR"]:
            over = menu.add_name(item)
            over.add_callback('activated',
                              lambda w, mode=item: self.set_IFU_override(mode))
        over = menu.add_name("OVERRIDE OFF")
        over.add_callback('activated', lambda w: self.set_IFU_override(False))

        menu.popup(btn_w)

    def last_image(self, *args):
        # Display the most recent image for the date given

        # if 'NORMAL' exists, switch to normal
        if (('NORMAL' in DATA_PATHS.keys()
             and self.datapath != DATA_PATHS['NORMAL'])):
            self.datapath = DATA_PATHS['NORMAL']
            self.combobox_source.widget.setCurrentText('NORMAL')

        list_of_files = self.find_fits()
        if len(list_of_files) < 1:
            self.set_status("No files found")
            return

        most_recent = list_of_files[-1]
        self.im_UTCdate, self.im_frame = get_date_frame(most_recent)
        self.set_status(f"Most recent data from {self.im_UTCdate} UTC")
        self.logger.info(f"last_image: {most_recent}")

        self.display_image()

        return

    def load_file(self, button):

        fitspath = QFileDialog.getOpenFileName(None, "Select FITS")[0]

        path, fitsname = os.path.split(fitspath)
        if not os.path.exists(fitspath):
            return

        self.im_UTCdate = None
        self.im_frame = None

        self.autoload_fitspath = fitspath

        self.display_image()

        return

    def next_image(self, button):

        # Display the next image.
        # If next image does not exist, check for next day
        #  If no image is shown, show first image of current date.

        update = False

        if self.im_frame is None:
            frame = 1
        else:
            frame = self.im_frame+1

        fitsname, fitspath, exists = self.get_fitspath(date=self.im_UTCdate,
                                                       frame=frame)
        if exists:
            self.im_frame = frame
            update = True
        else:
            # first, look for the next image on that date:

            searchkey = (f"*{self.im_UTCdate}")
            list_of_files = self.find_fits(key=searchkey)
            key = self.currentFrame[self.active].prefix + '.fits'
            index = [i for i, s in enumerate(list_of_files) if key in s][-1]

            if index+1 < len(list_of_files):
                nextfilename = list_of_files[index+1]
                self.im_UTCdate, self.im_frame = get_date_frame(nextfilename)
                update = True

            else:
                # no more images on this day, let's look at the next day

                self.set_status("There are no later images for this date")
                done = False
                date = datetime.strptime(self.im_UTCdate, "%Y%m%d")
                date = date.replace(tzinfo=timezone('UTC'))
                while not done:
                    # increment the date:
                    date = date + timedelta(days=1)
                    if date < datetime.now(timezone('UTC')):
                        searchkey = (f"*{date.strftime('%Y%m%d')}")
                        list_of_files = self.find_fits(key=searchkey)
                        if len(list_of_files) > 0:
                            # nab the first one, and we're done!
                            nextfilename = list_of_files[0]
                            date, frame = get_date_frame(nextfilename)
                            self.im_UTCdate, self.im_frame = date, frame
                            update = True
                            done = True
                            self.set_status(f"Next image from {date} UTC")
                    else:
                        # we've caught up, no more files!  Return!
                        done = True

        if update:
            self.display_image()
        else:
            self.set_status("There are no later images available.")
        return

    def pause_display(self, w, state):

        # there are two pause buttons (main control and review menu).
        # make sure that both
        self.btn_autoPause.set_state(state)
        self.btn_reviewPause.set_state(state)

        # Grab or release the lock depending on state
        if state:
            self.set_status('Display paused')
            DISPLAY_MUTEX.lock()
        else:
            self.set_status('Display resumed')
            DISPLAY_MUTEX.unlock()

    def post_channel_initialisation(self):

        # we have some initialisation that needs to be completed after
        # channel creation, but before we enter the event loop.

        # This is executed from ginga_config.py

        channel = self.fv.get_current_channel()
        channel.viewer.set_color_map(DEFAULT_COLORMAP)

        self.channel[CHNAME_A] = channel
        self.fitsimage[CHNAME_A] = channel.fitsimage

        # Set up a canvas for saturation overlays and histograms if it
        # hasn't been done yet:
        self.dc = self.fv.get_draw_classes()
        self.canvas[CHNAME_A] = self.configure_canvas(channel)
        self.active = CHNAME_A

        if self.datapath is not None:
            # preload with the most recent image
            self.last_image()
            
            self.update_zoomviewer(channel)

            # turn on autoload if available:
            if have_watchdog:
                self.btn_auto.set_state(True)
                self.toggle_autoload(self.btn_auto, True)

        return

    def preprocess_fits(self, cF, instrument, ext):
        # preprocess_fits takes care of any initial actions (such as tiling)
        # that have to take place.

        cF.ext = ext
        cF.imname = cF.prefix

        if instrument.startswith('GMOS'):
            hdul, arr, val = self.tile_gmos_wrapper(cF.raw_hdul)
            cF.data = hdul[ext].data
            cF.raw_hdul = hdul
            cF.sat_val = val
            cF.sat_arr = arr
        elif instrument in IR_INSTRUMENTS:
            hdul = cF.raw_hdul
            cF.data = hdul[ext].data
            arr, val = calculate_ir_saturation(hdul, logger=self.logger)
            cF.sat_val = val
            cF.sat_arr = arr
        elif instrument == IGRINS2:
            sat_val = 0.90*22000
            # IGRINS2 has three science extensions, [1] - [3].  Copy here
            # instead of re-reading.

            for i, ext in enumerate(IGRINS_EXTS):
                chname = CHNAME_LIST[i]
                cFX = self.currentFrame[chname]

                cFX.raw_hdul = cF.raw_hdul
                cFX.headers = cF.headers
                cFX.ext = ext

                if cF.plane is not None and cF.raw_hdul[ext].data.ndim > 2:
                    cFX.data = cF.raw_hdul[ext].data[cF.plane]
                else:
                    cFX.data = cF.raw_hdul[ext].data
                cFX.image_exts = cF.image_exts
                cFX.sat_val = sat_val
                cFX.sat_arr = calculate_sat_array(cFX.data, cFX.sat_val)
                cFX.prefix = cF.prefix
                cFX.fitspath = cF.fitspath
                if 'BAND' in cFX.headers[ext]:
                    cFX.imname = f"{cFX.prefix} [{cFX.headers[ext]['BAND']}]"
                else:
                    # image is SVC
                    cFX.imname = f"{cFX.prefix} [S]"
        elif instrument == 'GRACES':
            hdul, arr, val = self.tile_graces(cF.raw_hdul)
            cF.raw_hdul = hdul
            cF.data = hdul[ext].data
            cF.sat_val = val
            cF.sat_arr = arr
        elif instrument == 'IGRINS':
            cF.data = cF.raw_hdul[0].data
            cF.sat_val = 0.90*22000
            cF.sat_arr = calculate_sat_array(cF.raw_hdul[0].data, cF.sat_val)
            cF.imname = f"{cF.prefix} [{cF.headers[0]['BAND']}]"
        elif instrument == 'F2':
            cF.data = cF.raw_hdul[1].data[0]
            cF.sat_val = 21000
            cF.sat_arr = calculate_sat_array(cF.raw_hdul[1].data[0],
                                             cF.sat_val)
        elif instrument == 'GSAOI':
            cF.data = cF.raw_hdul[ext].data
            cF.sat_val = 32000
            cF.sat_arr = calculate_sat_array(cF.raw_hdul[1].data, cF.sat_val)
        elif instrument == 'hrwfs':
            cF.data = cF.raw_hdul[0].data
            cF.sat_val = 65000
            cF.sat_arr = calculate_sat_array(cF.data, cF.sat_val)
        else:
            # Instrument is unknown: Find and display the first image extension
            cF.ext = ext
            if cF.plane is not None:
                cF.data = cF.raw_hdul[ext].data[cF.plane]
            else:
                cF.data = cF.raw_hdul[ext].data

            cF.sat_val = None
            cF.sat_arr = calculate_sat_array(cF.data, None)

        if instrument != 'UNKNOWN' and instrument != IGRINS2:
            if self.dialogue_MEF is not None:
                # Clear the MEF dialogue
                self.toggle_dialogue_MEF(None, False)

        return

    def prev_image(self, button):

        # Display previous image.  If no image is shown, show most recent
        # image of current date.

        if self.im_frame == 0:
            filelist = self.find_fits(f"*{self.im_UTCdate}*.fits")

            # extract frame from the path
            date, frame = get_date_frame(filelist[-1])
        else:
            frame = self.im_frame-1

        update = False

        name, path, exists = self.get_fitspath(date=self.im_UTCdate,
                                               frame=frame)
        if exists:
            self.im_frame = frame
            update = True
        else:
            # first, look for the previous-existing image on that date:
            searchkey = (f"*{self.im_UTCdate}")
            filelist = self.find_fits(key=searchkey)
            key = self.currentFrame[self.active].prefix+'.fits'
            index = [idx for idx, s in enumerate(filelist) if key in s][-1]

            if index-1 > 0:
                nextfilename = filelist[index-1]
                self.im_UTCdate, self.im_frame = get_date_frame(nextfilename)
                update = True

            else:
                # no more images on this day, let's look at the previous day
                # repeat for DATE_WINDOW days, then we'll take another approach

                self.set_status("There are no earlier images for this date")
                self.logger.info("Looking for earlier images on {searchkey}")
                done = False
                date = datetime.strptime(self.im_UTCdate, "%Y%m%d")
                date = date.replace(tzinfo=timezone('UTC'))

                counter = 0
                while not done and counter < DATE_WINDOW:
                    # decrement the date:
                    date = date - timedelta(days=1)
                    searchkey = (f"*{date.strftime('%Y%m%d')}")
                    filelist = self.find_fits(key=searchkey)
                    if len(filelist) > 0:
                        # nab the first one, and we're done!
                        nextfilename = filelist[-1]
                        date, frame = get_date_frame(nextfilename)
                        self.im_UTCdate, self.im_frame = date, frame
                        update = True
                        done = True
                        self.set_status(f"Previous image from {date} UTC")
                    counter += 1

            if not update:
                # we still haven't found a previous file.  Let's not search
                # forever -- do a glob.
                filelist = self.find_fits('')
                filelist.sort(key=os.path.getmtime)
                fitspath = self.currentFrame[self.active].fitspath
                idx = filelist.index(fitspath)-1
                if idx > -1:
                    date, frame = get_date_frame(filelist[idx])
                    self.im_UTCdate, self.im_frame = date, frame
                    update = True
                else:
                    msg = 'There are no earlier images in this directory'
                    self.set_status(msg)
        if update:
            self.display_image()

        return

    def process_fits(self, cF, instrument, chname=CHNAME_A):
        # process_fits will take care of all the optional image processing
        # to be done.
        # The following process steps do NOT change saturation maps

        # Initially point hdul to the raw unprocessed data:
        # we'll copy it if we need to.
        hdul = cF.raw_hdul

        # Select the proper plane if we have a cube
        if cF.plane is not None:  # != 0:
            hdul = copy_FITS(hdul)
            hdul[cF.ext].data = cF.raw_hdul[cF.ext].data[cF.plane]

            # calculate the saturation map for a new plane.  We
            # can only do this for IGRINS2 currently.
            # Unknown instruments do not currently have saturation values,
            # so no need to update.  We can fix this if needed.
            if self.inst == IGRINS2:
                cF.sat_val = 0.90*22000
                cF.sat_arr = calculate_sat_array(hdul[cF.ext].data, cF.sat_val)

        # IR CLEAN
        if self.clean:
            if cF.cln_hdul is not None:
                # if we have a clean hdul in memory, display that.
                hdul = cF.cln_hdul
            else:
                # if no clean hdul in memory, go off and clean
                self.clean_IR(hdul)
                return (False)

        # Sky subtraction.  This is returns a new array.
        if self.sub:
            hdul = self.subtract_frames(cF, chname=chname)
            cF.data = hdul[cF.ext].data

        # IFU INTEGRATION
        # If we have an image that can be integrated, integrate it.
        if self.btn_integIFU.widget.isEnabled():
            self.integrated_IFU_hdul = self.integrate_IFU(hdul)
            self.IFUimage.set_data(self.integrated_IFU_hdul[cF.ext].data)
        else:
            self.integrated_IFU_data = None

        # if self.swapIFU:
        if self.integIFU:

            self.IFUimage.set_data(hdul[cF.ext].data)
            hdul = self.integrated_IFU_hdul

            cF.data = hdul[cF.ext].data

        return hdul

    def read_IFU_params(self, *args, redisplay=False):

        # Reload IFU parameters in case of bad analysis

        fullparampath = os.path.join(IFUparampath,
                                     f"IFUorientation_{self.IFUinst}.csv")
        if os.path.exists(fullparampath):
            IFU_params[self.IFUinst] = IFU.read_IFUparams(fullparampath)
            self.IFUoffset[self.IFUinst] = 0
        self.set_status(f"{self.IFUinst} default IFU parameters restored.")

        if redisplay:
            self.display_image()

    def resume(self, **kwargs):
        # turn off any mode user may be in
        chname = kwargs['chname'] if 'chname' in kwargs.keys() else self.active
        self.canvas[chname].ui_set_active(True, viewer=self.fitsimage[chname])

    def get_viewport_dim(self, canvasview):

        (dx1, dy1), (dx2, dy2) = canvasview.get_limits()
        vx1, vy1, vx2, vy2 = canvasview.get_datarect()

        xlo = vx1 if vx1 > dx1 else dx1
        xhi = vx2 if vx2 < dx2 else dx2
        ylo = vy1 if vy1 > dy1 else dy1
        yhi = vy2 if vy2 < dy2 else dy2

        return (xlo, ylo, xhi, yhi)

    def redraw_slice(self, canvas):

        # Redraw slice in a new colour
        for tag in [SLICETAG_C, SLICETAG_M]:
            alpha = ALPHA_CURSOR if tag == SLICETAG_C else ALPHA_MARKER
            if canvas.has_tag(tag):
                coords = canvas.get_object_by_tag(tag).get_llur()
                canvas.delete_object_by_tag(tag)
                line = self.dc.Line(*coords, color=self.drawcolour,
                                    showcap=False, alpha=alpha)
                canvas.add(line, tag=tag)

    def scroll_pan_cb(self, canvasview, flag):

        # Currently, this only affects the slice plot.
        # Close if the slice plot is not visible or if there is no data

        if not self.dialogue_hvslice.widget.isVisible():
            return

        viewport_dim = self.get_viewport_dim(canvasview)
        chname = canvasview.name.split(':')[1]
        if viewport_dim != self.viewport_dim[chname]:
            self.viewport_dim[chname] = viewport_dim

            canvas = self.canvas[chname]
            if canvas.has_tag(SLICETAG_C):
                self.plot_slice(canvasview)

    def set_active_cb(self, canvas, button_pressed, x_pos, y_pos):
        # set the active and inactive channels based on a button press

        if self.active != canvas.name:
            chname = canvas.name
            self.active = chname
            self.change_channel(chname)
            self.set_inactive_channel()

    def set_bbox_cuts(self, button):
        # Set the cuts based on bbox hi and lo values

        if self.bbox_min is not None and self.bbox_max is not None:
            # self.fitsimage.autocuts holds the autocut algorithm
            # default (apparently) is zscale

            z = ZScaleInterval()
            lo, hi = z.get_limits(self.box_data)
            self.fitsimage[self.active].cut_levels(lo, hi)

            self.set_status("Setting cut levels on selected region")

    def set_current_as_sub(self, *args):

        # Sets the current image as the sky frame / sub frame
        # for the current instrument

        # The self.currentFrame object contains the pre-processed raw
        # images for display (and may contain cleaned if that has been done).
        # We always sub either raw or cleaned (never IFU).  So we should
        # just be able to take the currentFrame object and create a new handle
        # for it in self.subFits.

        # We *have* to create a copy of the currentFrame here, else
        # the data will reset as we move to a different image.
        # We have to copy carefully, as several channels may not exist.
        self.subFits[self.inst] = {}
        for ch in CHNAME_LIST:
            if self.channel[ch] is not None:
                currentFrame_ch = self.currentFrame[ch]
                self.subFits[self.inst][ch] = copy.deepcopy(currentFrame_ch)
        self.entry_sub.set_text(self.subFits[self.inst][self.active].prefix)
        self.sub = True
        self.btn_sub_toggle.set_state(True)

        self.display_image()

        return

    def set_drawtype(self, mode, canvas):

        # similar to set_cursor_mode, but changes only drawtype, not callbacks
        # or cursor interactions

        if mode == 'navigate':
            canvas.set_drawtype('rectangle', color=self.drawcolour,
                                linestyle=None, alpha=0.0)
        elif mode == 'histogram':
            canvas.set_drawtype('rectangle', color=self.drawcolour,
                                linestyle='dash', drawdims=True)
        elif mode == 'slice':
            canvas.set_drawtype(None, color=None, linestyle=None)
        elif mode == 'examine':
            canvas.set_drawtype(self.examine.pickshape, color='cyan',
                                linestyle='dash')
        elif mode == 'cuts':
            canvas.set_drawtype('line', color='cyan', linestyle='dash')

    def set_cursor_mode(self, mode, canvas=None):

        # Toggle the cursor mode between navigate, histogram, slice, examine

        mode = 'navigate' if mode == 'seqplot' else mode
        self.cursor_mode = mode

        # Set up lists for draw and edit events
        callbacks = {}
        callbacks['draw-event'] = [self.draw_statsbox_cb]
        callbacks['edit-event'] = [self.edit_cb]
        if self.cuts is not None:
            callbacks['draw-event'].append(self.cuts.draw_cb)
            callbacks['edit-event'].append(self.cuts.edit_cb)
        if self.examine is not None:
            callbacks['draw-event'].append(self.examine.draw_cb)
            callbacks['edit-event'].append(self.examine.edit_cb)
        callbacks['button-press'] = [self.take_slice_cb]

        # make sure that buttons are set correctly
        if mode != 'histogram':
            self.statsdraw_btn.set_state(False)
            self.w_drawbox_radio.set_enabled(False)
            self.w_editbox_radio.set_enabled(False)
            self.w_movebox_radio.set_enabled(False)
        else:
            self.w_drawbox_radio.set_enabled(True)
            self.w_editbox_radio.set_enabled(True)
            self.w_movebox_radio.set_enabled(True)

        if mode != 'slice' and self.sliceguide_btn.get_state() is True:
            # turn off slice guides
            self.toggle_slice_guide(False)

        canvases = [canvas] if canvas is not None else self.canvas.values()
        for canvas in canvases:
            if canvas is None:
                continue

            self.set_drawtype(mode, canvas)

            # Remove all active callbacks
            for key in callbacks.keys():
                for callback in callbacks[key]:
                    canvas.remove_callback(key, callback)

            # Add callbacks, set drawtype and draw mode appropriate for mode
            if mode == 'navigate':
                canvas.set_draw_mode('navigate')
            elif mode == 'histogram':
                canvas.set_callback('draw-event', self.draw_statsbox_cb)
                canvas.set_callback('edit-event', self.edit_cb)
                canvas.set_draw_mode('draw')
            elif mode == 'slice':
                canvas.set_draw_mode('move-slice')
            elif mode == 'examine':
                self.examine.set_canvas_actions(canvas)
                canvas.add_callback('button-press', self.examine.set_active_cb)
                canvas.set_draw_mode('move-examine')
            elif mode == 'cuts':
                self.cuts.set_canvas_actions(canvas)
                canvas.add_callback('button-press', self.cuts.set_active_cb)
                canvas.set_draw_mode('move-cuts')
            else:
                # mode is something else
                self.logger.error(f"bad mode: {mode}")

    def set_cut_by_click(self, plot, event):
        """Set cut levels by a mouse click in the histogram plot:
        left: set low cut
        middle: reset (auto cuts)
        right: set high cut
        """

        data_x = event.xdata
        fitsimage = self.fitsimage[self.active]
        lo, hi = fitsimage.get_cut_levels()
        if event.button == 1:
            lo = data_x
            fitsimage.cut_levels(lo, hi)
        elif event.button == 2:
            fitsimage.auto_levels()
        elif event.button == 3:
            hi = data_x
            fitsimage.cut_levels(lo, hi)

        self.update_histogram()

    def set_date(self):

        def is_date(string):
            # quick determination if a string is a date
            # returns a YYYYMMDD date if date, else False
            try:
                return (du_parse(string).strftime("%Y%m%d"))
            except Exception as e:
                self.set_status(f'Entry "{string}" is not a valid date.',
                                error=True)
                self.logger.error(e)
                return (False)

        # set the date or frame number directly from a text entry box.
        entry = self.entry_date.get_text()

        n_spaces = entry.count(' ')
        if n_spaces == 2 or n_spaces == 0:
            # assume that date is entered as YYYY MM DD or YYYYMMDD
            split = [entry.replace(' ', '')]
        else:     # if n_spaces == 1 or n__spaces == 3:
            # assume that frame NNN is specificed: entry is YYYYMMDD NNN
            # or YYYY-MM-DD NNN or YYYY MM DD NNN
            split = entry.rsplit(' ', 1)
        date, frame_input = split if len(split) > 1 else (split[0], None)

        # parse the date:
        date = is_date(date)
        if not date:
            self.set_status(f"Cannot parse {entry} as a viable date.")
            return

        # First assume that frame 1 exists to save time if frame not specified
        frame = int(frame_input) if frame_input is not None else 1
        frame = frame if frame != 0 else 1

        name, path, exists = self.get_fitspath(date=date, frame=frame)
        if exists:
            self.im_UTCdate = date
            self.im_frame = frame
        elif date <= datetime.now(timezone('UTC')).strftime("%Y%m%d"):
            # automatically switch to archive if we're on NORMAL
            if ((os.path.normpath(self.datapath) ==
                 os.path.normpath(DATA_PATHS['NORMAL']))):

                self.datapath = DATA_PATHS['ARCHIVE']
                self.combobox_source.widget.setCurrentText('ARCHIVE')

                # Check the archive for this date
                name, path, exists = self.get_fitspath(date=date, frame=frame)
                if exists:
                    self.im_UTCdate = date
                    self.im_frame = frame

            # if we still haven't found it, search for last or first on date
            if not exists:
                # find first (or last) image for that date
                searchkey = f"*{date}"
                files = self.find_fits(key=searchkey)
                if len(files) > 0:
                    idx = 0 if frame == 1 else -1
                    order = "first" if idx == 0 else "last"
                    self.im_UTCdate, self.im_frame = get_date_frame(files[idx])
                    exists = True
                    self.set_status(f"Frame {frame} does not exist, "
                                    f"selecting {order} available image")
                else:
                    self.set_status(f"No files exist for date {date}")
                    return
        else:
            # Check for future date because I want to be a smart aleck
            self.set_status("Telescopes see into the past, not the future.")

        # set values in the date and frame boxes
        if exists:
            self.set_status(f"Setting date {date} and frame {frame}")

            self.entry_date.set_text(self.im_UTCdate)
            self.entry_frame.set_text(f'{self.im_frame:04d}')

            self.display_image()

    def set_frame_number(self):

        def check(date, frame, datapath=None):
            datapath = self.datapath if datapath is None else datapath

            name, path, exists = self.get_fitspath(date=date, frame=frame,
                                                   path=datapath)
            if exists:
                self.im_UTCdate = date
                self.im_frame = frame
                # set values in the date and frame boxes
                self.entry_date.set_text(date)
                self.entry_frame.set_text(f'{self.im_frame:04d}')

            return exists

        # set the date or frame number directly from a text entry box.
        frameentry = self.entry_frame.get_text()
        date = self.entry_date.get_text()

        # assume entry is a NNN
        if frameentry.isnumeric():
            frame = int("%04d" % int(frameentry))

        name, path, exists = self.get_fitspath(date=date, frame=frame)
        if exists:
            self.im_UTCdate = date
            self.im_frame = frame
        elif date <= datetime.now(timezone('UTC')).strftime("%Y%m%d"):
            # automatically switch to archive if we're on NORMAL
            if ('NORMAL' in DATA_PATHS.keys() and
                (os.path.normpath(self.datapath) ==
                 os.path.normpath(DATA_PATHS['NORMAL']))):

                self.datapath = DATA_PATHS['ARCHIVE']
                self.combobox_source.widget.setCurrentText('ARCHIVE')

                # Check the archive for this date
                name, path, exists = self.get_fitspath(date=date, frame=frame)
                if exists:
                    self.im_UTCdate = date
                    self.im_frame = frame

            # if we still haven't found it, search for last or first on date
            if not exists:
                # find first (or last) image for that date
                searchkey = f"*{date}"
                files = self.find_fits(key=searchkey)
                if len(files) > 0:
                    idx = 0 if frame <= 1 else -1
                    order = "first" if idx == 0 else "last"
                    self.im_UTCdate, self.im_frame = get_date_frame(files[idx])
                    exists = True
                    self.set_status(f"Frame {frame} does not exist, "
                                    f"selecting {order} available image")
                else:
                    self.set_status(f"No files exist for date {date}")
                    return
        else:
            # Check for future date because I want to be a smart aleck
            self.set_status("Telescopes see into the past, not the future.")

        # set values in the date and frame boxes
        if exists:
            self.set_status(f"Setting date {date} and frame {frame}")

            self.entry_date.set_text(self.im_UTCdate)
            self.entry_frame.set_text(f'{self.im_frame:04d}')

            self.display_image()
        return

    def select_fullframe(self, *args, **kwargs):

        # Set fullframe mode
        chname = kwargs['chname'] if 'chname' in kwargs.keys() else self.active

        # autoscale the cut cut levels and contrast
        self.fitsimage[chname].auto_levels()

        # set image box to full frame and update stats
        self.full_image_cb(chname=chname)

    def set_slice_axis_cb(self, axis, tf):

        # Toggle the slice mode.  clear the active indicator to be redrawn
        self.slice_axis = axis

        canvas = self.canvas[self.active]
        if canvas.has_tag(SLICETAG_C):
            # get the dimensions of the current cut line
            obj = canvas.get_object_by_tag(SLICETAG_C)
            bbox = obj.objects[0] if obj.kind == 'compound' else obj
            x1, y1, x2, y2 = bbox.get_llur()
            canvas.delete_object_by_tag(SLICETAG_C)

            xpos, ypos = (x1+x2)/2, (y1+y2)/2
            self.motion_slice_cb(canvas, None, xpos, ypos)

    def set_IFU_enabled(self, state):

        # Set all the button states
        labelcolor = 'white' if state else 'gray'
        self.offsetLabel.set_color(labelcolor)
        if not state:
            # Set integration false automatically
            self.integIFU = False
            self.btn_integIFU.set_state(False)
        self.btn_integIFU.set_enabled(state)
        self.btn_defaultIFU.set_enabled(state)
        self.btn_analyseIFU.set_enabled(state)
        self.btn_IFUmenu.set_state(state)
        for btn in self.btns_IFUoffset.values():
            btn.set_enabled(state)
        if state:
            self.dialogue_IFU.widget.show()
        else:
            self.dialogue_IFU.widget.hide()

        return

    def set_IFU_override(self, widget, index, update=True):

        overridemode = widget.widget.currentText()

        if self.IFUoverride == overridemode:
            # if the state hasn't changed, return
            return

        self.logger.info(f"Setting IFU override mode to {overridemode}")
        if overridemode == 'OVERRIDE':
            # override is deactivated
            state = False
            self.IFUoverride = False
        else:
            if self.inst not in overridemode:
                msg = f"Cannot set override for {self.inst} image!"
                self.logger.error(msg)
                state = False
                overridemode = 'OVERRIDE'
                self.IFUoverride = False
            else:
                state = True
                self.IFUoverride = overridemode

        widget.widget.setCurrentText(overridemode)
        self.set_IFU_enabled(state)
        self.btn_analyseIFU.set_enabled(state)
        self.btn_defaultIFU.set_enabled(state)

        if update:
            self.display_image()

    def set_inactive_channel(self):

        # redraw the inactive window (if we are in multi-window mode)

        for chname in self.channel.keys():
            canvas = self.canvas[chname]
            if canvas is None:
                continue

            if chname == self.active:
                if canvas.has_tag(STATSTAG):
                    self.draw_statsbox_cb(canvas, STATSTAG, check_canvas=False,
                                          inactive=False, chname=chname)
            else:
                if canvas.has_tag(STATSTAG):
                    self.draw_statsbox_cb(canvas, STATSTAG, check_canvas=False,
                                          inactive=True, chname=chname)

    def set_mode_cb(self, mode, tf):
        """Called when one of the Move/Draw/Edit radio buttons is selected."""

        if tf:
            self.canvas[self.active].set_draw_mode(mode)
            if mode == 'edit':
                self.edit_select_box()
        return True

    def set_QA(self, widget, index):

        # we need to do two things: set the QA state in the FITS file, and
        # set the QA state for the menu.

        qa = QA_STATES[index]

        prefix = self.currentFrame[self.active].prefix

        # set active text colour appropriately
        for state, color in zip(QA_STATES, QA_COLOURS):
            qw = widget.widget
            if state == qw.currentText():
                qw.setStyleSheet(f"QComboBox:editable{{color: {color}}}")

        self.logger.info(f"Setting {prefix} QA status to {qa}...")

        # currently unthreaded, may need to add thread if this takes a while

        sa = seq_utils.ServerAccess(seq_utils.SERVERNAME, logger=self.logger,
                                    set_status=self.set_status)
        fitsname = os.path.split(self.currentFrame[self.active].fitspath)[-1]

        results = sa.change(fitsname, 'QA', qa)

        if not results:
            # reset the QA state.  This is kinda brute force, but it works.
            msg = f"{prefix} QA state could not be set"
            self.update_image_info()
        else:
            msg = f"{prefix} QA set to {qa}"
        self.set_status(msg, info=True)

    def set_status(self, text, error=False, warning=False, info=False):
        # report messages to the status display
        if not self.gui_up:
            # Our GUi is not built so we don't actually have a widget
            return

        now = datetime.strftime(datetime.now(), "%H:%M:%S")
        fracsecs = datetime.strftime(datetime.now(), ".%f")
        tenthsecs = str(round(float(fracsecs), 1))[1:]
        now = f"{now}{tenthsecs}"
        self.statusWidget.append_text(f"{now} {LTZ}: {text}")

        if error:
            self.logger.error(text)
        elif warning:
            self.logger.warning(text)
        elif info:
            self.logger.info(text)

    def set_sub_entry(self, widget):
        # parse the text entry widget for image subtraction reference frame

        entry = widget.get_text()

        # let's parse the entry box.
        # Empty entry box -- do nothing, don't set self.sub True yet.
        if len(entry) == 0:
            self.sub = False
            widget.set_state(False)
            self.sub_fitspath[self.inst] = None
            return

        if '_' in entry:
            name, ext = entry.split('_')
            fitspath = os.path.join(WORK_PATH, f"{name}_{ext}.fits")
            fits_exists = os.path.exists(fitspath)
            fitsprefix = entry
        elif len(entry) > 10:
            fitsprefix, fitspath, fits_exists = self.get_fitspath(name=entry)
        else:
            if " " in entry:
                # entry is YYYYMMDD NNN
                date, frame = entry.split(' ')
                frame = int(frame)
            else:
                # entry is NNN
                date = self.im_UTCdate
                frame = int(entry)
            fitsprefix, fitspath, fits_exists = self.get_fitspath(date=date,
                                                                  frame=frame)

        if fits_exists:
            self.sub = True
            self.sub_fitspath[self.inst] = fitspath
            self.entry_sub.set_text(fitsprefix)
            self.btn_sub_toggle.set_state(True)
            self.display_image()
        else:
            self.sub = False
            self.btn_sub_toggle.set_state(False)

            msg = f"Could not parse subtraction entry {widget.get_text()}"
            self.logger.error(msg)
            self.set_status(msg)

    def set_today(self):
        # Set to today's date, frame 1

        utcNow = datetime.now(timezone('UTC'))
        utcDate = utcNow.strftime("%Y%m%d")

        self.im_UTCdate = utcDate
        self.im_frame = 1
        self.set_status(f"UTC date set for today, {utcDate} UTC")

        self.display_image()

    def spawn_thread(self, task):

        # Create the dialogue

        dialogue = Widgets.Dialog(title=f"Seqplot: {task.funcname}",
                                  modal=False)
        dialogueBox = dialogue.get_content_area()
        dialogueBox.set_border_width(6)

        vbox = Widgets.VBox()
        vbox.set_border_width(0)
        vbox.set_spacing(0)

        label = Widgets.Label(text=task.initmsg)
        vbox.add_widget(label)

        w_progLabel = Widgets.Label(text="FOO")
        vbox.add_widget(w_progLabel)

        w_progBar = Widgets.ProgressBar()
        w_progBar.widget.setMinimum(task.progMin)
        w_progBar.widget.setMaximum(task.progMax)
        vbox.add_widget(w_progBar)

        dialogueBox.add_widget(vbox)

        # initialise the thread and daemon task
        self.thread = QThread()
        self.threaded_task = ThreadedTask(task)
        self.threaded_task.moveToThread(self.thread)

        # Connect start signals
        self.thread.started.connect(self.threaded_task.start)
        self.threaded_task.started.connect(dialogue.widget.exec_)

        # Connect progress signals
        self.threaded_task.updateProgVal.connect(w_progBar.set_value)
        self.threaded_task.updateProgStr.connect(w_progLabel.set_text)
        self.threaded_task.updateProgStr.connect(self.set_status)
        self.threaded_task.updateLog.connect(self.set_status)

        # Connect completion signals
        self.threaded_task.finished.connect(self.thread.quit)
        self.threaded_task.finished.connect(self.threaded_task.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.threaded_task.finished.connect(lambda: dialogue.widget.accept())
        if task.completed is not None:
            self.threaded_task.finished.connect(task.completed)

        # start the task
        self.thread.start()

    def subtract_frames(self, ickfoo, chname=CHNAME_A):

        # Return hdul[ext] - shdul[ext].
        # At the time this is getting called, both HDULs should be in memory
        # This will NOT modify either array.

        cF = self.currentFrame[chname]
        ext = self.currentFrame[chname].ext

        # Make a copy of the appropriate base file for the subtraction
        if self.clean and cF.cln_hdul is not None:
            hdul = copy_FITS(cF.cln_hdul)
        else:
            hdul = copy_FITS(cF.raw_hdul)

        if self.clean and self.subFits[self.inst][chname].cln_hdul is not None:
            shdul = self.subFits[self.inst][chname].cln_hdul
        else:
            shdul = self.subFits[self.inst][chname].raw_hdul

        prefix1 = self.subFits[self.inst][chname].prefix
        prefix2 = cF.prefix

        # Check that both have same dimensions (probably only a problem for
        # GMOS
        if hdul[ext].shape != shdul[ext].shape:
            self.set_status(f"{prefix1} and {prefix2} have different shapes "
                            "and cannot be subtracted.  Apborting subtraction")
            self.btn_sub_toggle.set_state(False)
            self.sub = False
            return hdul

        # Check for gain correction
        if 'EXPTIME' in hdul[0].header and 'EXPTIME' in shdul[0].header:
            hdu1_exp = hdul[0].header['EXPTIME']
            hdu2_exp = shdul[0].header['EXPTIME']
            gain_corr = hdu1_exp/hdu2_exp if hdu1_exp*hdu2_exp != 0 else 1
        else:
            gain_corr = 1

        # Do the subtraction.  Store this in 'data'.
        result = np.subtract(hdul[ext].data, shdul[ext].data * gain_corr)
        hdul[ext].data = result

        return hdul

    def start(self):
        """
        This method is called just after ``build_gui()`` when the plugin
        is invoked.  This method could be called more than once if the
        plugin is opened and closed.  This method may be omitted
        in many cases.
        """
        self.hv_plot.set_titles(rtitle='Cuts')

        pass

    def stop(self):
        """
        This method is called when the plugin is stopped.
        It should perform any special clean up necessary to terminate
        the operation.  This method could be called more than once if
        the plugin is opened and closed, and may be omitted if there is no
        special cleanup required when stopping.
        """
        self.gui_up = False

        # remove the canvas from the image
        for chname in CHNAME_LIST:
            p_canvas = self.fitsimage[chname].get_canvas()
            try:
                p_canvas.delete_object_by_tag(SATTAG)
            except Exception:
                pass
        self.fv.show_status("")

    def test(self, *args):
        print("test")
        print(f"args = {args}")

    def take_slice_cb(self, obj, button_pressed, data_x, data_y, *args):

        # note -- called from add_draw_mode down passes canvas,
        # while called from  canvas callback does NOT.

        if len(args) != 1:
            return

        fitsimage = args[0]
        name = fitsimage.name.split(':')[-1]
        canvas = self.canvas[name]

        # and then call the plotSlice
        if ((self.oneshotslice_btn.get_state() is True and
             canvas.has_tag(SLICETAG_C))):

            self.slice_coords = canvas.get_object_by_tag(SLICETAG_C).get_llur()

            # remove the existing slice marker
            self.clear_slices()

            # draw a new slice  marker line
            line = self.dc.Line(*self.slice_coords, color=self.drawcolour,
                                showcap=False, alpha=ALPHA_MARKER)
            canvas.add(line, tag=SLICETAG_M)

            # plot the slice
            self.plot_slice(fitsimage)

    def tile_graces(self, hdul):

        # wrapper for read_graces.
        # returns hdul, saturation array, and saturation value

        # Remove the overscan region for display
        rule = r"([0-9]{1,})"
        datasec = re.compile(rule).findall(hdul[SCIEXT].header['DATASEC'])
        datasec = [int(i)-1 for i in datasec]
        data = hdul[SCIEXT].data[slice(datasec[2], datasec[3]+1),
                                 slice(datasec[0], datasec[1]+1)]

        # Calculate saturation value and array
        sat_val = hdul[SCIEXT].header['MAXLIN']
        sat_arr = calculate_sat_array(data, sat_val)

        # create a new hdu
        hdu0 = fits.PrimaryHDU(header=hdul[0].header)
        hdu1 = fits.ImageHDU(header=hdul[1].header, data=data)
        tile_hdul = fits.HDUList([hdu0, hdu1])

        return (tile_hdul, sat_arr, sat_val)

    def tile_gmos_wrapper(self, hdul):

        # Wrapper for tile_gmos.
        # Returns mosaic name and saturation value.

        # Tile the image

        ###########
        # mosaic the image
        self.timeSomething(None)
        data, sat_img, sat_limit = tile_gmos(hdul, set_status=self.set_status)
        self.timeSomething("Time to tile GMOS: {} s")
        if data is None:
            return

        ##########
        # concatenate hdu0 header and new header

        hdr0 = hdul[0].header
        if 'RADECSYS' in hdr0:
            hdr0['RADESYSa'] = hdr0['RADECSYS']
            del hdr0['RADECSYS']

        hdu0 = fits.PrimaryHDU(header=hdul[0].header)
        hdu1 = fits.ImageHDU(data=data, header=hdul[1].header)
        tile_hdul = fits.HDUList([hdu0, hdu1])

        return (tile_hdul, sat_img, sat_limit)

    def timeSomething(self, *args):
        """
        Called to time something _after_ the thing has completed. E.g.:
         self.timeSomething("Prior call to timeSomething")
         <some code to be timed>
         self.timeSomething("The prior code took {} seconds to run")
        """
        t = time.time()
        T = t - self.timeSomethingTimer

        if len(args) > 0:
            try:
                msg = args[0]
                if self.logger is not None:
                    self.logger.info(msg.format(T))
                else:
                    print(msg.format(T))
            except Exception as e:
                self.logger.error(f'cannot output timer results: {e}')
                pass

        self.timeSomethingTimer = t
        return T

    def toggle_autoload(self, button, autoload):
        # Toggle the autoload function.
        # This function always looks for the most recently added image.

        button = self.btn_auto if button is None else button
        if autoload:
            for path in self.autoload_paths:
                # look at the lastlabel file if it's the dhs path
                if 'dhs' in path:
                    path = os.path.join(path, '.lastlabelprefix')

                # If we already have the path in our list, don't add it.
                if ((path in self.autoload_tasks.keys() or
                     not os.path.exists(path))):
                    continue

                # Create a thread and task
                autoload_thread = QThread()
                autoload_task = Autoload_Watchdog(self, path)
                autoload_task.moveToThread(autoload_thread)

                # Connect start signals
                autoload_thread.started.connect(autoload_task.start)

                # Connect event signals
                autoload_task.newfile.connect(self.display_image)
                autoload_task.status.connect(self.set_status)
#                autoload_task.wait.connect(self.wait_for_file_write)

                # Connect completion signals
                autoload_task.finished.connect(autoload_thread.quit)
                autoload_task.finished.connect(autoload_task.deleteLater)
                autoload_thread.finished.connect(autoload_thread.deleteLater)

                # start the task
                autoload_thread.start()

                self.autoload_tasks[path] = autoload_task
                self.autoload_threads[path] = autoload_thread

            self.autoload = True

        else:
            # Stop the tasks
            for path in self.autoload_tasks.keys():
                self.autoload_tasks[path].stop()
                self.autoload_threads[path].quit()
                self.autoload_threads[path].wait()
            self.autoload_tasks = {}
            self.autoload_threads = {}
            button.set_state(False)
            self.autoload = False

    def toggle_cleanIR(self, button, state):
        # Toggle cleanIR state

        self.clean = state
        chname = self.active
        if state:
            # Cleaning should always happen prior to image subtraction.
            if self.currentFrame[chname].cln_hdul is None:
                hdul = copy_FITS(self.currentFrame[chname].raw_hdul)
                self.clean_IR(hdul)
                return

        self.display_image()

    def toggle_dialogue_MEF(self, widget, state):

        if state:
            self.dialogue_MEF.widget.show()
        else:
            self.dialogue_MEF.widget.hide()

    def toggle_dialogue_review(self, widget, state):

        if state:
            self.reviewBegin = self.im_frame
            self.spinbox_reviewBegin.set_value(self.reviewBegin)
            self.dialogue_review.widget.show()
        else:
            # Stop the review
            self.toggle_review(None, False)
            self.dialogue_review.widget.hide()
            self.btn_review.set_state(False)

    def toggle_dialogue_zoom(self, widget, state):

        # Set button state (if called by close signal)
        if state is False:
            self.btn_zoom.set_state(False)

        if state:
            self.dialogue_zoom.widget.show()
        else:
            self.dialogue_zoom.widget.hide()

    def toggle_dialogue_IFU(self, button, state):

        # Set button state (if called by close signal)
        if state is False:
            self.btn_IFUmenu.set_state(False)

        if state:
            self.dialogue_IFU.widget.show()
        else:
            self.dialogue_IFU.widget.hide()

    def toggle_dialogue_info(self, button, state):

        if state is False:
            # Set button state (if called by close signal)
            self.w_info_btn.set_state(False)

        if state:
            self.w_info_dialogue.widget.show()
        else:
            self.w_info_dialogue.widget.hide()

    def toggle_integIFU(self, button, state):
        # Toggle IFU integration

        # Set button state (if called by close signal)
        if state is False:
            self.btn_zoom.set_state(False)

        # image should always be in memory at this point.
        self.integIFU = state
        cF = self.currentFrame[self.active]

        # reset the data
        cF.data = cF.raw_hdul[cF.ext].data

        self.display_image(reset_display=True)
        # self.display_image(hdul=self.currentFrame[self.active].raw_hdul,
        #                    reset_display=True)
        self.select_fullframe()

    def toggle_review(self, widget, state):
        if state:

            """
            if ((self.reviewBegin == self.reviewEnd
                 or self.reviewBegin >
                 and self.reviewBegin == self.im_frame)):
                # we're not going to do a review of the current slide.
                self.btn_reviewOn.set_state(False)
                self.set_status('Bad review limits')
                return
            """

            # Check if we're already on the start frame
            if self.reviewBegin == self.im_frame:
                # start the review on the *next* frame
                self.reviewBegin += 1

            # Check for good review slide limits
            if self.reviewBegin >= self.reviewEnd:

                self.btn_reviewOn.set_state(False)
                self.set_status('Bad review limits')
                return

            # check to make sure not locked.
            locked = DISPLAY_MUTEX.tryLock()
            if locked:
                DISPLAY_MUTEX.unlock()

            if self.autoload:
                # Turn off autoload
                self.toggle_autoload(None, False)

            # Create the list of files

            key = f'*{self.im_UTCdate}'
            fitspathList = self.find_fits(key=key)
            beginStr = f'{self.reviewBegin:04d}'
            beginList = [name for name in fitspathList if beginStr in name]
            if len(beginList) == 1:
                beginIndex = fitspathList.index(beginList[0])
            else:
                self.set_status(f'Cannot find initial frame {beginStr} '
                                'for review')
                self.toggle_review(None, False)
                return

            endStr = f'{self.reviewEnd:04d}'
            endList = [name for name in fitspathList if endStr in name]

            # If end element is not found, just run out the list.
            if len(endList) == 0:
                # if the element is not found, just run out to the end
                endIndex = len(fitspathList)
                self.set_status('End frame does not exist, setting to'
                                'last file of date.')
            elif len(endList) > 1:
                self.set_status(f'Multiple files match frame {endStr}!')
                self.set_status('Cannot start review')
                self.toggle_review(None, False)
                return
            else:
                endIndex = fitspathList.index(endList[0])+1

            fitspathList = fitspathList[beginIndex:endIndex]

            # Create a thread and task
            reviewThread = QThread()
            reviewTask = Autoload_Review(self, fitspathList, self.reviewDelay)
            reviewTask.moveToThread(reviewThread)

            # Connect start signals
            reviewThread.started.connect(reviewTask.start)

            # Connect event signals

            def set_frame(frame):
                self.spinbox_reviewBegin.set_value(frame)
                self.reviewBegin = frame

            reviewTask.frame.connect(set_frame)
            reviewTask.newfile.connect(self.display_image)
            reviewTask.status.connect(self.set_status)

            # Connect completion signals
            reviewTask.finished.connect(lambda s=False:
                                        self.btn_reviewOn.set_state(s))
            reviewTask.finished.connect(lambda s='Run':
                                        self.btn_reviewOn.widget.setText(s))
            reviewTask.finished.connect(lambda s=False:
                                        self.set_review_state(s))
            reviewTask.finished.connect(reviewThread.quit)
            reviewTask.finished.connect(reviewTask.deleteLater)
            reviewThread.finished.connect(reviewThread.deleteLater)

            # start the task
            reviewThread.start()

            self.reviewTask = reviewTask
            self.reviewThread = reviewThread

            self.btn_reviewOn.widget.setText('Halt')
            self.set_status('Starting review')
        else:
            # We'd like to stop the review thread
            self.btn_reviewOn.widget.setText('Run')
            self.btn_reviewOn.set_state(False)

            if self.review:
                # try to stop the tasks
                print(f'toggle_review: {state} self.review = {self.review}')
                if self.reviewTask is not None:
                    self.reviewTask.stop()
                if self.reviewThread is not None:
                    self.reviewThread.quit()
                    self.reviewThread.wait()
                self.reviewTask = None
                self.reviewThread = None

                self.set_status('Stopping review')
                self.btn_reviewOn.set_state(False)
        self.review = state

    def set_review_state(self, state):
        self.review = state

    def toggle_satmask(self, button, state):
        # toggle the saturation mask on and off

        for chname in [c for c in CHNAME_LIST if self.canvas[c] is not None]:
            canvas = self.canvas[chname]
            if state:
                canvas.add(self.canvas_img[chname], tag=SATTAG)
            else:
                canvas.delete_object_by_tag(SATTAG)
            canvas.update_canvas(whence=0)

    def toggle_slice_dialogue(self, widget, state):

        if state is False:
            # Set button state (if called by close signal)
            self.w_hvslice_btn.set_state(False)
            self.set_cursor_mode('navigate')

        if state:
            self.dialogue_hvslice.widget.show()

            # preset one-shot mode
            self.oneshotslice_btn.set_state(True)
            self.toggle_slice_oneshot(self.oneshotslice_btn, True)

        else:
            self.dialogue_hvslice.widget.hide()
            canvas = self.canvas[self.active]
            for tag in [SLICETAG_C, SLICETAG_M]:
                if canvas.has_tag(tag):
                    canvas.delete_object_by_tag(tag)

        self.HVslice_visible = state
        self.sliceguide_btn.set_state(state)

    def toggle_slice_guide(self, state):

        button = self.sliceguide_btn

        if button.get_state() != state:
            button.set_state(state)

        self.HVslice_visible = state
        if button.get_state() != state:
            button.set_state(state)

        canvas = self.canvas[self.active]
        if state is False:
            if canvas.has_tag(SLICETAG_C):
                canvas.delete_object_by_tag(SLICETAG_C)
            mode = 'navigate'

            # Make sure that both live and oneshot are deactivated
            for btn in [self.liveslice_btn, self.oneshotslice_btn]:
                btn.set_state(False)
        else:
            mode = 'slice'
        self.set_cursor_mode(mode)

    def toggle_slice_live(self, button, state):

        if state is True:
            # make sure oneshot slice is off
            self.oneshotslice_btn.set_state(False)
            self.toggle_slice_guide(True)

        self.liveslice = state

        canvas = self.canvas[self.active]
        if state:
            canvas.add_callback('button-press', self.take_slice_cb)
            # remove any previous slice indicator
            if self.canvas[self.active].has_tag(SLICETAG_M):
                self.canvas[self.active].delete_object_by_tag(SLICETAG_M)

        else:
            canvas.remove_callback('button-press', self.take_slice_cb)

    def toggle_slice_oneshot(self, button, state):

        if state is True:
            # make sure live slice is off
            self.liveslice_btn.set_state(False)
            self.liveslice = False
            self.toggle_slice_guide(True)

        canvas = self.canvas[self.active]
        if state is True:
            # add a click callback
            canvas.add_callback('button-press', self.take_slice_cb)
        else:
            # remove the click callback
            canvas.remove_callback('button-press', self.take_slice_cb)

    def toggle_statsdraw(self, button, state):

        mode = 'histogram' if state else 'navigate'
        self.set_cursor_mode(mode)

    def toggle_stats_monitor(self):

        return

        stats_thread = QThread()
        stats_task = StatsMonitorTask()
        stats_task.moveToThread(stats_thread)

        stats_thread.started.connect(stats_task.start)

        stats_task.status.connect(self.set_status)

        stats_task.finished.connect(stats_thread.quit)
        stats_task.finished.connect(stats_task.deleteLater)
        stats_thread.finished.connect(stats_thread.deleteLater)

        stats_thread.start()

        self.stats_thread = stats_thread
        self.stats_task = stats_task

    def toggle_sub(self, button, state):
        # Toggle sky subtraction and set name
        # If text box is empty and button is true, do nothing
        # (and keep sub flag set false)
        # If text box is occupied, check for file existence.
        # If file exists, set sub flag true else false
        # if text box is activated and file exists, set toggle true

        inst = self.inst

        if state:
            # if true, lets check a few things first:
            entry = self.entry_sub.get_text()
            if entry == "":
                state = False
                button.set_state(state)
                return
            elif entry != self.subFits[inst][self.active].prefix:
                # new entry has been specified, parse that
                pass
            else:
                # entry = subFits, so we've got it in memory... do nothing.
                pass

        self.sub = state
        button.set_state(state)

        self.display_image()

    def toggle_testmenu(self, widget, state):

        if state:
            self.test_dialogue.widget.show()
        else:
            self.test_dialogue.widget.hide()

    def update(self, canvas, event, data_x, data_y, viewer):

        obj = canvas.get_object_by_tag(STATSTAG)
        if obj.kind == 'compound':
            bbox = obj.objects[0]
        elif obj.kind == 'rectangle':
            bbox = obj
        else:
            return True

        # calculate center of bbox
        wd = bbox.x2 - bbox.x1
        dw = wd // 2
        ht = bbox.y2 - bbox.y1
        dh = ht // 2
        x, y = bbox.x1 + dw, bbox.y1 + dh

        # calculate offsets of move
        dx = (data_x - x)
        dy = (data_y - y)

        # calculate new coords
        x1, y1, x2, y2 = bbox.x1 + dx, bbox.y1 + dy, bbox.x2 + dx, bbox.y2 + dy

        try:
            canvas.delete_object_by_tag(STATSTAG)
        except Exception as e:
            self.logger(e, warning=True)

        canvas.add(self.dc.Rectangle(x1, y1, x2, y2, color=self.drawcolour,
                                     linestyle='dash'), tag=STATSTAG)

        self.draw_statsbox_cb(canvas, STATSTAG)

        return True

    def update_histogram(self):

        data_max, data_mean, data_med, data_min = self.viewstats
        data = self.box_data

        if len(data) == 0:
            return

        self.histPlot.clear()

        # Set a smaller number of bins if we have a very narrow
        # range of pixel values
        loval, hival = self.fitsimage[self.active].get_cut_levels()

        # Clear the existing cut lines
        try:
            self.loline.remove()
            self.hiline.remove()
        except Exception:
            pass

        tightrange = int(hival-loval)

        finite_data = data[np.where(np.isfinite(data))]
        lowlim = 1.1*loval if loval < 0 else 0.9*loval
        hist_data = finite_data[np.where((finite_data > lowlim) &
                                         (finite_data < 1.1*hival))]
        numbins = min(tightrange, 100)
        if numbins <= 0:
            return

        y, x = np.histogram(hist_data, bins=numbins)

        # normalise the hist data
        y = y/len(data)
        x, y = histOutline(x, y)

        # Had an error where histogram arrays were not equal, bail out.
        if x.shape != y.shape:
            self.set_status(f"Cannot plot histogram: x has shape {x.shape}, "
                            f"y has shape {y.shape}.", error=True)
            return

        xtitle, ytitle = "Pixel value", "Fraction"
        self.histPlot.plot(x, y, xtitle=xtitle, ytitle=ytitle,
                           color='blue', alpha=1.0, drawstyle='steps-post',
                           zorder=10)
        self.histPlot.ax.tick_params(axis='both', pad=0)

        # drop  x-axis ticklabel angle down from default 45deg to save space
        lbls = self.histPlot.ax.xaxis.get_ticklabels()
        for lbl in lbls:
            lbl.set(rotation=30, horizontalalignment='right')

        # show cut levels, mean, median, max as vertical lines
        self.loline = self.histPlot.ax.axvline(loval, 0.0, 0.99, linestyle='-',
                                               color='brown', zorder=-20)
        self.hiline = self.histPlot.ax.axvline(hival, 0.0, 0.99, linestyle='-',
                                               color='green', zorder=-20)
        self.meanline = self.histPlot.ax.axvline(data_mean, 0.0, 0.99,
                                                 linestyle='-', color='yellow')
        self.medline = self.histPlot.ax.axvline(data_med, 0.0, 0.99,
                                                linestyle='-',
                                                color='orange', zorder=-20)
        self.maxline = self.histPlot.ax.axvline(data_max, 0.0, 0.99,
                                                linestyle='-',
                                                color='red', zorder=-20)
        if self.xlimbycuts:
            # user wants "plot by cuts"--adjust plot limits to show only
            # area between locut and high cut "plus a little" so that
            # lo and hi cut markers are shown
            incr = np.fabs(self.lim_adj_pct * (hival - loval))
            self.histPlot.ax.set_xlim(loval - incr, hival + incr)

        self.histPlot.ax.set_facecolor('#E0E0E0')
        self.histPlot.fig.set_facecolor('#E0E0E0')
        self.histPlot.fig.subplots_adjust(left=0.1, right=0.9,
                                          top=0.9, bottom=0.1)
        self.histPlot.fig.canvas.draw()

    def update_image_info(self):
        # update the image information

        imname = self.currentFrame[self.active].imname
        self.wInfo.imname.set_text(imname)

        cF = self.currentFrame[self.active]
        hdr0 = self.currentFrame[self.active].headers[0]
        if len(self.currentFrame[self.active].headers) > 0:
            hdr1 = self.currentFrame[self.active].headers[1]
        else:
            hdr1 = None

        obsid = hdr0['OBSID'] if 'OBSID' in hdr0 else 'NO OBSID'

        self.wInfo.obsid.set_text(obsid)

        # set inst as self.inst, unless IFU
        inst = self.inst if self.IFUinst == "NOT IFU" else self.IFUinst
        self.wInfo.instrument.set_text(inst.upper())

        obstype = hdr0['OBSTYPE'] if 'OBSTYPE' in hdr0 else 'TARGET'
        obstype = obstype if len(obstype) > 0 else 'TARGET'
        self.wInfo.source.set_text(f"{obstype.lower().capitalize()}:")

        target = hdr0['OBJECT'] if 'OBJECT'.lower() in hdr0 else 'UNSPECIFIED'
        self.wInfo.target.set_text(target)

        poffset = hdr0["POFFSET"] if 'POFFSET' in hdr0 else None
        qoffset = hdr0["QOFFSET"] if 'QOFFSET' in hdr0 else None
        if poffset is None or qoffset is None:
            msg = "NO OFFSETS"
        else:
            msg = f"({poffset:0.2f}, {qoffset:0.2f})"
        self.wInfo.pqoffsets.set_text(msg)

        """
        rawgemqa = hdr0['RAWGEMQA'] if 'RAWGEMQA' in hdr0 else 'UNKNOWN'
        rawpireq = hdr0['RAWPIREQ'] if 'RAWPIREQ' in hdr0 else 'UNKNOWN'

        qa = "UNDEFINED"
        if rawgemqa == 'BAD':
            qa = "FAIL"
        elif rawgemqa == 'USABLE':
            if rawpireq == 'YES':
                qa = "PASS"
            elif rawpireq == 'NO':
                qa = "USABLE"
         """
        # QA isn't getting changed in the header, only on the fitsserver,
        # so read from there.

        qa = seq_utils.readQA(f'{imname}.fits')
        self.wInfo.qa.widget.setCurrentText(qa)

        # Update the extra info box
        self.infoDialogueBox.remove_all(delete=True)
        cardnames = ['RA', 'DEC', 'UT', 'EXPTIME',
                     'RAWIQ', 'RAWCC', 'RAWWV', 'RAWBG',
                     'M2BAFFLE', 'M2CENBAF',
                     'AOFOLD', 'PWFS1_ST', 'PWFS2_ST', 'OIWFS_ST', 'AOWFS_ST',
                     'GCALLAMP', 'GCALFILT', 'GCALDIFF', 'GCALSHUT',
                     'INSTRUME', 'CAMERA',
                     'CRVAL1'  # CRVAL1 is a placeholder for scale
                     ]

        cardnames_inst = {'F2': ['NAXIS1', 'FILTER', 'FILT1ID', 'FILT2ID',
                                 'FOCUS', 'GRISM', 'LYOT'],
                          'GMOS-N': ['DETRO1XS', 'CCDSUM', 'FILTER1',
                                     'FILTER2', 'GRATING', 'MASKNAME'],
                          'GMOS-S': ['DETRO1XS', 'CCDSUM', 'FILTER1',
                                     'FILTER2', 'GRATING', 'MASKNAME'],
                          'GNIRS': ['NAXIS1', 'SLIT', 'DECKER', 'FILTER1',
                                    'FILTER2', 'GRATING', 'PRISM', ],
                          'GSAOI': ['NAXIS1', 'UTLWHEEL', 'SAMPMODE', ],
                          'NIFS': ['NAXIS1', 'APERTURE', 'FILTER', 'OBSMODE'],
                          'NIRI': ['NAXIS1', 'FPMASK', 'BEAMSPLT',
                                   'FILTER1', 'FILTER2', 'FILTER3']
                          }

        if self.inst in cardnames_inst.keys():
            cardnames = cardnames + cardnames_inst[self.inst]

        cardnames = [card for card in cardnames if (card in hdr0 or
                                                    card in hdr1)]
        infogrid = Widgets.GridBox(rows=len(cardnames), columns=2)

        ra = hdr0['RA'] if 'RA' in hdr0 else -9999
        dec = hdr0['DEC'] if 'DEC' in hdr0 else -9999
        if ra > 0:
            ra = Angle(f'{ra}d').to_string(unit=u.hour, sep=':')
        else:
            ra = 'NO WCS'
        if dec >= -180:
            dec = Angle(f'{dec}d').to_string(sep=':')
            if dec[0] != '-':
                dec = f'+{dec}'
        else:
            dec = 'NO WCS'

        for idx, card in enumerate(cardnames):
            if card == 'RA':
                val = ra
            elif card == 'DEC':
                val = dec
            elif card == 'INSTRUME':
                val = hdr0['INSTRUME']
                card = 'INSTRUMENT'
            elif card == 'CRVAL1':
                """
                card = 'PIXEL SCALE'
                ps = WCS(hdr1).proj_plane_pixel_scales()
                try:
                    ps0, ps1 = Angle(ps[0]).arcsecond, Angle(ps[1]).arcsecond
                    val = (f'{ps0:0.2f} x {ps1:0.2f}').replace('.', '\".')
                except Exception as e:
                    self.set_status('Cannot determine pixel scale',
                                    warning=True)
                    self.logger.warning(e)
                    continue
                """
            elif card == 'DETRO1XS':
                card = 'IMGSIZE'
                val = f"{hdr0['DETRO1XS']}x{hdr0['DETRO1YS']}"
            elif card == 'CCDSUM':
                val = hdr1['CCDSUM'].replace(' ', 'x')
                card = 'BINNING'
            elif card == 'NAXIS1':
                card = 'IMGSIZE'
                val = f"{hdr1['NAXIS1']}x{hdr1['NAXIS2']}"
            else:
                if card in hdr0 or card in cF.headers[cF.ext]:
                    # Check both in hdr0 and in the image header.
                    hdr = [hdr for hdr in cF.headers if card in hdr][0]
                    val = f'{hdr[card]}'
                else:
                    continue

            label = Widgets.Label(f'{card}:')
            value = Widgets.Label(val)
            infogrid.add_widget(label, idx, 0)
            infogrid.add_widget(value, idx, 1)
        self.infoDialogueBox.add_widget(infogrid)

    def update_stats(self, **kargs):
        # Update image statistics after a draw or redo event

        chname = self.active
        if ((self.canvas[chname] is None or
             not self.canvas[chname].has_tag(STATSTAG))):
            return
        obj = self.canvas[chname].get_object_by_tag(STATSTAG)
        if obj.kind != 'compound':
            return True

        bbox = obj.objects[0]
        x1, y1, x2, y2 = int(bbox.x1), int(bbox.y1), int(bbox.x2), int(bbox.y2)

        # if you click the upper-RHC (blue) grab point, the box will reshape
        # this can lead to x2 < x1 and y2 < y1.
        x1, x2 = min(x1, x2), max(x1, x2)
        y1, y2 = min(y1, y2), max(y1, y2)

        # Update image stats

        cF = self.currentFrame[chname]
        data = cF.data

        # select subset of data in the box
        data_sub = data[y1:y2+1, x1:x2+1]
        sat_sub = cF.sat_arr[y1:y2+1, x1:x2+1]

        sat_val = cF.sat_val

        non_nans = np.count_nonzero(np.invert(np.isnan(data_sub)))
        if non_nans == 0:
            data = np.array([])
            sat_limit_str = "--"
            nsat_str = '-- (--%)'
            sat_perc = -9999
        else:
            nsat = np.count_nonzero(sat_sub)
            sat_perc = nsat / sat_sub.size * 100
            # omit saturated data if not integrated and sat value != 0
            if sat_val == 0 or self.integIFU:
                data = data_sub
            else:
                data = data_sub[np.invert(sat_sub)]
            if not self.integIFU:
                sat_limit_str = f'{sat_val}'
                nsat_str = f'{nsat} ({sat_perc:0.1f}%)'
            else:
                sat_limit_str = "--"
                nsat_str = '-- (--%)'

        # Calculate statistics on the box area

        data_max = int(np.nanmax(data)) if len(data) > 0 else '--'
        data_mean = int(np.nanmean(data)) if len(data) > 0 else '--'
        data_med = int(np.nanmedian(data)) if len(data) > 0 else '--'
        data_min = int(np.nanmin(data)) if len(data) > 0 else '--'
        data_max_str = (f'{data_max}')
        data_mean_str = (f'{data_mean}')
        data_med_str = (f'{data_med}')
        self.bbox_max = data_max
        self.bbox_min = data_min
        self.viewstats = (data_max, data_mean, data_med, data_min)
        self.box_data = data
        self.statswidgts['max'].set_text(data_max_str)
        self.statswidgts['mean'].set_text(data_mean_str)
        self.statswidgts['med'].set_text(data_med_str)

        self.satwidgts['sat_limit'].set_text(sat_limit_str)
        self.satwidgts['nsat'].set_text(nsat_str)

        if sat_perc < SATURATION_THRESHOLD or self.integIFU:
            for widget in self.satwidgts.values():
                widget.set_color()
            for widget in self.satlabels.values():
                widget.set_color()
        else:
            for widget in self.satwidgts.values():
                widget.set_color('red')
            for widget in self.satlabels.values():
                widget.set_color('red')

        # Display the image histogram
        self.update_histogram()

    def zoom_to_fit(self):
        # Zoom each channel to fit

        chnames = [chname for chname in self.channel.keys()
                   if self.channel[chname] is not None]

        for chname in chnames:
            # set magnification to fit
            self.channel[chname].viewer.zoom_fit()
            # setting stats box to full frame and update stats
            self.select_fullframe(chname=chname)

    def __str__(self):
        """
        This method should be provided and should return the lower case
        name of the plugin.
        """
        return 'seqplot'

###############################################################################
# Autoload classes


class Autoload_Inotify(QObject):

    finished = pyqtSignal()
    started = pyqtSignal()
    newfile = pyqtSignal()
    status = pyqtSignal(str)

    def __init__(self, seqplot, path):
        super().__init__()
        self.seqplot = seqplot
        self.path = path

    def start(self):
        self.status.emit("Autoload monitor activated (inotify)")
        inot = inotify.adapters.Inotify()
        inot.add_watch(self.path, IN_CLOSE_WRITE)
        for event in inot.event_gen(yield_nones=False):
            self.autoload_on_created(event)

    def stop(self):
        self.status.emit("Autoload monitor stopped")
        self.finished.emit()

    def notify_on_created(self, event):
        self.status.emit(f"Found a file! {event.src_path}")

    def autoload_on_created(self, event):
        (_type_names, path, filename) = event
        date, frame = get_date_frame(filename)
        self.seqplot.datapath = self.path
        self.seqplot.im_UTCdate = date
        self.seqplot.im_frame = int(frame)
        self.newfile.emit()


class Autoload_Review(QObject):

    finished = pyqtSignal()
    started = pyqtSignal()
    newfile = pyqtSignal()
    status = pyqtSignal(str)
    frame = pyqtSignal(int)

    def __init__(self, seqplot, fitspathList, delay):
        super().__init__()

        self.seqplot = seqplot
        self.fitspathList = fitspathList
        self.delay = delay
        self.halt = False

    def start(self):

        for fitspath in self.fitspathList:

            if self.halt:
                break

            # nab the lock.  This allows for the pause to work
            DISPLAY_MUTEX.lock()

            date, frame = get_date_frame(fitspath)
            self.seqplot.im_UTCdate = date
            frame = int(frame) if frame is not None else frame
            self.seqplot.im_frame = frame
            self.frame.emit(frame)
            self.seqplot.datapath = os.path.split(fitspath)[0]
            self.newfile.emit()

            # release the lock
            DISPLAY_MUTEX.unlock()

            # Delay only if we're not done
            if fitspath != self.fitspathList[-1]:
                time.sleep(self.delay)

        self.finished.emit()

    def stop(self):
        # check to make sure not locked.
        self.halt = True
        locked = DISPLAY_MUTEX.tryLock()
        if locked:
            DISPLAY_MUTEX.unlock()

        self.status.emit('Review stopped')
        self.finished.emit()


class Autoload_Watchdog(QObject):

    finished = pyqtSignal()
    started = pyqtSignal()
    newfile = pyqtSignal()
    status = pyqtSignal(str)
    wait = pyqtSignal(str)

    def __init__(self, seqplot, path):
        super().__init__()
        self.seqplot = seqplot

        #self.observer_dict = {}

        self.autoload_queue = {}

        self.monitorpath = path
        if '.lastlabelprefix' in path:
            path, filename = os.path.split(path)
        self.path = path

    def start(self):

        patterns = ["*.fits"]
        ignore_patterns = None
        ignore_directories = False
        case_sensitive = False

        if self.monitorpath != self.path:
            # we're monitoring a file: '.lastlabelprefix' for dhs/perm
            fits_handler = FileSystemEventHandler()
            fits_handler.on_modified = self.autoload_on_modified
        else:
            # We're monitoring a path for any other case
            fits_handler = PatternMatchingEventHandler(patterns,
                                                       ignore_patterns,
                                                       ignore_directories,
                                                       case_sensitive)

            fits_handler.on_any_event = self.autoload_on_created

        self.observer = PollingObserver()
        self.observer.schedule(fits_handler, self.monitorpath)
        self.observer.start()

        self.status.emit(f"Autoload monitor of {self.path} activated")

    def stop(self):
        self.status.emit(f"Autoload monitor of {self.path} stopped")
        self.observer.stop()
        self.finished.emit()

    def stop_write_watchdog(self):
        if self.write_observer is not None:
            self.write_observer.stop()
            self.write_observer = None

    def notify_on_created(self, event):
        self.status.emit(f"Found a file! {event.src_path}")

    def autoload_on_modified(self, event):

        # DISPLAY_MUTEX.lock()

        with open(self.monitorpath) as labelfile:
            prefix = labelfile.read()

        fitspath = os.path.join(self.path, f'{prefix}.fits')

        exists = False
        while not exists:
            exists = os.path.exists(fitspath)
            
        date, frame = get_date_frame(fitspath)
        self.seqplot.autoload_fitspath = fitspath
        self.seqplot.datapath = self.path
        self.seqplot.im_UTCdate = date
        self.seqplot.im_frame = int(frame) if frame is not None else frame

        self.newfile.emit()
        # DISPLAY_MUTEX.unlock()

    def autoload_on_created(self, event):

        DISPLAY_MUTEX.lock()

        path = event.src_path
        date, frame = get_date_frame(path)
        self.seqplot.autoload_fitspath = path
        self.seqplot.datapath = self.path
        self.seqplot.im_UTCdate = date
        self.seqplot.im_frame = int(frame) if frame is not None else frame
        if 'hrwfs' in path:
            print(f'autoload_on_created: hrwfs in {path}')
            time.sleep(4)

        # hrwfs relies a few-second delay before loading
        if 'hrwfs' in self.fitspath:
            time.sleep(4)

        self.newfile.emit()

        DISPLAY_MUTEX.unlock()


class Autoload_Glob(QObject):

    finished = pyqtSignal()
    started = pyqtSignal()
    newfile = pyqtSignal()
    status = pyqtSignal(str)

    def __init__(self, seqplot, path):
        super().__init__()
        self.seqplot = seqplot
        self.path = path
        self.run = True

    def start(self):

        # Because be as specific as possible for glob
        date = datetime.now(timezone('UTC')).strftime('%Y%m%d')
        searchpath = os.path.join(self.path, f"*{date}*.fits")

        msg = f"Searching {searchpath} for new images using Glob"
        self.seqplot.logger.info(msg)

        self.status.emit("Autoload monitor (Glob) activated")

        while self.run:
            # Check to see if there's a new image here
            frame = self.find_latest_seqn(searchpath)

            # if there's a new image, then update
            if frame is not None and (frame != self.seqplot.im_frame or
                                      date != self.seqplot.im_UTCdate):
                self.seqplot.datapath = self.path
                self.seqplot.im_UTCdate = date
                self.seqplot.im_frame = int(frame)
                self.newfile.emit()

            time.sleep(AUTOLOAD_CADENCE)

    def stop(self):
        self.run = False
        self.status.emit("Autoload monitor stopped")
        self.finished.emit()

    def find_latest_seqn(self, searchpath):
        try:
            file_list = glob.glob(searchpath)
        except Exception as e:
            self.seqplot.logger.info(e)

        if len(file_list) > 0:
            latest_file = max(file_list, key=os.path.getctime)
            date, frame = get_date_frame(latest_file)

            return frame
        else:
            return None

###############################################################################
# THREADED TASK CLASSES


class Task():

    def __init__(self, funcname=None, initmsg=None, finimsg=None,
                 function=None, completed=None, progMax=0,
                 progMin=0, **kwargs):

        self.funcname = funcname
        self.function = function
        self.completed = completed
        self.initmsg = initmsg
        self.finimsg = finimsg
        self.progMin = progMin
        self.progMax = progMax
        self.taskargs = kwargs

        self.results = None


class ThreadedTask(QObject):

    finished = pyqtSignal()
    updateProgVal = pyqtSignal(int)
    updateProgStr = pyqtSignal(str)
    updateLog = pyqtSignal(str)

    started = pyqtSignal()

    def __init__(self, task):
        super().__init__()
        self.task = task

    def start(self):

        task = self.task

        self.started.emit()

        self.updateLog.emit(task.initmsg)
        self.updateProgStr.emit(task.initmsg)
        t0 = time.time()

        task.taskargs['report_status'] = self.statusRepeater
        # task.taskargs['report_val'] = self.progressRepeater

        task.result = task.function(**task.taskargs)

        dt = time.time()-t0
        self.updateLog.emit(f"{task.finimsg}: {dt:0.2f} s")

        self.finished.emit()

    def statusRepeater(self, msg):
        self.updateProgStr.emit(msg)

    def progressRepeater(self, val):
        self.updateProgVal.emit(val)


class StatsMonitorTask(QObject):

    finished = pyqtSignal()
    started = pyqtSignal()
    status = pyqtSignal(str)

    def start(self):

        self.status.emit('Resource monitor activated')
        while True:

            cpu_percent = psutil.cpu_percent(interval=1)
            memory_usage = psutil.virtual_memory()
            self.status.emit(f'CPU = {cpu_percent}%  '
                             f'Memory = {memory_usage.percent}%')

###############################################################################
# seqplot classes


class ViewStruct():
    def __init__(self):
        self.channel = None
        self.fitsimage = None
        self.canvas = None
        self.canvas_img = None


class FrameObject():

    # Handy org utility for tracking data on a given channel
    def __init__(self):
        self.prefix = ''
        self.fitspath = None
        self.imname = ''
        self.data = None
        self.dim = (None, None, None, None)
        self.headers = None
        self.sat = -1, -1, -1
        self.sat_val = None
        self.sat_arr = None
        self.ext = 1
        self.image_exts = []
        self.plane = None
        self.raw_hdul = None
        self.cln_hdul = None
        self.sub_hdul = None

    def reset(self):
        # reset before creating a new structure, i think we want to make
        # sure that we close the fits structures so that they don't end up
        # with memory leaks.
        if self.raw_hdul is not None:
            self.raw_hdul.close()
        if self.cln_hdul is not None:
            self.cln_hdul.close()
        self.__init__()


class NoImageExtensionException(Exception):
    def __init__(self, message):
        super().__init__(message)


###############################################################################


def calculate_ir_saturation(hdul, logger=None):
    """ Calc saturation level for Gemini IR instruments
    """

    img = hdul[1].data
    hd0 = hdul[0].header
    checks = True
    # coadds checks
    if 'COADDS' not in hd0:
        logger.error('# ERROR! Could not determine the number of coadds')
        checks = False
    elif int(hd0['COADDS']) < 1:
        logger.error('# ERROR! Could not determine the number of coadds')
        checks = False

    elif hd0['INSTRUME'].startswith('NIFS'):
        sat_val = 35000.

    elif hd0['INSTRUME'].startswith('NIRI'):
        checks = False
        if 'A_VDDUC' in hd0 and 'A_VDET' in hd0:
            shallowbias = -0.6
            deepbias = -0.87
            #
            vdduc = hd0['A_VDDUC']
            vdet = hd0['A_VDET']
            biasvolt = vdduc - vdet
            if abs(biasvolt - shallowbias) < 0.05:
                sat_val = 10000.
                checks = True
            elif abs(biasvolt - deepbias) < 0.05:
                sat_val = 15000.
                checks = True
        if checks:
            sat_val = sat_val*hd0['coadds']

    elif hd0['INSTRUME'].startswith('GNIRS'):
        checks = False
        if 'DETBIAS' in hd0:
            detbias = hd0['DETBIAS']
            if abs(detbias + 0.3) < 0.1:
                sat_val = 5500.
                checks = True
            elif abs(detbias + 0.6) < 0.1:
                sat_val = 11000.
                checks = True
        if checks:
            sat_val = sat_val*hd0['coadds']

    # Headers without "COADDS" or none of the above
    else:
        if hd0['INSTRUME'].startswith('F2') and 'NREADS' in hd0:
            if hd0['NREADS'] > 0:
                checks = True
                sat_val = 25000.*hd0['NREADS']
        else:
            logger.error('# ERROR! Instrument/config not recognized')
            checks = False

    if not checks:
        errmsg = ("Saturation determination failed")
        if logger is not None:
            logger.info(errmsg)
        else:
            print(errmsg)

        sat_val = 0

    sat_arr = calculate_sat_array(img, sat_val)
    return (sat_arr, sat_val)


def tile_gmos(hdul, set_status=None):
    """ Rebuilding GMOS images
    """

    hdr0 = hdul[0].header
    hdr1 = hdul[1].header

    if 'DETNROI' not in hdr0:
        # if DETNROI keywords not set, we cannot tile
        msg = ("No DETNROI card set -- cannot mosaic")
        if set_status is not None:
            set_status(msg, error=True)
        data = hdul[1].data
        sat_img = np.zeros_like(data)
        hdul.close()

        return (data, sat_img, 0)

    detnroi = hdr0['DETNROI']

    rois = []
    for i in range(1, detnroi+1):
        tmp = []
        tmp += [i]
        tmp += [hdr0[f'DETRO{i}X']]
        tmp += [hdr0[f'DETRO{i}XS']]
        tmp += [hdr0[f'DETRO{i}Y']]
        tmp += [hdr0[f'DETRO{i}YS']]
        rois += [tmp]

    rule = r"([0-9]{1,})"

    # convert the detsize from [x0:x,y0:y] to [x0,x,y0,y]
    detsize = re.compile(rule).findall(hdr0['DETSIZE'])
    detsize = [int(i) for i in detsize]

    # convert the ccdbin from '1 1' to [1,1]
    ccdbin = re.compile(rule).findall(hdr1['CCDSUM'])
    ccdbin = [int(i) for i in ccdbin]
    xbin = ccdbin[0]

    namps = int(hdr0['NAMPS'])
    nccds = int(hdr0['NCCDS'])

    gap = 67

    # create an array of zeroes of the full size determined by detsize above
    img = np.full(((detsize[3]-detsize[2])//ccdbin[1]+1,
                   (detsize[1]-detsize[0])//ccdbin[0]+1+(nccds-1)*gap),
                  np.NaN)
    vmax = np.inf

    # iterate over every data HDU
    lastCCDnum = None
    xoff = 0
    for j in range(1, len(hdul)):

        frameid = int(hdul[j].header['FRAMEID'])

        chipname = hdul[j].header['FRMNAME'].split(',')[1]
        CCDnum = int(chipname.split('<')[1].split('>')[0])
        lastCCDnum = CCDnum if lastCCDnum is None else lastCCDnum
        if CCDnum != lastCCDnum:
            xoff += int(gap/xbin)
            lastCCDnum = CCDnum

        detsec = re.compile(rule).findall(hdul[j].header['DETSEC'])
        detsec = [int(i)-1 for i in detsec]

        biassec = re.compile(rule).findall(hdul[j].header['BIASSEC'])
        biassec = [int(i)-1 for i in biassec]  # FITS is 1-indexed, numpy is 0
        biassec = hdul[j].data[slice(biassec[2], biassec[3]+1),
                               slice(biassec[0], biassec[1]+1)]

        # oscan is median of the bias region, will be subtracted off of the
        # image at the end
        oscan = np.median(biassec)
        if 65534 - oscan < vmax:
            vmax = 65534 - oscan

        ccdsec = re.compile(rule).findall(hdul[j].header['CCDSEC'])
        ccdsec = [int(i)-1 for i in ccdsec]
        for i in [0, 1]:
            ccdsec[i*2+1] = (ccdsec[i*2+1]-ccdsec[i*2])//ccdbin[i]
            ccdsec[i*2] = ccdsec[i*2]//ccdbin[i]
            ccdsec[i*2+1] += ccdsec[i*2]

        datasec = re.compile(rule).findall(hdul[j].header['DATASEC'])
        datasec = [int(i)-1 for i in datasec]

        xf = (frameid % (namps*nccds)) // namps*(2048//ccdbin[0])
        img[slice(ccdsec[2], ccdsec[3]+1),
            slice(ccdsec[0]+xf+xoff, ccdsec[1]+1+xf+xoff)] = (
                hdul[j].data[slice(datasec[2], datasec[3]+1),
                             slice(datasec[0], datasec[1]+1)]) - oscan

    img2 = trim_img(img)
    if sum([True for i in np.shape(img2) if i > 0]) == 2:
        img = img2

    sat_val = vmax
    sat_arr = calculate_sat_array(img, sat_val)

    return img, sat_arr, sat_val


def tile_gmos_gaps(hdul, set_status=None):
    """ Rebuilding GMOS images
    """

    hdr0 = hdul[0].header
    hdr1 = hdul[1].header

    # if DETNROI keywords not set, we cannot tile
    if 'DETNROI' not in hdr0:
        msg = ("No DETNROI card set -- cannot mosaic")
        if set_status is not None:
            set_status(msg, error=True)
        data = hdul[1].data
        sat_img = np.zeros_like(data)
        hdul.close()

        return (data, sat_img, 0)

    detnroi = hdr0['DETNROI']

    rois = []
    for i in range(1, detnroi+1):
        tmp = []
        tmp += [i]
        tmp += [hdr0[f'DETRO{i}X']]
        tmp += [hdr0[f'DETRO{i}XS']]
        tmp += [hdr0[f'DETRO{i}Y']]
        tmp += [hdr0[f'DETRO{i}YS']]
        rois += [tmp]

    rule = r"([0-9]{1,})"

    # convert the detsize from [x0:x,y0:y] to [x0,x,y0,y]
    detsize = re.compile(rule).findall(hdr0['DETSIZE'])
    detsize = [int(i) for i in detsize]

    # convert the ccdbin from '1 1' to [1,1]
    ccdbin = re.compile(rule).findall(hdr1['CCDSUM'])
    ccdbin = [int(i) for i in ccdbin]

    namps = int(hdr0['NAMPS'])
    nccds = int(hdr0['NCCDS'])

    # create an array of zeroes of the full size determined by detsize above
    img = np.zeros(((detsize[3]-detsize[2])//ccdbin[1]+1,
                    (detsize[1]-detsize[0])//ccdbin[0]+1))

    vmax = np.inf

    # iterate over every data HDU
    for j in range(1, len(hdul)):
        frameid = int(hdul[j].header['FRAMEID'])

        biassec = re.compile(rule).findall(hdul[j].header['BIASSEC'])
        biassec = [int(i)-1 for i in biassec]  # FITS is 1-indexed, numpy is 0

        biassec = hdul[j].data[slice(biassec[2], biassec[3]+1),
                               slice(biassec[0], biassec[1]+1)]

        # oscan is median of the bias region, will be subtracted off of the
        # image at the end
        oscan = np.median(biassec)
        if 65534 - oscan < vmax:
            vmax = 65534 - oscan

        ccdsec = re.compile(rule).findall(hdul[j].header['CCDSEC'])
        ccdsec = [int(i)-1 for i in ccdsec]

        for i in [0, 1]:
            ccdsec[i*2+1] = (ccdsec[i*2+1]-ccdsec[i*2])//ccdbin[i]
            ccdsec[i*2] = ccdsec[i*2]//ccdbin[i]
            ccdsec[i*2+1] += ccdsec[i*2]

        datasec = re.compile(rule).findall(hdul[j].header['DATASEC'])
        datasec = [int(i)-1 for i in datasec]

        xf = (frameid % (namps*nccds)) // namps*(2048//ccdbin[0])
        img[slice(ccdsec[2], ccdsec[3]+1),
            slice(ccdsec[0]+xf, ccdsec[1]+1+xf)] = (
                hdul[j].data[slice(datasec[2], datasec[3]+1),
                             slice(datasec[0], datasec[1]+1)]) - oscan
    img2 = trim_img(img)
    if sum([True for i in np.shape(img2) if i > 0]) == 2:
        img = img2

    sat_val = vmax
    sat_arr = calculate_sat_array(img, sat_val)

    return img, sat_arr, sat_val


def calculate_sat_array(arr, sat_val):

    if sat_val == 0 or sat_val is None:
        # 0 indicates no saturation value.
        # returns an array that is False everywhere

        sat_arr = np.zeros_like(arr).astype(bool)
    else:
        # returns an array that is False where below the sat limit, True above.
        sat_arr = np.zeros_like(arr).astype(bool)
        sat_arr[arr >= sat_val] = True

    return sat_arr


def copy_FITS(hdul):
    # return a deep copy of hdul

    list_of_hdus = []
    for ext, hdu in enumerate(hdul):

        # copy WCS cards from ext 0 if available
        if ext > 0:
            for card in WCS_CARDS:
                if card in hdul[0].header and card not in hdu.header:
                    hdu.header[card] = hdul[0].header[card]

        new_hdu = None
        if type(hdu) == fits.hdu.image.PrimaryHDU:
            new_hdu = fits.PrimaryHDU(header=hdu.header, data=hdu.data)
        elif type(hdu) == fits.hdu.image.ImageHDU:
            new_hdu = fits.ImageHDU(header=hdu.header, data=hdu.data)
        elif type(hdu) == fits.hdu.compressed.CompImageHDU:
            new_hdu = fits.hdu.compressed.CompImageHDU(header=hdu.header,
                                                       data=hdu.data)
        if new_hdu is not None:
            list_of_hdus.append(new_hdu)

    new_hdul = fits.HDUList(list_of_hdus)
    return new_hdul


def construct_sat_image(arr, rgba):
    # Takes a np array, and returns an rgbobj that can be overlayed

    rarr = rgba[0]*arr
    garr = rgba[1]*arr
    barr = rgba[2]*arr
    aarr = rgba[3]*arr

    rgba_arr = np.dstack((rarr, garr, barr, aarr))
    rgb_obj = RGBImage.RGBImage(data_np=rgba_arr, order='RGBA')

    return rgb_obj


def generate_chname_dict(value=None, class_name=None):
    # returns a dictionary with CHNAME keys, initially set to default
    d = {}

    for ch in CHNAME_LIST:
        default = class_name() if class_name is not None else value
        d[ch] = default

    return d


def get_date_frame(fitspath):

    items = os.path.split(fitspath)
    prefix = items[-1].split('.')[0]
    try:
        date, frame = re.sub('[_a-zA-Z]', ' ', prefix).lstrip().split()[0:2]
        frame = int(frame)
    except Exception as e:
        date, frame = None, None
        print(f'get_date_frame: cannot parse {fitspath} into date and frame:')
        print(e)
    return (date, frame)


def get_fitsprefix(fitspath):
    # Strip out the file name from path and extension

    head, tail = os.path.split(fitspath)
    ext = [ext for ext in ['.fits', '.FITS'] if ext in tail][0]
    name = tail.split(ext)[0]

    return name


def get_path(fitspath):
    head, tail = os.path.split(fitspath)
    return head


def tile_igrins(fitspath, logger=None, show=False):

    # Join two IGRINS images and write out a merged one.
    # Returns the merged name if successful, None if not.

    # Currently (2023/01/11) IGRINS images are stored as individual files
    # for H and K filters.  Each file has only a single HDU (as opposed to
    # Gemini facilities instruments which use a primary HDU and a data HDU.

    # This may well change for IGRINS-2, as it will be a facility instrument.
    # IGRINS-2 will be stored as an HDUlist with primary plus data HDUs.

    science_data = []

    # Let's find the images.  fitspath is only ONE of the images
    # (and we don't know which one)

    fitspath = fitspath.replace('K', 'H')

    for filter in {"H", "K"}:

        fitspath = fitspath.replace('H', filter)
        if not os.path.exists(fitspath):
            date, frame = get_date_frame(fitspath)
            logger.error(f"{filter} filter image for {date} frame {frame}"
                         "does not exist -- aborting join.")
            return None, np.NaN
        else:
            # read the data for that filter
            # igrins has a single HDU unlike GEMINI facility detectors.
            with fits.open(fitspath) as hdu:
                science_data.append(hdu[0].data)
                oldheader = hdu[0].header  # automatically save second header
            logger.info(f"Read in {filter} filter image")

    # join the images
    joined_data = np.concatenate(science_data, axis=1)

    hdu = fits.PrimaryHDU(joined_data)
    newhdr = hdu.header
    newhdr_keys = hdu.header.keys()
    for key in oldheader.keys():
        if len(key) > 0 and key not in newhdr_keys:
            try:
                newhdr[key] = oldheader[key]
            except Exception:
                logger.error(f"Could not set {key} = {oldheader[key]}")
    hdu.header = newhdr

    # Save with filter removed and _VIS added
    fitsname = get_fitsprefix(fitspath).replace('H', '')
    joined_name = (f"{fitsname}_VIS.fits")
    joined_path = os.path.join(WORK_PATH, joined_name)
    hdu.writeto(joined_path, overwrite=True)

    """
    if show:
        plt.figure()
        plt.imshow(joined_data, cmap='viridis')
        plt.colorbar()
        plt.show()
    """

    sat_limit = 22000
    return joined_path, sat_limit


def histOutline(binsIn, histIn, *args, **kwargs):
    # code from http://www.scipy.org/Cookbook/Matplotlib/UnfilledHistograms
    # Modified by C Figura

    """
    Make a histogram that can be plotted with plot() so that
    the histogram just has the outline rather than bars as it
    usually does.

    Example Usage:
    binsIn = numpy.arange(0, 1, 0.1)
    angle = pylab.rand(50)

    (bins, data) = histOutline(binsIn, angle)
    plot(bins, data, 'k-', linewidth=2)

    """

    if len(binsIn) < 2 or len(histIn) < 2:
        return binsIn, histIn

    stepSize = binsIn[1] - binsIn[0]
    bins = np.array([binsIn[0]])
    #  data = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
    data = np.zeros(len(binsIn)*2 + 2, dtype=float)
    for bb in range(len(binsIn)):
        bins = np.append(bins, binsIn[bb])
        bins = np.append(bins, binsIn[bb]+stepSize)
        if bb < len(histIn):
            data[2*bb + 1] = histIn[bb]
            data[2*bb + 2] = histIn[bb]

    bins = np.append(bins, bins[-1])
    data[0] = np.NaN
    data[-1] = np.NaN

    return (bins, data)


def trim_img(a):
    """ Trim an image
    """

    nzero = np.nonzero(np.isfinite(a))
    top, bottom = np.min(nzero[0]), np.max(nzero[0])
    left, right = np.min(nzero[1]), np.max(nzero[1])
    out = a[top:bottom+1, left:right+1]  # +1 as second argument is exclusive

    return out

###############################################################################
# A few debugging/analysis tools, because gigna is NOT as well documented
# as it should be.


def print_dir(obj):
    # little coding tool
    print(f"\n\n type = {type(obj)}")
    a = dir(obj)
    for item in a:
        print(item)
    print("\n")


def print_dict(dict):
    # another little coding tool
    print(f"\n\n type = {type(dict)}")
    keylist = []
    for key in dict.keys():
        keylist.append(key)
    keylist.sort()
    for key in keylist:
        print(f"{key}")

    print("\n")
