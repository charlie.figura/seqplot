from astropy.io import fits

import logging
import numpy as np
import os
import sys

import re       # for html tag removal

from scipy.signal import argrelextrema
from scipy.ndimage.interpolation import rotate
from scipy.optimize import curve_fit

from matplotlib import rc
from math import floor

import matplotlib.pyplot as plt
from astropy.visualization import astropy_mpl_style
plt.style.use(astropy_mpl_style)


def integrateIFU(input_path=None, hdulist=None, n=2, dmin=-1, dmax=-1,
                 integ=True, angle=0.809, IFUoffset=0, output_path=None,
                 parameters=None, logger=None, set_status=None):

    # angle = rotation angle in degrees.

    if input_path is None and hdulist is None:
        return

    # I don't remember what I was doing with n....

    # Parameter list is
    # [instrument,timestamp,collapseaxis,angle,medwidth,centres]

    #    inst,timestamp,collapseaxis,angle,medwidth,centres = parameters

    logger.info(f"IFUoffset  = {IFUoffset}")
    logger.info(f"IFUcentres = {parameters.centres}")
    centres = np.array(parameters.centres) + IFUoffset
    medwidth = parameters.medwidth

    leadE = centres-medwidth/2
    traiE = centres+medwidth/2

    # Set up the logger if there isn't one:
    if logger is None:
        logger = logging.getLogger('read')

    # Read the fits image in
    if hdulist is None:
        logger.info('Reading %s', input_path)
        hdulist = fits.open(input_path)

    numext = len(hdulist)
    logger.debug('Numext: %d', numext)

    sciext = None
    for i in range(len(hdulist)):
        try:
            extname = hdulist[i].header['EXTNAME']
        except Exception:
            extname = None
        logger.debug('Extension %d name: %s', i, extname)
        if extname == 'SCI':
            sciext = i
    if sciext is None:
        if numext == 1:
            sciext = 0
        else:
            sciext = 1
    logger.debug('Science extension: %s', sciext)

    data = hdulist[sciext].data
    instrument = hdulist[0].header['INSTRUME']
    logger.debug('Instrument: %s', instrument)

    #  I measured angle to be 0.809
    if parameters.angle == 0:
        rotdata = data
    else:
        rotdata = rotate(data, parameters.angle)

    collapse0 = rotdata.mean(axis=parameters.collapseaxis)

    # create our image array
    naxis1 = len(centres)
    naxis2 = medwidth
    integratedMap = np.zeros((naxis1, naxis2))

    for i in range(naxis1):
        if leadE[i] < 0:
            leadE[i] = 0
            startindex = int(naxis2-(traiE[i]-leadE[i]))
        else:
            startindex = 0
        strip = collapse0[int(leadE[i]):int(traiE[i])]
        if len(strip) == len(integratedMap[i]):
            integratedMap[i][startindex:] = strip
        else:
            set_status("Shifting beyond edge of image: truncating "
                       "reconstruction", warning=True)

    if "NIFS" in parameters.instrument:
        # maintaining aspect ratio without WCS was puzzling, but *apparently*
        # what I decided to do is the same hack as used in gacq.
        # We need to multiply the array width by 5 and the array height by 2
        # to get the appropriate aspect ratio. (1:2.5)

        newMap = np.flipud(np.rot90(integratedMap))
        newMap = np.repeat(np.repeat(newMap, 2, axis=0), 5, axis=1)
        integratedMap = newMap

    collapse_hdup = fits.PrimaryHDU(header=hdulist[0].header)
    collapse_hdui = fits.ImageHDU(integratedMap)
    collapse_hdul = fits.HDUList([collapse_hdup, collapse_hdui])

    #  Write out the collapsed FITS image
    if output_path is not None:
        collapse_hdul.writeto(output_path, overwrite=True)
    else:
        # close the input hdulist, and return the new one.
        return collapse_hdul


def read_IFUparams(paramfile):

    # Read in the data line and parse it
    # Return parameter list [instrument,timestamp,collapseaxis,angle,medwidth,
    #                        centres]

    # Read in the paramfile and nab the latest entry
    with open(paramfile) as fp:
        rawparamlines = fp.readlines()

    # clear any blank lines
    paramlines = []
    for item in rawparamlines:
        if len(item.strip()) > 0:
            paramlines.append(item)

    csvline = paramlines[-1].strip()

    # parse the line into a list
    params = csvline.split(",", maxsplit=5)
    instrument, timestamp, collapseaxis, angle, medwidth, centres = params
    collapseaxis = int(collapseaxis)
    angle = float(angle)
    medwidth = int(medwidth)
    centres = centres[1:-2].split(",")
    for i in range(len(centres)):
        centres[i] = float(centres[i])

    params = IFU_params(instrument, timestamp, collapseaxis, angle, medwidth,
                        centres)

    return params


class IFU_params():

    def __init__(self, instrument, timestamp, collapseaxis, angle, medwidth,
                 centres):
        self.instrument = instrument
        self.timestamp = timestamp
        self.collapseaxis = collapseaxis
        self.angle = angle
        self.medwidth = medwidth
        self.centres = centres

###############################################################################


def analyse_IFU_flat(input_path=None, parampath=None, debug=False, logger=None,
                     collapse_axis=0, IFU_name=None, plots=True,
                     report_status=None, results=None):

    if report_status:
        report_status("Starting analysis...")

    """
    Analyse IFU flats to determine collapse parameters:
    1.  Determine orientation angle assuming small offset
    2.  Collapse flat to single row
    3.  Analyse row to determine spatial row locations
    4.  Write out the collapse parameters to file
    """

    # read in the data
    image_data = fits.getdata(input_path, ext=1)

    header = fits.getheader(input_path, ext=0)

    try:
        date = header['DATE'].replace("-", "")
        time = header['UT'].replace(":", "")
    except Exception:
        date, frame = get_date_frame(input_path)
        time = "000000"
    timestamp = (f"{date} {time}")

    instrument = header['INSTRUME']

    if "GNIRS" in instrument:
        try:
            decker = header['DECKER']
            collapse_axis = 0
            if "HR-IFU" in decker:
                instrument = "GNIRS-HR"
            elif "LR-IFU" in decker:
                instrument = "GNIRS-LR"
            collapse_axis = 0
        except Exception:
            instrument = IFU_name
    elif "NIFS" in instrument:
        OBSMODE = header['OBSMODE']
        if "IFU" in OBSMODE:
            collapse_axis = 1
    else:
        if logger:
            logger.debug("This frame is not an IFU frame!")
        return False

    if logger:
        logger.debug(f"Analysing {instrument} image...")

    # 1. Determine orientation angle assuming small offset.
    # 2. Collapse flat to a single row
    if logger:
        logger.info("Finding orientation angle")
    if report_status:
        report_status("Finding orientation angle...")
    angle, row, figure = find_rot_brute(image_data, plots=plots,
                                        collapse_axis=collapse_axis)

    # 3. Analyse row to determine spatial row locations
    if logger:
        logger.info("Determining spectra centres")
    if report_status:
        report_status("Determining spectra centres...")
    centres = find_spectra_centres(row, figure=figure, debug=debug,
                                   logger=logger, plots=plots)

    # 4. Write out the collapse parameters to a file

    # calculate median width
    spacings = np.insert(centres, 0, 0) - np.append(centres, 0)
    spacings = spacings[1:-1]

    medwidth = floor(abs(np.nanmedian(spacings)))

    logger.info("analyse_IFU_flat:")
    logger.info(centres)
    logger.info(type(centres))
    logger.info(type(centres[0]))
    dataline = (f'{instrument},{timestamp},{collapse_axis},{angle:3.1f},'
                f'{medwidth},({centres[0]:3.1f}')
    for centre in centres[1:]:
        dataline = dataline + (f",{centre:3.1f}")
    dataline = dataline + ")"

    # 5.  Print out synopsis information
    if logger:
        logger.debug(f'{len(centres)} lines found, '
                     f'with average spacing of {medwidth}')

    # Write out the plot
    if not os.path.exists(parampath):
        os.makedirs(parampath)

    if plots:
        figname = (f'IFUorientation_{instrument}_{timestamp[:-2]}.png')
        figname = figname.replace(" ", "_")
        fullpath = os.path.join(parampath, figname)
        if logger:
            logger.info(f"Writing out analysis plot to file {fullpath}")
        plt.savefig(fullpath)

    # Write out the parameters
    filename = ("IFUorientation_%s.csv" % instrument)
    fullpath = os.path.join(parampath, filename)
    if logger:
        logger.info(f"Writing out collapse parameters to file {fullpath}")

    path_exists = os.path.exists(fullpath)
    with open(fullpath, 'a') as f:
        if not path_exists:
            f.write('Instrument,Timetamp,CollapseAxis, Angle, MedWidth, '
                    'Centres\n')
        f.write(dataline+"\n")

    if results is not None:
        results.__init__(instrument, timestamp, collapse_axis, angle, medwidth,
                         centres)

    return True


def find_spectra_centres(row, figure=None, debug=False, logger=None,
                         plots=True):

    """
    Given a row of data, try to identify the IFU bins for reconstruction.
    """

    relaxed = 0.4
    cutoff = relaxed

    # Analyse the histogram

    hist, bins = np.histogram(row)

    # insert an initial zero so that the peakfinder can identify first peak
    phist = np.insert(hist, 0, 0.)
    pbins = np.insert(bins, 0, 0.)

    # find local extrema
    peaks = argrelextrema(phist, np.greater)[0]

    # This should have two primary peaks (first and last), with possibly
    # an interior peak

    if len(peaks) >= 2:
        lowcounts = pbins[peaks[0]]
        highcounts = pbins[peaks[-1]]
    else:
        if logger:
            logger.error("Error: Row histogram has %d peaks" % len(peaks))
        return

    if debug:
        print("debug: lowcounts =", lowcounts)
        print("debug: highcounts=", highcounts)

    # In order to locate the spectrum edges, we need to eliminate possible
    # count variations that take place elsewhere.

    margin = cutoff*(highcounts-lowcounts)

    cleaned = np.copy(row)

    cleaned[row < (lowcounts+margin)] = 0
    cleaned[row > (lowcounts+margin)] = 1

    # use a derivitive to locate the edges
    # This step seems likely to produce +/1 error due to integer edges
    dcleaned = np.diff(cleaned)

    risingedge = argrelextrema(dcleaned, np.greater)[0]
    fallingedge = argrelextrema(dcleaned, np.less)[0]

    centres = (risingedge+fallingedge)/2.0

    widths = fallingedge-risingedge

    if debug:
        print("debug: centres:")
        print(centres)
        print("debug: widths:")
        print(widths)

    """
    # First and last may be dodgy due to vignetting, so manually redo them
    centres[0] = centres[1]-medianwidth
    centres[-1] = centres[-2]+medianwidth
    """

    maxval = 1.05*np.max(row)

    if plots:
        if figure is None:
            plt.plot(row, color='blue')
            plt.plot(cleaned, color='red')
            plt.vlines(centres, 0, maxval, color='orange')
        else:
            fig, axs = figure
            axs[2].hlines(lowcounts+margin, 0, len(row), color='green')
            axs[2].vlines(centres, 0, maxval, color='orange')
            if debug:
                axs[2].plot(cleaned, color='red')
    return centres


def find_rot_brute(image_data, angleinc=0.1, return_row=True, debug=False,
                   return_fig=True, collapse_axis=0, plotfilename=None,
                   plots=True):

    """
    find_rot_brute is a rather brute-force rotation finder.
    Rotate the image over a small span of angles, and calculate
    the image michaelson contrast at each angle.  Take a gaussian
    fit of these for the angle that gives the optimal contrast.
    """

    anglestart = -2
    anglestop = 2

    def gauss(x, *p):
        A, mu, sigma = p
        return A*np.exp(-(x-mu)**2/(2.*sigma**2))

    bestangle = None
    bestcontrast = None

    anglearr = []
    contrastarr = []
    for i in np.arange(anglestart, anglestop, angleinc):

        # rotate
        rotdata = rotate(image_data, i)

        # collapse
        collapsedata = rotdata.sum(axis=collapse_axis)

        # calculate the Michelson contrast
        # compute min and max of collapsedata
        sample = collapsedata[200:800]
        try:
            mindat = np.min(sample)
            maxdat = np.max(sample)
        except Exception:
            pass

        # compute contrast
        contrast = (maxdat-mindat)/(maxdat+mindat)

        anglearr.append(i)
        contrastarr.append(contrast)

        if bestcontrast is None or contrast > bestcontrast:
            bestcontrast = contrast
            bestangle = i

    anglearr = np.array(anglearr)
    contrastarr = np.array(contrastarr)

    # Fit the contrast curve

    # The shape of the curve is weird.  We're going chop off the wings
    # and fit it as a gaussian.

    fitcontrastarr = contrastarr[np.where(contrastarr > 0.5)]
    fitanglearr = anglearr[np.where(contrastarr > 0.5)]

    p0 = [bestcontrast, bestangle, 1]

    coeff, var_matrix = curve_fit(gauss, fitanglearr, fitcontrastarr, p0=p0)

    angle_fit = gauss(anglearr, *coeff)
    A, fitangle, width = coeff

    # Rotate the image for demonstration purposes.

    rotdata = rotate(image_data, fitangle)
    collapsedata = rotdata.sum(axis=collapse_axis)

    # normalise the collapsedata

    collapsedata = np.add(collapsedata, np.min(collapsedata))
    collapsedata = np.divide(collapsedata, np.max(collapsedata))

    # Plot the results
    if plots:

        rc('text', usetex=False)

        fig, axs = plt.subplots(1, 3, figsize=(15, 5), sharey=False)

        axs[0].scatter(anglearr, contrastarr)
        axs[0].plot(anglearr, angle_fit, color='red',
                    label=(f'Gaussian $\phi$ = {fitangle:1.3f}'))
        axs[0].title.set_text('Contrast curve')
        axs[0].set_xlabel('Angle')
        axs[0].set_ylabel('Michaelson contrast')
        axs[0].legend()

        axs[1].imshow(rotdata)
        axs[1].title.set_text('Rotated frame')

        axs[2].plot(collapsedata)
        axs[2].title.set_text('Row centres')
        axs[2].set_ylabel('Normalised Counts')
        axs[2].set_xlabel('Row position on detector')

        """
        if plotfilename is not None:
            plt.savefig(plotfilename)
        else:
            plt.show()
        """
    else:
        fig = None
        axs = None

    # Write the angle to a file?  Or return it?
    returnlist = [fitangle]
    if return_row:
        returnlist = returnlist+[collapsedata]
    if return_fig:
        returnlist = returnlist+[(fig, axs)]

    return returnlist


def get_date_frame(fitspath):

    items = os.path.split(fitspath)
    prefix = items[-1].split('.')[0]
    prefix = prefix.replace("_IFU", "")
    date, frame = re.sub('[a-zA-Z]', ' ', prefix).lstrip().split()[0:2]

    frame = int(frame)
    return date, frame


###############################################################################


if __name__ == "__main__":
    print(sys.argv)
