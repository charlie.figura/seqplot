#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""
Program converts an estimate of the IQ (FWHM measured on an image using
e.g. IRAF) in pixels, into a FWHM in arcsecs. It corrects for airmass,
and provides the associated IQ band for this measurement.
It can also take seeing measurements from QAP or AO, and for all these methods
checks and then adds or updates the FWHM database, and image header if
prompted to.

Intended to run in the ``hbfsos-lv1`` environment.

The FWHM database is saved at  ``/gemsoft/var/log/pix2iq/gemFwhm_YYYMMDD.db``

TBD
----
- Py2 dependency with fixHead.py *environment* that is Py2
(see "... setenv PYTHONPATH $ {PYTHONPATH}:/usr/lib/python2.6/site-packages/")
– line ~865


ChangeLog
-----------
- 20210305: Initial gempyops migration (original version written by M. Pohlen)
- 20210306: Removed dautils dependencies (now gemlib and pystdlib),
            moved ``checkDB()`` out of ``main`` and properly indented
- 20210308: Updated ``(endTime-utConv)/2`` to ``(endTime-utConv)//2``,
            added parenthesis and print function to ``daLogDefs.py``,
            changed ``mean`` to ``np.mean``, ``xmlrpc.client`` added
- 20210310: Swapped `input` for `raw_input` to be run in Py2 for now,
            updated color class and TBD
- 20210315: Removed iqclass module dependency, moved functions to gemlib
- 20210316: Py3 working version. No ``daLogDefs`` and `pylab`` mentions,
            `raw_input` replace by `gempyops.pystdlib.prompt`, code cosmetics,
            `os.chmod(dbfile,0666)` to `os.chmod(dbfile,0o666)`
- 20210317: fix "No header update" problem, improve documentation and code
            indentation
- 20210318: removed pytz dependency (dateutil instead)
- 20210319: fix dateutil call
- 20230510: assorted cleanup
- 20230515: Adapted for use with seqplot2 & ginga
"""


# from __future__ import absolute_import, print_function, division
import sys
import os
import datetime
from pytz import timezone

#import seqplot_components.seq_pystdlib as pystdlib

try:
    import gemlib
    HAVE_GEMLIB = True
except Exception as e:
    print(e)
    #ImportError:
    
    HAVE_GEMLIB = False

"""
from importlib.machinery import SourceFileLoader
#site = os.environ['GEMINI_SITE'] if 'GEMINI_SITE' in os.environ else 'LOCAL'
#if site == 'MK' or site == 'CP':
GEMPYOPS_PATH = os.path.expanduser('~/sciops/gempyops')
lib_path = os.path.join(GEMPYOPS_PATH, 'gemlib.py')
gemlib = SourceFileLoader("gemlib", lib_path).load_module()
lib_path = os.path.join(GEMPYOPS_PATH, 'pystdlib.py')
pystdlib = SourceFileLoader("pystdlib", lib_path).load_module()
"""

sys.path.append(os.path.expanduser('~/sciops/gempyops'))


USERID = os.getuid()


import dataset

"""print("AAA")
try:
    import dataset
except Exception as e:
    print("EEEK",e)
#except ImportError:
    print('# Could not import Gemini specific modules...')
print("BBB")
"""

__version__ = "20230526"
__author__ = "B.Cooper, D.Faes, C.Figura"

DB_FILE_PATH = '/gemsoft/var/log/pix2iq/'
if not os.path.exists(DB_FILE_PATH):
    DB_FILE_PATH = "/tmp/"


class Args(object):
    def __init__(self):
        self.ao = False
        self.band = 'None'
        self.dateOfNight = None
        self.debug = False
        self.fileNumber = None
        self.fwhm = None
        self.initials = ''
        self.no = False
        self.qap = False
        self.wfsValue = '0'

class Color:
    """Can be used with ``print`` to ouptut text in 'color'.

    Parameters
    -----------
    Color. : str
        The selected color from this list:

        * DARKPURPLE
        * PURPLE 
        * MAGENTA 
        * DARKCYAN
        * CYAN
        * DARKBLUE
        * BLUE
        * DARKGREEN
        * GREEN
        * DARKYELLOW
        * YELLOW
        * DARKRED
        * RED
        * DARKGREY
        * GREY
        * BOLD
        * UNDERLINE 
        * END
        * PLAIN

    Example
    ---------
    ``print(Color.PURPLE+'This is a test!' + Color.END)``
    """
    DARKPURPLE = '\033[35m'
    PURPLE = '\033[95m'
    DARKMAGENTA = DARKPURPLE
    MAGENTA = PURPLE  
    DARKCYAN = '\033[36m'
    CYAN = '\033[96m'
    DARKBLUE = '\033[34m'
    BLUE = '\033[94m'
    DARKGREEN = '\033[32m'
    GREEN = '\033[92m'
    DARKYELLOW = '\033[33m'
    YELLOW = '\033[93m'
    DARKRED = '\033[31m'
    RED = '\033[91m'
    DARKGREY = '\033[90m'
    GREY = '\033[37m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'
    PLAIN = END


class Pix2IQ():

    # Pix2IQ object so that we don't have to pass everything back to
    # seq_examine.

    """
    It can also take seeing measurements from AO, and for all these
    methods, will check, and then add or update the FWHM database, and
    image header if prompted to.
    """

    def __init__(self, set_status=None, logger=None):

        self.db_check = {}
        self.set_status = set_status
        self.logger = logger

    def calculateIQ(self, headers, fwhm_pixels, fitspath, report=False):

        """Program converts an estimate of the IQ (FWHM measured on an image
        using e.g. IRAF) in pixels, into a FWHM in arcsecs. It corrects for
        airmass, and provides the associated IQ band for this measurement.
        """

        args = Args()

        # Options:
        debug = args.debug
        band = args.band
        ao = args.ao

        hdr0, hdr1 = headers[0], headers[1]

        airmass = hdr0['AIRMASS']
        instrume = hdr0['INSTRUME']
        aofold = hdr0['AOFOLD']
        utDate = hdr0['DATE-OBS']
        azimuth = str(round(hdr0['AZIMUTH'], 0))  # use only rounded val

        try:
            utStart = hdr0['UTSTART']
        except KeyError:
            try:
                utStart = hdr0['UT']
            except KeyError:
                utStart = None
        try:
            utEnd = hdr0['UTEND']
        except KeyError:
            utEnd = None
        expTime = hdr0['EXPTIME']

        rawIq = hdr0['RAWIQ'].replace('-percentile', '')
        effWavelength = hdr0['WAVELENG']/10000.  # org in [A]

        if debug:
            print(f"airmass {airmass} instrume {instrume} aofold {aofold} "
                  f"utData {utDate} utStart {utStart} utEnd {utEnd} "
                  f"expTime {expTime} rawIq {rawIq} "
                  f"effWavelength {effWavelength}")

        # Instrument specific header keywords:
        if instrume[:-2] != 'GMOS':
            coadds = hdr0['COADDS']
        else:
            coadds = 1

        if instrume[:-2] == 'GMOS':
            detector = hdr0['DETECTOR']

        if debug:
            print('rawIq header:', rawIq)

        gmosSpec = False
        gnirsXd = False
        gnirsSpec = False
        gnirsImag = False

        head_instspec = ''
        if instrume == 'NIRI':
            head_instspec = hdr0['CAMERA']
            centwave = effWavelength
        elif instrume == 'F2':
            centwave = effWavelength
        elif instrume == 'NIFS':
            centwave = hdr0["GRATWAVE"]  # org [mum]
            if not ao:
                msg = ("NB: FWHM from reconstructed NIFS images are expected!")
                self.set_status.warning(msg)
                head_instspec = 'iraf'
        elif instrume[:-2] == 'GMOS':
            ccdsum = hdr1["CCDSUM"]
            xBin = ccdsum.split(' ')[0]
            yBin = ccdsum.split(' ')[1]
            head_instspec = str(xBin)
            grating = hdr0["GRATING"]
            if grating != 'MIRROR':
                centwave = hdr0["CENTWAVE"]/1000.  # in [nm]
                gmosSpec = True
                # use the y-binning for spectra (LAF)
                head_instspec = str(yBin)
            else:
                centwave = effWavelength

        elif instrume == 'GNIRS':
            acqMir = hdr0["ACQMIR"]
            decker = hdr0["DECKER"]
            centwave = hdr0["GRATWAVE"]  # org [mum]
            if acqMir != 'In':
                gnirsSpec = True
            else:
                gnirsImag = True
            if 'XD' in decker:
                gnirsXd = True
            cameraLong = hdr0["CAMERA"]
            camera = cameraLong.split("_", 1)[0]
            if camera[:5] == 'Short':
                head_instspec = 'short'
            elif camera[:4] == 'Long':
                head_instspec = 'long'
            else:
                self.set_status("--> WARNING, GNIRS camera not detected, ask "
                                "software custodian for help.", warning=True)

        if debug:
            print('')
            print('gmosSpec  = ', gmosSpec)
            print('gnirsXd   = ', gnirsXd)
            print('gnirsSpec = ', gnirsSpec)
            print('gnirsImag = ', gnirsImag)
            print('')

        # Get wavefront sensor:
        pwfs1State = hdr0["PWFS1_ST"]
        pwfs2State = hdr0["PWFS2_ST"]
        oiwfsState = hdr0["OIWFS_ST"]
        aowfsState = hdr0["AOWFS_ST"]

        wfs = 'None'

        if pwfs1State in ['frozen', 'guiding']:
            wfs = 'PWFS1'
        if pwfs2State in ['frozen', 'guiding']:
            wfs = 'PWFS2'
        if oiwfsState in ['frozen', 'guiding']:
            wfs = 'OIWFS'
        if aowfsState in ['frozen', 'guiding']:
            wfs = 'AOWFS'

        # Check if observation is with Altair:
        if aofold == 'IN':
            ao = True
            if wfs != "AOWFS":
                self.set_status("==> Note: astrodata did not recognize Altair "
                                "as the WFS for this image, so something is "
                                "wrong with the header (image?).",
                                warning=True)
            wfs = "AOWFS"
            aoseeingHeader = hdr0['AOSEEING']
            if debug:
                print('aoseeingHeader: ', aoseeingHeader)
            if debug:
                print('wfs, ao: ', wfs, ao)
            self.set_status("AO fold is in!")

        # Convert beginning and end times to seconds (for AO, db storage):
        if debug:
            print("utStart,utEnd: ", utStart, utEnd)

        utConv = datetime.datetime.strptime(utStart, '%H:%M:%S.%f')

        if utEnd is None:
            endTime = utConv + datetime.timedelta(seconds=coadds*expTime)
            midTime = utConv + datetime.timedelta(seconds=0.5*coadds*expTime)
            utMid = midTime.strftime("%H:%M:%S")
            utEnd = endTime.strftime("%H:%M:%S.%f")
        else:
            endTime = datetime.datetime.strptime(utEnd, '%H:%M:%S.%f')
            midTime = utConv + (endTime-utConv)//2
            utMid = midTime.strftime("%H:%M:%S")

        #######################################################################
        # Determine filter

        # Get filterName:
        # Use centwave to derive generic filter:
        if gnirsXd and not ao:
            if band != 'none':
                filterName = band
                if debug:
                    print('Assuming FWHM was measured in band: '+band)
            else:
                prompt = ('Enter name of band where FWHM was measured '
                          '(K,H,J from left to right on GNIRS-XD): ')
                print("OH NOES YOU DIDN'T FIX THIS!")
#                filterName = pystdlib.prompt(prompt)
        elif gnirsImag:
            myFormat = '%Y-%m-%d'
            otChange = datetime.datetime.strptime('2016-12-06', myFormat)
            currFile = datetime.datetime.strptime(hdr0['DATE'], myFormat)
            if currFile < otChange:
                filterName = hdr0["FILTER2"].split('_')[0]
                if filterName not in ['J', 'H', 'K', 'L', 'M']:
                    filterName = hdr0["FILTER1"].split('_')[0]
                    if filterName not in ['J', 'Y', 'K']:
                        filterName = 'K'
                        """
                        if band == 'none':
                            print(f'{Color.DARKRED}GNIRS filtername '
                                  'not recognised, assuming K, please check!\n'
                                  'In case use -b <filterName> to use a '
                                  f'different filter!{Color.END}')
                        else:
                            print(f'{Color.DARKRED}GNIRS filtername '
                                  f'not recognised, using K as default!'
                                  f'{Color.END}')
                        """
                if debug:
                    print()
                    print(f'Filter taken from header: {filterName}')
            else:
                # For GNIRS acq. images use effWavelength and not centwave
                filterName = gemlib.filterClose(effWavelength)
                if debug:
                    print(f'Closest filtername to effWavelength: {filterName}')

        else:
            filterName = gemlib.filterClose(centwave)
            if debug:
                print()
                print(f'Closest filtername to ctlwl: {filterName}')

        # Allow manual overwrite of filter:
        if not gnirsXd and band != 'None':
            print(f"{Color.DARKRED}Manual override of auto filter! "
                  f"Using {band} instead of {filterName}{Color.END}")
            filterName = band

        # AO specific case:
        if ao:
            # V no longer available for >=2014B
            filterName = 'v2'
            self.set_status(f"ao = {ao}, filterName set to {filterName}")

        #######################################################################
        # Calculate seeing in [arcsec] and derive IQ band:

        # Set GMOS platescale according to CCD:
        gmosCcd = ''
        if instrume[:-2] == 'GMOS':
            if 'Hamamatsu' in detector:
                gmosCcd = 'hamamatsu'
            else:
                gmosCcd = 'notHam_amatsu'  # gemlib looks for 'hama' in lower()

        # Calculate FWHM in arcseconds, corrected for airmass
        fwhmCorr = gemlib.platescale(instrume, head_instspec, fwhm_pixels,
                                     airmass, verbose=False, gmos=gmosCcd)

        # FWHM delivered to the instrument (uncorrected for airmass)
        fwhmDeli = fwhmCorr / (airmass**(-0.6)) if airmass > 1 else fwhmCorr

        # Use P2 for unguided to get an IQ reference:
        if wfs == 'None':
            wfs = 'PWFS2'

        # determine IQ for value
        rawiq = gemlib.iqclass(filterName, wfs, fwhmCorr, fuzzy=True,
                               ranges=True, ao_est=True)

        # Catch cases where gemlib.iqclass results in ?,? as output:
        if rawiq[0] == '?':
            # Assume filterName is unknown:
            print('')
            print('---> Note, gemlib.iqclass returned "?" for IQ. Assuming '
                  'filterName is not recognised so trying to determine filter '
                  'from ctrlwvl. Please double check!')
            print('')
            if gmosSpec:
                wave = centwave
            else:
                wave = effWavelength
            filterName = gemlib.filterClose(wave)
            rawiq = gemlib.iqclass(filterName, wfs, fwhmCorr, fuzzy=True,
                                   ranges=True, ao_est=True)

#        dateOfNight = pystdlib.get_utcdate()

#        print('A')  
        dateOfNight = datetime.datetime.now(timezone('UTC').strftime("%Y%m%d"))
#        print('B')
        self.fitsprefix = fitspath[-19:-5]
#        print('C')
        self.dbfile = os.path.join(DB_FILE_PATH, f'gemFwhm_{dateOfNight}.db')
        self.logger.info(f'dbfile = {self.dbfile}')

        """
        # kludge fix for write permissions
        for ext in ['shm', 'val']:
            filepath = f'{self.dbfile}-{ext}'
            if os.path.exists(filepath) and not os.access(filepath, os.W_OK):
                # it exists, but we don't have write access:
                os.remove(filepath)
        """

        if os.path.exists(DB_FILE_PATH):
            # Check database for current entry
            self.checkDB_args = {'ao': args.ao, 'spec': (gmosSpec or gnirsXd),
                                 'fwhmDeli': fwhmDeli, 'fwhmCorr': fwhmCorr}
            action = self.checkDB(**self.checkDB_args)

        # setup the entry for the database
        self.new_entry = {'utDate': utDate,
                          'utTime': utMid,
                          'instrument': instrume,
                          'iqBand': filterName,
                          'fwhmDeli': str(round(fwhmDeli, 2)),
                          'fwhmCorr': str(round(fwhmCorr, 2)),
                          'azimuth': azimuth,
                          'airmass':  str(round(airmass, 1)),
                          'effWavelength': str(round(effWavelength, 3)),
                          'method':  self.method,
                          'filename': self.fitsprefix
                          }

        return (rawiq[0], fwhmDeli, fwhmCorr, action)

    def updateDB(self):

        # fileName is fitspath

        table, db = self.checkDB(**self.checkDB_args, return_results=True)

        # Modify header and/or db entry:
        if self.dbUpdate or self.dbUpload:
            try:
                msg_base = f"FWHM database entry for {self.fitsprefix}"
                if self.dbUpdate:

                    # Update db:
                    table.update(self.new_entry, ['filename'])
                    self.set_status(f"{msg_base} updated.", info=True)
                if self.dbUpload:
                    table.insert(self.new_entry)
                    self.set_status(f"{msg_base} added.", info=True)

                # If dbfile needs updating, check ownership & change perms
                if self.dbUpdate or self.dbUpload:

                    # Check UID
                    fileUserID = os.stat(self.dbfile).st_uid

                    # If owner of file allow all to read/write:
                    if fileUserID == USERID:
                        os.chmod(self.dbfile, 0o666)
                success = True

            except Exception as e:
                msg = ("updateDB encountered problems updating FWHM database "
                       f"{self.dbfile}")
                self.set_status(msg, error=True)
                self.logger.error(e)

                success = False

        db.close()

        return success

    def checkDB(self, ao, spec, fwhmDeli, fwhmCorr,
                return_results=False):

        """ Checks the FWHM database for an entry for the current file. If
        there isn't one, it sets ``dbUpload`` to ``True``, and if there is and
        the delivered and corrected IQ values differ from what's in the db,
        it sets ``dbUpdate`` to ``True``.

        Parameters
        -----------
        dbfile : str
            Name of FWHM db file ``/gemsoft/var/log/pix2iq/gemFwhm_YYYMMDD.db``

        fileName : str
            Full filename.

        ao : bool
            AO option for pix2iq3.py.

        debug : bool
            Debug option for pix2iq3.py.

        spec : bool
            True if GMOS spectroscopy or GNIRS cross-dispersion observation

        gmosSpec : bool
            True if GMOS spectroscopy observation (info within header).

        gnirsXd : bool
            True if GNIRS cross-dispersion observation (info within header).

        fwhmDeli : float
             Delivered IQ from QAP or AO or manual FWHM measurement.

        fwhmCorr : float
            Zenith-corrected IQ from QAP or AO or manual FWHM measurement.

        Returns
        --------
        dbUpload : bool
            Returns ``True`` if a new entry is to be created in the db file

        dbUpdate : bool
            Returns ``True`` if an already exisiting entry is to be updated

        filenameEntry : str
            A substring of the file name, as the file name entry in ``table``.

        table : dict
            A dictionary of the FWHM db entries for the date.

        method : str
            Method of fwhm determination (i.e. 'imaging' or 'AlairWFS' ).
        """

        # Check db content:
        dbUpload = False
        dbUpdate = False

        print(f'seq_pix2iq3.checkDB: self.dbfile = {self.dbfile}')
        try:

            # Connect to db:
            db = dataset.connect('sqlite:///'+self.dbfile,
                                 sqlite_wal_mode=False)

            table = db['FWHM']

            # Collect values:
            if ao:
                method = 'AltairWFS'
            elif spec:
                method = 'cross-cut'
            else:
                method = 'imaging'

            self.method = method

            # Check if entries for file already there:
            entry = table.find_one(filename=self.fitsprefix)

            # Update or upload delivered and corrected iq values:
            if entry is not None:
                if entry['fwhmDeli'] != str(round(fwhmDeli, 2)):
                    dbUpdate = True
                if entry['fwhmCorr'] != str(round(fwhmCorr, 2)):
                    dbUpdate = True
            else:
                # print 'Image not in FWHM database!'
                dbUpload = True

        except Exception as e:
            msg = "Encountered problems when accessing the FWHM database!"
            if self.logger is not None:
                self.logger.error(msg)
                self.logger.error(e)
                self.logger.info(f'dbfile = {self.dbfile}')
                print('seq_pix2iq3.checkDB: error:')
                print(msg)
                print(e)
                print(f'dbfile = {self.dbfile}')

            else:
                print(msg)
                print(e)

            db.close()

            # return False / NULL if except is encountered
            return None

        self.dbUpload = dbUpload
        self.dbUpdate = dbUpdate

        print(f'seq_pix2iq3.checkDB: dbUpload = {dbUpload}  '
              f'dbUpdate = {dbUpdate}')

        if return_results:
            return table, db
        else:
            # close the sql database
            db.close()

            if dbUpload:
                action = "Add to"
            elif dbUpdate:
                action = "Update"
            else:
                action = None
            return action


def round_to(n, precision):
    """
    Rounds a number ``n`` using specified precision, with correction of 0.5.
    """

    correction = 0.5 if n >= 0 else -0.5
    return int(n/precision+correction) * precision


def roundto05(n):
    """Rounds a number ``n`` to the nearest 0.05."""
    return round_to(n, 0.05)
