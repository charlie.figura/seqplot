"""
Modification of ginga 'pick' global plugin, adapting it to use with the
seqplot2 global plugin.  Main modifications include:
- adaptation to work as a global plugin in conjunction with seqplot instead of
  as a local plugin
- modification of presented information
- addition of pix2insight functionality
- capability to add IQ to database
- capability to update IQ for image

This plugin acts as a combined pyraf/imexamine and pix2insight3

TBD
-----
- See C Figura's evernote page

Changelog
___________
- 2023.05.15:  initial release candidate
"""


import threading
import os
import sys
import traceback
import time
from collections import OrderedDict

import numpy as np

from ginga.gw import Widgets, Viewers
from ginga.misc import Bunch
from ginga.util import wcs, contour
from ginga import GingaPlugin, cmap, trcalc

from astropy import wcs as ast_wcs
from astropy import units as ast_units
import re

try:
    from ginga.gw import Plot
    from ginga.util import plots
    have_mpl = True
except ImportError:
    have_mpl = False

import seqplot_components.mywidgets as mywidgets




try:
    import seqplot_components.seq_pix2iq3 as seq_pix2iq3
    HAVE_PIX = True
except Exception as e:
    print(f'seq_examine: error on seq_pix2iq3 import: {e}')
    # except ImportError:
    HAVE_PIX = False

import seqplot_components.seq_utils as seq_utils


__version__ = '20230823'
__author__ = 'cfigura'

# ORIGINAL HEADER FOLLOWS

# This is open-source software licensed under a BSD license.
# Please see the file LICENSE.txt for details.
"""Perform quick astronomical stellar analysis.

**Plugin Type: Local**

``Pick`` is a local plugin, which means it is associated with a channel.
An instance can be opened for each channel.

**Usage**

The ``Pick`` plugin is used to perform quick astronomical data quality analysis
on stellar objects.  It locates stellar candidates within a drawn box
and picks the most likely candidate based on a set of search settings.
The Full Width Half Max (FWHM) is reported on the candidate object, as
well as its size based on the plate scale of the detector.  Rough
measurement of background, sky level and brightness is also done.

**Defining the pick area**

The default pick area is defined as a box of approximately 30x30
pixels that encloses the search area.

The move/draw/edit selector at the bottom of the plugin is used to
determine what operation is being done to the pick area:

.. figure:: figures/pick-move-draw-edit.png
   :width: 400px
   :align: center
   :alt: Move, Draw and Edit buttons

   "Move", "Draw", and "Edit" buttons.

* If "move" is selected, then you can move the existing pick area by
  dragging it or clicking where you want the center of it placed.
  If there is no existing area, a default one will be created.
* If "draw" is selected, then you can draw a shape with the cursor
  to enclose and define a new pick area.  The default shape is a
  box, but other shapes can be selected in the "Settings" tab.
* If "edit" is selected, then you can edit the pick area by dragging its
  control points, or moving it by dragging in the bounding box.

After the area is moved, drawn or edited, ``Pick`` will perform one of three
actions:

1. In "Quick Mode" ON, with "From Peak" OFF, it will simply attempt to
   perform a calculation based on the coordinate under the crosshair in
   the center of the pick area.
2. In "Quick Mode" ON, with "From Peak" ON, it will perform a quick
   detection of peaks in the pick area and perform a calculation on the
   first one found, using the peak's coordinates.
3. In "Quick Mode" OFF, it will search the area for all peaks and
   evaluate the peaks based on the criteria in the "Settings" tab of the UI
   (see "The Settings Tab" below) and try to locate the best candidate
   matching the settings.

**If a candidate is found**

The candidate will be marked with a point (usually an "X") in the
channel viewer canvas, centered on the object as determined by the
horizontal and vertical FWHM measurements.

The top set of tabs in the UI will be populated as follows:

.. figure:: figures/pick-cutout.png
   :width: 400px
   :align: center
   :alt: Image tab of Pick area

   "Image" tab of ``Pick`` area.

The "Image" tab will show the contents of the cutout area.
The widget in this tab is a Ginga widget and so can be zoomed and panned
with the usual keyboard and mouse bindings (e.g., scroll wheel).  It will
also be marked with a point centered on the object and additionally the
pan position will be set to the found center.

.. figure:: figures/pick-contour.png
   :width: 300px
   :align: center
   :alt: Contour tab of Pick area

   "Contour" tab of ``Pick`` area.

The "Contour" tab will show a contour plot.
This is a contour plot of the area immediately surrounding the
candidate, and not usually encompassing the entire region of the pick
area.  You can use the scroll wheel to zoom the plot and a click of the
scroll wheel (mouse button 2) to set the pan position in the plot.

.. figure:: figures/pick-fwhm.png
   :width: 400px
   :align: center
   :alt: FWHM tab of Pick area

   "FWHM" tab of ``Pick`` area.

The "FWHM" tab will show a FWHM plot.
The purple lines show measurements in the X direction and the green lines
show measurements in the Y direction.  The solid lines indicate actual
pixel values and the dotted lines indicate the fitted 1D function.
The shaded purple and green regions indicate the FWHM measurements for the
respective axes.

.. figure:: figures/pick-radial.png
   :width: 400px
   :align: center
   :alt: Radial tab of Pick area

   "Radial" tab of ``Pick`` area.

The "Radial" tab contains a radial profile plot.
Plotted points in purple are data values, and a line is fitted to the
data.

.. figure:: figures/pick-ee.png
   :width: 600px
   :align: center
   :alt: EE tab of Pick area

   "EE" tab of ``Pick`` area.

The "EE" tab contains a plot of fractional encircled and ensquared energies
(EE) in purple and green, respectively, for the chosen target. Simple
background subtraction is done in a way that is consistent with FWHM
calculations before EE values are measured. The sampling and total radii,
shown as black dashed lines, can be set in the "Settings" tab; when these are
changed, click "Redo Pick" to update the plot and measurements.
The measured EE values at the given sampling radius are also displayed in the
"Readout" tab. When reporting is requested, the EE values at the given sampling
radius and the radius itself will be recorded under "Report" table, along with
other information.

When "Show Candidates" is active, the candidates near the edges of the bounding
box will not have EE values (set to 0).

.. figure:: figures/pick-readout.png
   :width: 400px
   :align: center
   :alt: Readout tab of Pick area

   "Readout" tab of ``Pick`` area.

The "Readout" tab will be populated with a summary of the measurements.
There are two buttons and three check boxes in this tab:

* The "Default Region" button restores the pick region to the default
  shape and size.
* The "Pan to pick" button will pan the channel viewer to the
  located center.
* The "Quick Mode" check box toggles "Quick Mode" on and off.
  This affects the behavior of the pick region as described above.
* The "From Peak" check box changes the behavior of "Quick Mode" slightly
  as described above.
* If "Center on pick" is checked, the shape will be recentered on the
  located center, if found (i.e., the shape "tracks" the pick).

.. figure:: figures/pick-controls.png
   :width: 400px
   :align: center
   :alt: Controls tab of Pick area

   "Controls" tab of ``Pick`` area.

The "Controls" tab has a couple of buttons that will work off of the
measurements.

* The "Bg cut" button will set the low cut level of the channel viewer
  to the measured background level.  A delta to this value can be
  applied by setting a value in the "Delta bg" box (press "Enter" to
  change the setting).
* The "Sky cut" button will set the low cut level of the channel viewer
  to the measured sky level.  A delta to this value can be
  applied by setting a value in the "Delta sky" box (press "Enter" to
  change the setting).
* The "Bright cut" button will set the high cut level of the channel
  viewer to the measured sky+brightness levels. A delta to this value
  can be applied by setting a value in the "Delta bright" box
  (press "Enter" to change the setting).

.. figure:: figures/pick-report.png
   :width: 400px
   :align: center
   :alt: Report tab of Pick area

   "Report" tab of ``Pick`` area.

The "Report" tab is used to record information about the measurements in
tabular form.

By pressing the "Add Pick" button, the information about the most recent
candidate is added to the table.  If the "Record Picks automatically"
checkbox is checked, then any candidates are added to the table
automatically.

.. note:: If the "Show Candidates" checkbox in the "Settings" tab is
          checked, then *all* objects found in the region (according to
          the settings) will be added to the table instead of just the
          selected candidate.

You can clear the table at any time by pressing the "Clear Log" button.
The log can be saved to a table by putting a valid path and
filename in the "File:" box and pressing "Save table". File type is
automatically determined by the given extension (e.g., ".fits" is FITS and
".txt" is plain text).

**If no candidate is found**

If no candidate can be found (based on the settings), then the pick area
is marked with a red point centered on the pick area.

.. figure:: figures/pick-no-candidate.png
   :width: 800px
   :align: center
   :alt: Marker when no candidate found

   Marker when no candidate found.

The image cutout will be taken from this central area and so the "Image"
tab will still have content.  It will also be marked with a central red
"X".

The contour plot will still be produced from the cutout.

.. figure:: figures/pick-contour-no-candidate.png
   :width: 400px
   :align: center
   :alt: Contour when no candidate found.

   Contour when no candidate found.

All the other plots will be cleared.

**The Settings Tab**

.. figure:: figures/pick-settings.png
   :width: 400px
   :align: center
   :alt: Settings tab of Pick plugin

   "Settings" tab of ``Pick`` plugin.

The "Settings" tab controls aspects of the search within the pick area:

* The "Show Candidates" checkbox controls whether all detected sources
  are marked or not (as shown in the figure below).  Additionally, if
  checked, then all the found objects are added to the pick log table
  when using the "Report" controls.
* The "Draw type" parameter is used to choose the shape of the pick area
  to be drawn.
* The "Radius" parameter sets the radius to be used when finding and
  evaluating bright peaks in the image.
* The "Threshold" parameter is used to set a threshold for peak finding;
  if set to "None", then a reasonable default value will be chosen.
* The "Min FWHM" and "Max FWHM" parameters can be used to eliminate
  certain sized objects from being candidates.
* The "Ellipticity" parameter is used to eliminate candidates based on
  their asymmetry in shape.
* The "Edge" parameter is used to eliminate candidates based on how
  close to the edge of the cutout they are.  *NOTE: currently this
  works reliably only for non-rotated rectangular shapes.*
* The "Max side" parameter is used to limit the size of the bounding box
  that can be used in the pick shape.  Larger sizes take longer to
  evaluate.
* The "Coordinate Base" parameter is an offset to apply to located
  sources.  Set to "1" if you want sources pixel locations reported
  in a FITS-compliant manner and "0" if you prefer 0-based indexing.
* The "Calc center" parameter is used to determine whether the center
  is calculated from FWHM fitting ("fwhm") or centroiding ("centroid").
* The "FWHM fitting" parameter is used to determine which function is
  is used for FWHM fitting ("gaussian" or "moffat"). The option to use
  "lorentz" is also available if "calc_fwhm_lib" is set to "astropy"
  in ``~/.ginga/plugin_Pick.cfg``.
* The "Contour Interpolation" parameter is used to set the interpolation
  method used in rendering the background image in the "Contour" plot.
* The "EE total radius" defines the radius (for encircled energy) and box
  half-width (for ensquared energy) in pixels where EE fraction is expected to
  be 1 (i.e., all the flux for a point-spread function is contained within).
* The "EE sampling radius" is the radius in pixel used to sample the measured
  EE curves for reporting.

The "Redo Pick" button will redo the search operation.  It is convenient
if you have changed some parameters and want to see the effect based on the
current pick area without disturbing it.

.. figure:: figures/pick-candidates.png
   :width: 600px
   :align: center
   :alt: The channel viewer when "Show Candidates" is checked.

   The channel viewer when "Show Candidates" is checked.

**User Configuration**

"""

region_default_width = 30
region_default_height = 30

__all__ = ['Examine']

COLOR_A = 'orange'



# commented out 2024/04/05
#component_paths = ['/net/mko-nfs/gem/home/telops/perm/cfigura/seqplot/plugins',
#                   '/Users/cfigura/.ginga/plugins']
#component_path = [path for path in component_paths if os.path.exists(path)][0]
#sys.path += [component_path]


DB_FILE_PATH = '/gemsoft/var/log/pix2iq/'
HAVE_DB = os.path.exists(DB_FILE_PATH)


class FWHMPlot2(plots.FWHMPlot):

    def __init__(self, *args, **kwargs):
        super(FWHMPlot2, self).__init__(*args, **kwargs)

    def _plot_fwhm_axis(self, arr, iqcalc, skybg, color1, color2, color3,
                        fwhm_method='gaussian'):
        N = len(arr)
        X = np.array(list(range(N)))
        Y = arr
        # subtract sky background
        Y = Y - skybg
        maxv = Y.max()
        # clamp to 0..max
        Y = Y.clip(0, maxv)
        self.logger.debug("Y=%s" % (str(Y)))
        self.ax.plot(X, Y, color=color1, marker='o', linestyle='None')

        res = iqcalc.calc_fwhm(arr, medv=skybg, method_name=fwhm_method)
        fwhm, mu = res.fwhm, res.mu

        # Make a little smoother fitted curve by plotting intermediate
        # points
        XN = np.linspace(0.0, float(N), N * 10)
        Z = np.array([res.fit_fn(x, res.fit_args) for x in XN])
        self.ax.plot(XN, Z, color=color1, linestyle='-')
        self.ax.axvspan(mu - fwhm * 0.5, mu + fwhm * 0.5,
                        facecolor=color3, alpha=0.25)
        return fwhm


class Examine(GingaPlugin.LocalPlugin):

    def __init__(self, fv, fitsimage, canvas, active, currentFrame,
                 set_status=None, logger=None):
        # superclass defines some variables for us, like logger

        super(Examine, self).__init__(fv, fitsimage[active])

        self.set_status = set_status
        self.logger = self.logger

        self.active = active
        self.fitsimage = fitsimage
        self.currentFrame = currentFrame

        self.layertag = 'pick-canvas'
        self.pickimage = None
        self.pickcenter = None
        self.pick_qs = None
        self.pick_obj = None
        self._textlabel = 'Current'

        self.contour_image = None
        self.contour_plot = None
        self.fwhm_plot = None
        self.radial_plot = None
        self.contour_interp_methods = trcalc.interpolation_methods

        # types of pick shapes that can be drawn
        self.drawtypes = ['box', 'squarebox', 'rectangle',
                          'circle', 'ellipse',
                          'freepolygon', 'polygon',
                          ]

        # get Pick preferences
        prefs = self.fv.get_preferences()
        self.settings = prefs.create_category('plugin_Pick')
        self.settings.load(onError='silent')

        self.sync_preferences()

        self.pick_x1 = 0
        self.pick_y1 = 0
        self.pick_data = None
        self.pick_log = None
        self.dx = region_default_width
        self.dy = region_default_height
        # For offloading intensive calculation from graphics thread
        self.serialnum = 0
        self.lock = threading.RLock()
        self.lock2 = threading.RLock()
        self.ev_intr = threading.Event()
        self._wd, self._ht = 400, 400
        self._split_sizes = [self._ht, self._ht]

        self.last_rpt = []
        self.rpt_dict = OrderedDict({})
        self.rpt_cnt = 0
        self.rpt_tbl = None
        self.rpt_mod_time = 0.0
        self.rpt_wrt_time = 0.0
        self.rpt_wrt_interval = self.settings.get('report_write_interval',
                                                  30.0)

        if self.iqcalc_lib == 'astropy':
            self.logger.debug('Using iqcalc_astropy')
            from ginga.util import iqcalc_astropy as iqcalc
        else:  # Falls back to native
            self.logger.debug('Using native iqcalc')
            from ginga.util import iqcalc

        if not iqcalc.have_scipy:
            raise ImportError('Please install scipy to use this plugin')

        self.iqcalc = iqcalc.IQCalc(self.logger)
        self.copy_attrs = ['transforms', 'cutlevels']
        if (self.settings.get('pick_cmap_name', None) is None and
                self.settings.get('pick_imap_name', None) is None):
            self.copy_attrs.append('rgbmap')

        self.dc = self.fv.get_draw_classes()

        self.canvas = canvas
        canvas[self.active].add_draw_mode('move-examine', down=self.btn_down,
                                          move=self.btn_drag, up=self.btn_up)

        # Values for display when search fails:
        self.x = None
        self.y = None
        self.median_background = None

        # For pix2insight averaging
        self.fwhm_raw = np.NaN
        self.fwhm_arr = []
        self.pix = None
        if HAVE_PIX:
            self.pix = seq_pix2iq3.Pix2IQ(set_status=set_status, logger=logger)

        self.have_mpl = have_mpl
        self.gui_up = False

    def set_canvas_actions(self, canvas):

        canvas.set_drawtype(self.pickshape, color=COLOR_A, linestyle='dash')
        canvas.set_callback('draw-event', self.draw_cb)
        canvas.set_callback('edit-event', self.edit_cb)

    def sync_preferences(self):
        # Load various preferences
        self.pickcolor = self.settings.get('color_pick', 'green')
        self.pickshape = self.settings.get('shape_pick', 'circle')
        if self.pickshape not in self.drawtypes:
            self.pickshape = 'circle'
        self.candidate_color = self.settings.get('color_candidate', 'orange')
        self.quick_mode = self.settings.get('quick_mode', False)
        self.from_peak = self.settings.get('quick_from_peak', True)

        # Peak finding parameters and selection criteria
        self.max_side = self.settings.get('max_side', 1024)
        self.radius = self.settings.get('radius', 25)
        self.ee_total_radius = self.settings.get('ee_total_radius', 10.0)
        self.ee_sampling_radius = self.settings.get('ee_sampling_radius', 2.5)
        self.threshold = self.settings.get('threshold', None)
        self.min_fwhm = self.settings.get('min_fwhm', 1.5)
        self.max_fwhm = self.settings.get('max_fwhm', 50.0)
        self.min_ellipse = self.settings.get('min_ellipse', 0.5)
        self.edgew = self.settings.get('edge_width', 0.01)
        self.show_candidates = self.settings.get('show_candidates', True)
        # Report in 0- or 1-based coordinates
        coord_offset = self.fv.settings.get('pixel_coords_offset', 0.0)
        self.pixel_coords_offset = self.settings.get('pixel_coords_offset',
                                                     coord_offset)
        self.center_algs = ['fwhm', 'centroid']
        self.center_alg = self.settings.get('calc_center_alg', 'fwhm')
        self.fwhm_algs = ['gaussian', 'moffat']
        self.iqcalc_lib = self.settings.get('calc_fwhm_lib', 'native')
        if self.iqcalc_lib == 'astropy':
            self.fwhm_algs.append('lorentz')
        self.fwhm_alg = self.settings.get('calc_fwhm_alg', 'moffat')
        self.center_on_pick = self.settings.get('center_on_pick', True)

        # For controls
        self.delta_bg = self.settings.get('delta_bg', 0.0)
        self.delta_sky = self.settings.get('delta_sky', 0.0)
        self.delta_bright = self.settings.get('delta_bright', 0.0)

        # Formatting for reports
        self.do_record = self.settings.get('record_picks', False)
        columns = [("RA", 'ra_txt'), ("DEC", 'dec_txt'),
                   ("Equinox", 'equinox'),
                   ("X", 'x'), ("Y", 'y'), ("FWHM", 'fwhm'),
                   ("FWHM_X", 'fwhm_x'), ("FWHM_Y", 'fwhm_y'),
                   ("Star Size", 'starsize'),
                   ("Ellip", 'ellipse'), ("Background", 'background'),
                   ("Sky Level", 'skylevel'), ("Brightness", 'brightness'),
                   ("Time Local", 'time_local'), ("Time UT", 'time_ut'),
                   ("RA deg", 'ra_deg'), ("DEC deg", 'dec_deg'),
                   ]
        self.rpt_columns = self.settings.get('report_columns', columns)

        # For contour plot
        self.num_contours = self.settings.get('num_contours', 8)
        self.contour_size_max = self.settings.get('contour_size_limit', 100)
        self.contour_size_min = self.settings.get('contour_size_min', 30)
        self.contour_interp = self.settings.get('contour_interpolation',
                                                'nearest')

        # for pix2iq3 use
        self.db = None

    def build_gui(self, vtop):

        vtop.set_border_width(0)
        box, sw, orientation = Widgets.get_oriented_box(vtop)

        box.set_border_width(0)
        box.set_spacing(0)

        paned = Widgets.Splitter(orientation=orientation)
        self.w.splitter = paned

        nb = Widgets.TabWidget(tabpos='bottom')
        self.w.nb1 = nb
        paned.add_widget(Widgets.hadjust(nb, orientation))

        cm, im = self.fv.cm, self.fv.im

        # Set up "Image" tab viewer
        di = Viewers.CanvasView(logger=self.logger)
        width, height = self._wd, self._ht
        di.set_desired_size(width, height)
        di.enable_autozoom('override')
        di.enable_autocuts('off')
        di.set_zoom_algorithm('rate')
        di.set_zoomrate(1.6)
        settings = di.get_settings()
        settings.get_setting('zoomlevel').add_callback('set', self.zoomset, di)

        cmname = self.settings.get('pick_cmap_name', None)
        if cmname is not None:
            di.set_color_map(cmname)
        else:
            di.set_cmap(cm)
        imname = self.settings.get('pick_imap_name', None)
        if imname is not None:
            di.set_intensity_map(imname)
        else:
            di.set_imap(im)
        di.set_callback('none-move', self.detailxy)
        di.set_bg(0.4, 0.4, 0.4)
        # for debugging
        di.set_name('pickimage')
        di.show_mode_indicator(True)
        self.pickimage = di

        bd = di.get_bindings()
        bd.enable_pan(True)
        bd.enable_zoom(True)
        bd.enable_cuts(True)
        bd.enable_cmap(True)

        di.set_desired_size(width, height)

        p_canvas = di.get_canvas()
        tag = p_canvas.add(self.dc.Point(width / 2, height / 2, 5,
                                         linewidth=1, color='red'))
        self.pickcenter = p_canvas.get_object_by_tag(tag)

        iw = Viewers.GingaViewerWidget(viewer=di)
        iw.resize(width, height)

        # Set up "Contour" tab viewer
        if contour.have_skimage:
            # Contour plot, Ginga-style
            ci = Viewers.CanvasView(logger=self.logger)
            width, height = 400, 300
            ci.set_desired_size(width, height)
            ci.enable_autozoom('override')
            ci.enable_autocuts('override')
            ci.set_zoom_algorithm('rate')
            ci.set_zoomrate(1.6)
            ci.set_autocut_params('histogram')

            t_ = ci.get_settings()
            if self.contour_interp not in self.contour_interp_methods:
                self.contour_interp = 'basic'
            t_.set(interpolation=self.contour_interp)

            ci.set_bg(0.4, 0.4, 0.4)
            # for debugging
            ci.set_name('contour_image')

            self.contour_canvas = self.dc.DrawingCanvas()
            ci.get_canvas().add(self.contour_canvas)

            # original colourmap 'RdYlGn_r'
            default_cmap = 'autumn_r'
            default_cmap = 'plasma'  # YlOrBr_r'
            if cmap.has_cmap(default_cmap):
                ci.set_color_map(default_cmap)
            else:
                ci.set_color_map('pastel')

            # ci.show_color_bar(True,side='bottom')
            self.contour_image = ci

            bd = ci.get_bindings()
            bd.enable_pan(True)
            bd.enable_zoom(True)
            bd.enable_cuts(True)
            bd.enable_cmap(True)

            ci.set_desired_size(width, height)
            ci.show_mode_indicator(True)

            ciw = Viewers.GingaViewerWidget(viewer=ci)
            ciw.resize(width, height)

            nb.add_widget(ciw, title="Contour")

        if have_mpl:
            if not contour.have_skimage:
                # Contour plot
                self.contour_plot = plots.ContourPlot(
                    logger=self.logger, width=width, height=height)
                self.contour_plot.cmap = 'plasma'
                self.contour_plot.add_axis(facecolor='black')
                pw = Plot.PlotWidget(self.contour_plot)
                pw.resize(width, height)
                self.contour_plot.enable(pan=True, zoom=True)

                self.contour_interp_methods = ('bilinear', 'nearest',
                                               'bicubic')
                if self.contour_interp not in self.contour_interp_methods:
                    self.contour_interp = 'nearest'
                self.contour_plot.interpolation = self.contour_interp

                nb.add_widget(pw, title="Contour")

            # FWHM gaussians plot
            self.fwhm_plot = FWHMPlot2(logger=self.logger,
                                       width=width, height=height)
            self.fwhm_plot.add_axis(facecolor='white')
            pw = Plot.PlotWidget(self.fwhm_plot)
            pw.resize(width, height)
            nb.add_widget(pw, title="FWHM")

            # Radial profile plot
            self.radial_plot = plots.RadialPlot(logger=self.logger,
                                                width=width, height=height)
            self.radial_plot.add_axis(facecolor='white')
            pw = Plot.PlotWidget(self.radial_plot)
            pw.resize(width, height)
            nb.add_widget(pw, title="Radial")

            """
            # EE profile plot
            self.ee_plot = EEPlot(logger=self.logger,
                                        width=width, height=height)
            self.ee_plot.add_axis(facecolor='white')
            pw = Plot.PlotWidget(self.ee_plot)
            pw.resize(width, height)
            nb.add_widget(pw, title="EE")
            """

        fr = Widgets.Frame()

        nb = Widgets.TabWidget(tabpos='bottom')
        self.w.nb2 = nb

        vbox1 = Widgets.VBox()

        """
        hbox = Widgets.HBox()
        hbox.set_spacing(4)
        hbox.add_widget(Widgets.Label(),stretch=1)
        hbox.add_widget(Widgets.Label("WCS:"))
        ra = Widgets.Label("RA")
        hbox.add_widget(ra,stretch=0)
        dec = Widgets.Label("DEC")
        hbox.add_widget(dec,stretch=0)
        hbox.add_widget(Widgets.Label(),stretch=1)
        vbox1.add_widget(hbox)
        """

        # Build report panel
        captions = (('RA:', 'label', 'RA', 'llabel',
                     'FITS X:', 'label', 'Object_X', 'llabel'),
                    ('DEC:', 'label', 'DEC', 'llabel',
                     'FITS Y:', 'label', 'Object_Y', 'llabel'),
                    ('Equinox:', 'label', 'Equinox', 'llabel',
                     'Background:', 'label', 'Background', 'llabel'),
                    ('Sky Level:', 'label', 'Sky Level', 'llabel',
                     'Brightness:', 'label', 'Brightness', 'llabel'),
                    ('FWHM X:', 'label', 'FWHM X', 'llabel',
                     'FWHM Y:', 'label', 'FWHM Y', 'llabel'),
                    ('FWHM (pix):', 'label', 'FWHM', 'llabel',
                     'Ellipticity:', 'label', 'ellipticity', 'llabel'),
                    ('FWHM (COR):', 'label', 'Star Size Cor', 'llabel',
                     'FWHM (DLV):', 'label', 'Star Size Dlv', 'llabel'),
                    ('Image Quality:', 'label', 'IQ', 'llabel',
                     'Airmass:', 'label', 'airmass', 'llabel'),
                    ('Signal to Noise:', 'label', 'snr', 'llabel'),
                    ('', 'label', '', 'label'),
                    ('IMG FWHM (COR):', 'label', 'Img FWHM Cor', 'llabel',
                     'IMG FWHM (DLV):', 'label', 'Img FWHM Dlv', 'llabel'),
                    ('IMG Image Quality:', 'label', 'img IQ', 'llabel',
                     'Number of sources:', 'label', 'num sources', 'llabel')
                    )

        w, b = mywidgets.build_info(captions, orientation=orientation,
                                    align=True)
        self.w.update(b)
        self.wdetail = b

        # Prime all readouts to '     ' (currently called in display_image)
        self.clear_info()

        b.lbl_signal_to_noise.set_tooltip("Poisson signal-to-noise")
        b.snr.set_tooltip("Poisson signal-to-noise")
        b.lbl_fwhm__cor_.set_tooltip("Airmass corrected FWHM")
        b.star_size_cor.set_tooltip("Airmass corrected FWHM")
        b.lbl_fwhm__dlv_.set_tooltip("FWHM delivered at telescope")
        b.star_size_dlv.set_tooltip("FWHM delivered at telescope")
        b.lbl_image_quality.set_tooltip("Gemini image quality designation")
        b.iq.set_tooltip("Gemini image quality designation")

        vbox1.add_widget(w, stretch=0)

        hbox = Widgets.HBox()
        hbox.set_spacing(4)

        btn = Widgets.Button(text="Add to DB")
        btn.add_callback('activated', lambda w: self.add_fwhm_to_database())
        btn.set_tooltip("Add measurement to FWHM database")
        btn.set_enabled(False)
        hbox.add_widget(btn)
        hbox.add_widget(Widgets.Label(), stretch=1)
        self.add_db_btn = btn

        btn = Widgets.Button(text="Update FITS")
        btn.add_callback('activated', lambda w: self.update_fits_header())
        btn.set_tooltip("Update FITS header rawiq card")
        btn.set_enabled(False)
        hbox.add_widget(btn)
        hbox.add_widget(Widgets.Label(), stretch=1)
        self.update_hdr_btn = btn

        btn = Widgets.Button(text="Default Region")
        btn.add_callback('activated', lambda w: self.reset_region())
        btn.set_tooltip("Reset region size to default")

        btn = Widgets.Button(text="Add")
        btn.add_callback('activated', lambda w: self.add_pick_cb())
        btn.set_tooltip("Pan image to pick center")
        hbox.add_widget(btn)
        hbox.add_widget(Widgets.Label(), stretch=1)
        vbox1.add_widget(hbox)

        btn = Widgets.Button(text="Centre")
        btn.add_callback('activated', lambda w: self.pan_to_pick_cb())
        btn.set_tooltip("Pan image to pick center")
        hbox.add_widget(btn)
        vbox1.add_widget(hbox)

        # Pick field evaluation progress bar and stop button
        hbox = Widgets.HBox()
        hbox.set_spacing(4)
        hbox.set_border_width(0)
        btn = Widgets.Button("Stop")
        btn.add_callback('activated', lambda w: self.eval_intr())
        btn.set_enabled(False)
        self.w.btn_intr_eval = btn
        hbox.add_widget(btn, stretch=0)

        self.w.eval_pgs = Widgets.ProgressBar()

        hbox.add_widget(self.w.eval_pgs, stretch=0)
        vbox1.add_widget(hbox, stretch=0)

        nb.add_widget(vbox1, title="Readout")

        # Build settings panel
        captions = (('Show Candidates', 'checkbutton'),
                    ("Draw type:", 'label', 'xlbl_drawtype', 'label',
                     "Draw type", 'combobox'),
                    ('Radius:', 'label', 'xlbl_radius', 'label',
                     'Radius', 'spinbutton'),
                    ('Threshold:', 'label', 'xlbl_threshold', 'label',
                     'Threshold', 'entry'),
                    ('Min FWHM:', 'label', 'xlbl_min_fwhm', 'label',
                     'Min FWHM', 'spinfloat'),
                    ('Max FWHM:', 'label', 'xlbl_max_fwhm', 'label',
                     'Max FWHM', 'spinfloat'),
                    ('Ellipticity:', 'label', 'xlbl_ellipticity', 'label',
                     'Ellipticity', 'entry'),
                    ('Edge:', 'label', 'xlbl_edge', 'label',
                     'Edge', 'entry'),
                    ('Max side:', 'label', 'xlbl_max_side', 'label',
                     'Max side', 'spinbutton'),
                    ('Coordinate Base:', 'label',
                     'xlbl_coordinate_base', 'label',
                     'Coordinate Base', 'entry'),
                    ("Calc center:", 'label', 'xlbl_calccenter', 'label',
                     "Calc center", 'combobox'),
                    ("FWHM fitting:", 'label', 'xlbl_fwhmfitting', 'label',
                     "FWHM fitting", 'combobox'),
                    ('Contour Interpolation:', 'label',
                     'xlbl_cinterp', 'label',
                     'Contour Interpolation', 'combobox'),
                    ('EE total radius:', 'label', 'xlbl_ee_total_radius',
                     'label',
                     'EE total radius', 'spinfloat'),
                    ('EE sampling radius:', 'label', 'xlbl_ee_radius', 'label',
                     'EE sampling radius', 'spinfloat')
                    )

        w, b = Widgets.build_info(captions, orientation=orientation)
        self.w.update(b)
        b.radius.set_tooltip("Radius for peak detection")
        b.threshold.set_tooltip("Threshold for peak detection (blank=default)")
        b.min_fwhm.set_tooltip("Minimum FWHM for selection")
        b.max_fwhm.set_tooltip("Maximum FWHM for selection")
        b.ellipticity.set_tooltip("Minimum ellipticity for selection")
        b.edge.set_tooltip("Minimum edge distance for selection")
        b.show_candidates.set_tooltip("Show all peak candidates")
        b.max_side.set_tooltip("Maximum dimension to search for peaks")
        b.coordinate_base.set_tooltip("Base of pixel coordinate system")
        b.calc_center.set_tooltip("How to calculate the center of object")
        b.fwhm_fitting.set_tooltip("Function for fitting the FWHM")
        b.contour_interpolation.set_tooltip("Interpolation for use in "
                                            "contour plot")
        b.ee_total_radius.set_tooltip("Radius where EE fraction is 1")
        b.ee_sampling_radius.set_tooltip("Radius for EE sampling")

        def chg_pickshape(w, idx):
            pickshape = self.drawtypes[idx]
            self.set_drawtype(pickshape)
            return True

        combobox = b.draw_type
        for name in self.drawtypes:
            combobox.append_text(name)
        index = self.drawtypes.index(self.pickshape)
        combobox.set_index(index)
        combobox.add_callback('activated', chg_pickshape)
        b.xlbl_drawtype.set_text(self.pickshape)

        # radius control
        b.radius.set_limits(5, 200, incr_value=1)

        def chg_radius(w, val):
            self.radius = int(val)
            self.w.xlbl_radius.set_text(str(self.radius))
            if self.pick_obj is not None:
                shape = self.pick_obj.objects[0]
                shape.radius = self.radius
                self.canvas[self.active].update_canvas()
                self.redo_manual()
            return True
        b.xlbl_radius.set_text(str(self.radius))
        b.radius.add_callback('value-changed', chg_radius)

        # EE total radius control
        b.ee_total_radius.set_limits(0.1, 200.0, incr_value=0.1)
        b.ee_total_radius.set_value(self.ee_total_radius)

        def chg_ee_total_radius(w, val):
            self.ee_total_radius = float(val)
            radius_text = f"{self.ee_total_radius:0.1f}"
            self.w.xlbl_ee_total_radius.set_text(radius_text)
            return True
        b.xlbl_ee_total_radius.set_text(f"{self.ee_total_radius:0.1f}")
        b.ee_total_radius.add_callback('value-changed', chg_ee_total_radius)

        # EE sampling radius control
        b.ee_sampling_radius.set_limits(0.1, 200.0, incr_value=0.1)
        b.ee_sampling_radius.set_value(self.ee_sampling_radius)

        def chg_ee_sampling_radius(w, val):
            self.ee_sampling_radius = float(val)
            self.w.xlbl_ee_radius.set_text(f"{self.ee_sampling_radius:0.1f}")
            return True
        b.xlbl_ee_radius.set_text(f"{self.ee_sampling_radius:0.1f}")
        b.ee_sampling_radius.add_callback('value-changed',
                                          chg_ee_sampling_radius)

        # threshold control
        def chg_threshold(w):
            threshold = None
            ths = w.get_text().strip()
            if len(ths) > 0:
                threshold = float(ths)
            self.threshold = threshold
            self.w.xlbl_threshold.set_text(str(self.threshold))
            return True

        b.xlbl_threshold.set_text(str(self.threshold))
        b.threshold.add_callback('activated', chg_threshold)

        # min fwhm
        b.min_fwhm.set_limits(0.1, 200.0, incr_value=0.1)
        b.min_fwhm.set_value(self.min_fwhm)

        def chg_min(w, val):
            self.min_fwhm = float(val)
            self.w.xlbl_min_fwhm.set_text(f"{self.min_fwhm:0.02f}")
            return True

        b.xlbl_min_fwhm.set_text(f"{self.min_fwhm:0.02f}")
        b.min_fwhm.add_callback('value-changed', chg_min)

        # max fwhm
        b.max_fwhm.set_limits(0.1, 200.0, incr_value=0.1)
        b.max_fwhm.set_value(self.max_fwhm)

        def chg_max(w, val):
            self.max_fwhm = float(val)
            self.w.xlbl_max_fwhm.set_text(f"{self.max_fwhm:0.02f}")
            return True

        b.xlbl_max_fwhm.set_text(f"{self.max_fwhm:0.02f}")
        b.max_fwhm.add_callback('value-changed', chg_max)

        # Ellipticity control
        def chg_ellipticity(w):
            minellipse = None
            val = w.get_text().strip()
            if len(val) > 0:
                minellipse = float(val)
            self.min_ellipse = minellipse
            self.w.xlbl_ellipticity.set_text(str(self.min_ellipse))
            return True

        b.xlbl_ellipticity.set_text(str(self.min_ellipse))
        b.ellipticity.add_callback('activated', chg_ellipticity)

        # Edge control
        def chg_edgew(w):
            edgew = None
            val = w.get_text().strip()
            if len(val) > 0:
                edgew = float(val)
            self.edgew = edgew
            self.w.xlbl_edge.set_text(str(self.edgew))
            return True

        b.xlbl_edge.set_text(str(self.edgew))
        b.edge.add_callback('activated', chg_edgew)

        b.max_side.set_limits(5, 10000, incr_value=10)
        b.max_side.set_value(self.max_side)

        def chg_max_side(w, val):
            self.max_side = int(val)
            self.w.xlbl_max_side.set_text(str(self.max_side))
            return True

        b.xlbl_max_side.set_text(str(self.max_side))
        b.max_side.add_callback('value-changed', chg_max_side)

        combobox = b.contour_interpolation

        def chg_contour_interp(w, idx):
            self.contour_interp = self.contour_interp_methods[idx]
            self.w.xlbl_cinterp.set_text(self.contour_interp)
            if self.contour_image is not None:
                t_ = self.contour_image.get_settings()
                t_.set(interpolation=self.contour_interp)
            elif self.contour_plot is not None:
                self.contour_plot.interpolation = self.contour_interp
            return True

        for name in self.contour_interp_methods:
            combobox.append_text(name)
        index = self.contour_interp_methods.index(self.contour_interp)
        combobox.set_index(index)
        self.w.xlbl_cinterp.set_text(self.contour_interp)
        if self.contour_plot is not None:
            self.contour_plot.interpolation = self.contour_interp
        combobox.add_callback('activated', chg_contour_interp)

        b.show_candidates.set_state(self.show_candidates)
        b.show_candidates.add_callback('activated', self.show_candidates_cb)
        self.w.xlbl_coordinate_base.set_text(str(self.pixel_coords_offset))
        b.coordinate_base.set_text(str(self.pixel_coords_offset))
        b.coordinate_base.add_callback('activated', self.coordinate_base_cb)

        def chg_calccenter(w, idx):
            self.center_alg = self.center_algs[idx]
            self.w.xlbl_calccenter.set_text(self.center_alg)
            return True

        combobox = b.calc_center
        for name in self.center_algs:
            combobox.append_text(name)
        index = self.center_algs.index(self.center_alg)
        combobox.set_index(index)
        combobox.add_callback('activated', chg_calccenter)
        b.xlbl_calccenter.set_text(self.center_alg)

        def chg_fwhmfitting(w, idx):
            self.fwhm_alg = self.fwhm_algs[idx]
            self.w.xlbl_fwhmfitting.set_text(self.fwhm_alg)
            return True

        combobox = b.fwhm_fitting
        for name in self.fwhm_algs:
            combobox.append_text(name)
        index = self.fwhm_algs.index(self.fwhm_alg)
        combobox.set_index(index)
        combobox.add_callback('activated', chg_fwhmfitting)
        b.xlbl_fwhmfitting.set_text(self.fwhm_alg)

        sw2 = Widgets.ScrollArea()
        sw2.set_widget(w)

        vbox3 = Widgets.VBox()
        vbox3.add_widget(sw2, stretch=1)

        # Following block moved from "Readout" tab to "Settings" tab
        hbox = Widgets.HBox()
        hbox.set_spacing(4)
        btn = Widgets.CheckBox(text="Quick Mode")
        btn.set_tooltip("Turn Quick Mode on or off.\n"
                        "ON: Pick object manually ('From Peak' off)\n"
                        "or simply evaluate first peak found\n"
                        "in pick region ('From Peak' on).\n"
                        "OFF: Compare all peaks against selection\n"
                        "criteria (Settings) to avoid objects\n"
                        "and/or find 'best' peak.")
        btn.add_callback('activated', self.quick_mode_cb)
        btn.set_state(self.quick_mode)
        hbox.add_widget(btn)
        hbox.add_widget(Widgets.Label(), stretch=1)
        btn = Widgets.CheckBox(text="From Peak")
        btn.set_tooltip("In quick mode, calculate from any peak\n"
                        "found (on), or simply calculate from the\n"
                        "center of pick shape (off).")
        btn.add_callback('activated', self.from_peak_cb)
        btn.set_state(self.from_peak)
        hbox.add_widget(btn)
        hbox.add_widget(Widgets.Label(), stretch=1)
        btn = Widgets.CheckBox(text="Center on Peak")
        btn.add_callback('activated', self.center_on_pick_cb)
        btn.set_state(self.center_on_pick)
        btn.set_tooltip("When peak is found, center shape\n"
                        "on peak.")
        hbox.add_widget(btn)
        vbox3.add_widget(hbox)

        btns = Widgets.HBox()
        btn = Widgets.Button('Redo Pick')
        btn.add_callback('activated', lambda w: self.redo_manual())
        btns.add_widget(btn, stretch=0)
        btns.add_widget(Widgets.Label(''), stretch=1)

        nb.add_widget(vbox3, title="Settings")

        # Build controls panel
        vbox3 = Widgets.VBox()
        captions = (
            ('Bg cut', 'button', 'Delta bg:', 'label',
             'xlbl_delta_bg', 'label', 'Delta bg', 'entry'),
            ('Sky cut', 'button', 'Delta sky:', 'label',
             'xlbl_delta_sky', 'label', 'Delta sky', 'entry'),
            ('Bright cut', 'button', 'Delta bright:', 'label',
             'xlbl_delta_bright', 'label', 'Delta bright', 'entry'),
        )

        w, b = Widgets.build_info(captions, orientation=orientation)
        self.w.update(b)
        b.bg_cut.set_tooltip("Set image low cut to Background Level")
        b.delta_bg.set_tooltip("Delta to apply to this cut")
        b.sky_cut.set_tooltip("Set image low cut to Sky Level")
        b.delta_sky.set_tooltip("Delta to apply to this cut")
        b.bright_cut.set_tooltip("Set image high cut to Sky Level+Brightness")
        b.delta_bright.set_tooltip("Delta to apply to this cut")

        b.bg_cut.set_enabled(False)
        self.w.btn_bg_cut = b.bg_cut
        self.w.btn_bg_cut.add_callback('activated', lambda w: self.bg_cut())
        self.w.bg_cut_delta = b.delta_bg
        b.xlbl_delta_bg.set_text(str(self.delta_bg))
        b.delta_bg.set_text(str(self.delta_bg))

        def chg_delta_bg(w):
            delta_bg = 0.0
            val = w.get_text().strip()
            if len(val) > 0:
                delta_bg = float(val)
            self.delta_bg = delta_bg
            self.w.xlbl_delta_bg.set_text(str(self.delta_bg))
            return True

        b.delta_bg.add_callback('activated', chg_delta_bg)

        b.sky_cut.set_enabled(False)
        self.w.btn_sky_cut = b.sky_cut
        self.w.btn_sky_cut.add_callback('activated', lambda w: self.sky_cut())
        self.w.sky_cut_delta = b.delta_sky
        b.xlbl_delta_sky.set_text(str(self.delta_sky))
        b.delta_sky.set_text(str(self.delta_sky))

        def chg_delta_sky(w):
            delta_sky = 0.0
            val = w.get_text().strip()
            if len(val) > 0:
                delta_sky = float(val)
            self.delta_sky = delta_sky
            self.w.xlbl_delta_sky.set_text(str(self.delta_sky))
            return True

        b.delta_sky.add_callback('activated', chg_delta_sky)

        b.bright_cut.set_enabled(False)
        self.w.btn_bright_cut = b.bright_cut
        self.w.btn_bright_cut.add_callback('activated',
                                           lambda w: self.bright_cut())
        self.w.bright_cut_delta = b.delta_bright
        b.xlbl_delta_bright.set_text(str(self.delta_bright))
        b.delta_bright.set_text(str(self.delta_bright))

        def chg_delta_bright(w):
            delta_bright = 0.0
            val = w.get_text().strip()
            if len(val) > 0:
                delta_bright = float(val)
            self.delta_bright = delta_bright
            self.w.xlbl_delta_bright.set_text(str(self.delta_bright))
            return True

        b.delta_bright.add_callback('activated', chg_delta_bright)

        vbox3.add_widget(w, stretch=0)
        vbox3.add_widget(Widgets.Label(''), stretch=1)
        nb.add_widget(vbox3, title="Display Cut")  # formerly 'control'

        vbox3 = Widgets.VBox()
        tv = Widgets.TreeView(sortable=True, use_alt_row_color=True)
        self.rpt_tbl = tv
        vbox3.add_widget(tv, stretch=1)

        tv.setup_table(self.rpt_columns, 1, 'time_local')

        btns = Widgets.HBox()
        btns.set_spacing(4)
        btn = Widgets.Button("Add Pick")
        btn.add_callback('activated', lambda w: self.add_pick_cb())
        btns.add_widget(btn)
        btn = Widgets.CheckBox("Auto record stars")
        btn.set_state(self.do_record)
        btn.add_callback('activated', self.record_cb)
        btns.add_widget(btn)
        btn = Widgets.Button("Clear Log")
        btn.add_callback('activated', lambda w: self.clear_pick_log_cb())
        btns.add_widget(btn)
        vbox3.add_widget(btns, stretch=0)

        btns = Widgets.HBox()
        btns.set_spacing(4)
        btn = Widgets.Button("Save table")
        btn.add_callback('activated', self.write_pick_log_cb)
        btns.add_widget(btn)
        btns.add_widget(Widgets.Label("File:"))
        ent = Widgets.TextEntry()
        report_log = self.settings.get('report_log_path', None)
        if report_log is None:
            report_log = "pick_log.fits"
        ent.set_text(report_log)
        ent.set_tooltip('File type determined by extension')
        self.w.report_log = ent
        btns.add_widget(ent, stretch=1)
        vbox3.add_widget(btns, stretch=0)

        nb.add_widget(vbox3, title="Report")

        fr.set_widget(nb)

        box.add_widget(fr, stretch=5)

        paned.add_widget(box)
        paned.set_sizes(self._split_sizes)

        vtop.add_widget(paned, stretch=5)

        """
        mode = self.canvas[self.active].get_draw_mode()
        hbox = Widgets.HBox()
        btn1 = Widgets.RadioButton("Move")
        btn1.set_state(mode == 'move-examine')
        btn1.add_callback(
            'activated', lambda w, val: self.set_mode_cb('move-examine', val))
        btn1.set_tooltip("Choose this to position pick")
        self.w.btn_move = btn1
        hbox.add_widget(btn1)

        btn2 = Widgets.RadioButton("Draw", group=btn1)
        btn2.set_state(mode == 'draw')
        btn2.add_callback(
            'activated', lambda w, val: self.set_mode_cb('draw', val))
        btn2.set_tooltip("Choose this to draw a replacement pick")
        self.w.btn_draw = btn2
        hbox.add_widget(btn2)

        btn3 = Widgets.RadioButton("Edit", group=btn1)
        btn3.set_state(mode == 'edit')
        btn3.add_callback(
            'activated', lambda w, val: self.set_mode_cb('edit', val))
        btn3.set_tooltip("Choose this to edit or move a pick")
        self.w.btn_edit = btn3
        hbox.add_widget(btn3)
        """
        """
        hbox.add_widget(Widgets.Label(''), stretch=1)
        vtop.add_widget(hbox, stretch=0)

        btns = Widgets.HBox()
        btns.set_spacing(4)

        vtop.add_widget(btns, stretch=0)
        """
        self.gui_up = True

    def record_cb(self, w, tf):
        self.do_record = tf
        return True

    def update_status(self, text):
        self.logger.warning(f"update_status: set_status = {self.set_status}, "
                            f"text = {text}")
        self.fv.nongui_do(self.set_status, text)
        self.logger.warning("update_status: finish")

    def init_progress(self):
        self.w.btn_intr_eval.set_enabled(True)
        self.w.eval_pgs.set_value(0.0)

    def update_progress(self, pct):
        self.w.eval_pgs.set_value(pct)

    def show_candidates_cb(self, w, state):
        self.show_candidates = state
        if not self.show_candidates:
            # Delete previous peak marks
            objs = self.canvas[self.active].get_objects_by_tag_pfx('peak')
            self.canvas[self.active].delete_objects(objs)

    def coordinate_base_cb(self, w):
        self.pixel_coords_offset = float(w.get_text())
        self.w.xlbl_coordinate_base.set_text(str(self.pixel_coords_offset))

    def bump_serial(self):
        with self.lock:
            self.serialnum += 1
            return self.serialnum

    def get_serial(self):
        with self.lock:
            return self.serialnum

    def plot_contours(self, image):
        # Make a contour plot

        ht, wd = self.pick_data.shape
        x, y = self.pick_x1 + wd // 2, self.pick_y1 + ht // 2

        # If size of pick region is too small/large, recut out a subset
        # around the picked object coordinates for plotting contours
        recut = False
        if wd < self.contour_size_min or wd > self.contour_size_max:
            wd = max(self.contour_size_min, min(wd, self.contour_size_max))
            recut = True
        if ht < self.contour_size_min or ht > self.contour_size_max:
            ht = max(self.contour_size_min, min(ht, self.contour_size_max))
            recut = True

        if recut:
            radius = max(wd, ht) // 2

            if self.pick_qs is not None:
                x, y = self.pick_qs.x, self.pick_qs.y
            data, x1, y1, x2, y2 = image.cutout_radius(x, y, radius)
            x, y = x - x1, y - y1
            ht, wd = data.shape

        else:
            data = self.pick_data
            x, y = self.pickcenter.x, self.pickcenter.y

        try:
            if self.contour_image is not None:
                cv = self.contour_image
                with cv.suppress_redraw:
                    cv.set_data(data)
                    # copy orientation of main image, so that contour will
                    # make sense.  Don't do rotation, for now.
                    flips = self.fitsimage[self.active].get_transforms()
                    cv.transform(*flips)

                    cv.panset_xy(x, y)

                    canvas = self.contour_canvas
                    try:
                        canvas.delete_object_by_tag('_$cntr', redraw=False)
                    except KeyError:
                        pass

                    # calculate contour polygons
                    contour_grps = contour.calc_contours(data,
                                                         self.num_contours)

                    # get compound polygons object
                    c_obj = contour.create_contours_obj(canvas, contour_grps,
                                                        colors=['black'],
                                                        linewidth=2)
                    canvas.add(c_obj, tag='_$cntr')

            elif self.contour_plot is not None:
                self.contour_plot.plot_contours_data(
                    x, y, data, num_contours=self.num_contours)

        except Exception as e:
            self.logger.error("Error making contour plot: %s" % (
                str(e)))

    def clear_all(self):

        # Called when the image changes or target is not found
        self.clear_contours()
        self.clear_fwhm()
        self.clear_info()
        self.clear_radial()
        self.clear_pixfwhm()

        # reset the IQ add buttons
        self.add_db_btn.set_enabled(False)
        self.update_hdr_btn.set_enabled(False)

        # if there is a db, close it.
        if self.db is not None:
            self.db.close()

    def clear_almost_all(self):
        # Called target is not found
        self.clear_fwhm()
        self.clear_info_2()
        self.clear_radial()

    def clear_pixfwhm(self):

        # clear any canvas tags:
        canvas = self.canvas[self.active]
        for idx in range(len(self.fwhm_arr)):
            tag = f'examine_{idx+1}'
            if canvas.has_tag(tag):
                canvas.delete_object_by_tag(tag)

        self.fwhm_raw = np.NaN
        self.fwhm_arr = []

    def clear_info(self):
        # Clear all readouts to '     '
        for key in self.wdetail.keys():
            if 'lbl' not in key:
                self.wdetail[key].set_text('     ')

    def clear_info_2(self):
        # Clear almost all readouts -- leave the image averages

        keys = ['ra', 'object_x', 'dec', 'object_y', 'equinox', 'background',
                'sky_level', 'brightness', 'fwhm_x', 'fwhm_y', 'fwhm',
                'ellipticity', 'star_size_cor', 'star_size_dlv', 'iq',
                'airmass', 'snr']
        for key in keys:
            self.wdetail[key].set_text('     ')

    def clear_contours(self):
        if self.contour_image is not None:
            self.contour_canvas.delete_all_objects()
        elif self.contour_plot is not None:
            self.contour_plot.clear()

    def plot_fwhm(self, qs, image):
        # Make a FWHM plot
        x, y, radius = qs.x, qs.y, qs.fwhm_radius

        try:
            self.fwhm_plot.plot_fwhm(x, y, radius, image,
                                     cutout_data=self.pick_data,
                                     iqcalc=self.iqcalc,
                                     fwhm_method=self.fwhm_alg)

            # self.fwhm_plot.ax.set_facecolor('#E0E0E0')
            # self.fwhm_plot.fig.set_facecolor('#E0E0E0')

        except Exception as e:
            self.logger.error("Error making fwhm plot: %s" % (
                str(e)))

    def clear_fwhm(self):
        self.fwhm_plot.clear()

    def plot_radial(self, qs, image):
        # Make a radial plot
        x, y, radius = qs.x, qs.y, qs.fwhm_radius

        try:
            self.radial_plot.plot_radial(x, y, radius, image)

        except Exception as e:
            self.logger.error("Error making radial plot: %s" % (
                str(e)))

    def clear_radial(self):
        self.radial_plot.clear()

    """
    def plot_ee(self, qs):

        if qs.encircled_energy_fn is None and qs.ensquared_energy_fn is None:
            self.logger.warning("Could not calculate encircled nor "
                                "ensquared energy for this source")
            self.clear_ee()
            return

        # Make a EE plot
        self.ee_plot.plot_ee(
                encircled_energy_function=qs.encircled_energy_fn,
                ensquared_energy_function=qs.ensquared_energy_fn,
                sampling_radius=self.ee_sampling_radius,
                total_radius=self.ee_total_radius)


    def clear_ee(self):
        self.ee_plot.clear()
    """

    def close(self):
        self.fv.stop_local_plugin(self.chname, str(self))
        return True

    def start(self):
        # insert layer if it is not already
        p_canvas = self.fitsimage[self.active].get_canvas()
        try:
            p_canvas.get_object_by_tag(self.layertag)

        except KeyError:
            # Add canvas layer
            p_canvas.add(self.canvas[self.active], tag=self.layertag)

        self.resume()

    def pause(self):
        self.canvas[self.active].ui_set_active(False)

    def resume(self):
        # turn off any mode user may be in
        self.modes_off()

        viewer = self.fitsimage[self.active]
        self.canvas[self.active].ui_set_active(True, viewer=viewer)
        self.fv.show_status("Draw a shape with the right mouse button")

    def stop(self):
        self.gui_up = False
        self._split_sizes = self.w.splitter.get_sizes()
        # Delete previous peak marks

        for canvas in self.canvas:
            if canvas is None:
                continue

            objs = canvas.get_objects_by_tag_pfx('peak')
            canvas.delete_objects(objs)

            # deactivate the canvas
            canvas.ui_set_active(False)
            p_canvas = self.fitsimage[self.active].get_canvas()
            try:
                p_canvas.delete_object_by_tag(self.layertag)
            except Exception:
                pass
        self.fv.show_status("")

    def redo_manual(self):
        if self.quick_mode:
            self.redo_quick()
            self.calc_quick()
        else:
            serialnum = self.bump_serial()
            self.ev_intr.set()
            self._redo(serialnum)

    def redo(self):
        serialnum = self.bump_serial()
        self._redo(serialnum)

    def _redo(self, serialnum):
        if self.pick_obj is None:
            return

        pickobj = self.pick_obj
        if pickobj.kind != 'compound':
            return True
        shape = pickobj.objects[0]
        point = pickobj.objects[1]
        text = pickobj.objects[2]

        # reposition other elements to match
        ctr_x, ctr_y = shape.get_center_pt()
        point.x, point.y = ctr_x, ctr_y
        x1, y1, x2, y2 = shape.get_llur()
        text.x, text.y = x1, y2

        try:
            image = self.fitsimage[self.active].get_vip()

            # sanity check on size of region
            width, height = abs(x2 - x1), abs(y2 - y1)

            if (width > self.max_side) or (height > self.max_side):
                errmsg = "Image area (%dx%d) too large!" % (
                    width, height)
                self.fv.show_error(errmsg)
                raise Exception(errmsg)

            # Extract image in pick window
            self.logger.debug("bbox %f,%f %f,%f" % (x1, y1, x2, y2))
            x1, y1, x2, y2, data = self.cutdetail(image, shape)
            self.logger.debug("cut box %d,%d %d,%d" % (x1, y1, x2, y2))

            # calculate center of pick image
            ht, wd = data.shape[:2]
            xc = wd // 2
            yc = ht // 2
            self.pick_x1, self.pick_y1 = x1, y1
            self.pick_data = data

            point.color = 'red'
            text.text = '{0}: calc'.format(self._textlabel)
            self.pickcenter.x = xc
            self.pickcenter.y = yc
            self.pickcenter.color = 'red'

            # Offload this task to another thread so that GUI remains
            # responsive

            self.fv.nongui_do(self.search, serialnum, data,
                              x1, y1, wd, ht, pickobj)

        except Exception as e:
            self.logger.error("Error calculating quality metrics: %s" % (
                str(e)))
            return True

    def search(self, serialnum, data, x1, y1, wd, ht, pickobj):

        self.x, self.y = x1, y1

        # Plot countours regardless of whether we can fit it
        print('seq_examine.search: plotting contours...')
        self.clear_contours()
        vip_img = self.fitsimage[self.active].get_vip()
        self.plot_contours(vip_img)
        print('seq_examine.search: contours plotted.')

        # For display use if search fails

        # nanmedian doesn't play well with masked data, use ma.median instead
        self.median_background = np.ma.median(data)

        with self.lock2:
            if serialnum != self.get_serial():
                return

            self.pgs_cnt = 0

            self.ev_intr.clear()
            self.fv.gui_call(self.init_progress)
            msg, results, qs = None, None, None
            # Find bright peaks in the cutout
            self.update_status("Finding bright peaks...")
            self.logger.warning(f"find_bright_peaks: data={data}, "
                                f"threshold = {self.threshold}, "
                                f"radius = {self.radius}")
            peaks = self.iqcalc.find_bright_peaks(data,
                                                  threshold=self.threshold,
                                                  radius=self.radius)
            self.logger.warning(f"find_bright_peaks: peaks={peaks}")

            num_peaks = len(peaks)
            if num_peaks == 0:
                print('self.search: clear_all')
                self.clear_almost_all()

                msg = "Cannot find bright peaks"
                self.set_status(msg)
                raise Exception(msg)

            def cb_fn(obj):
                self.pgs_cnt += 1
                pct = float(self.pgs_cnt) / num_peaks
                self.fv.gui_do(self.update_progress, pct)

            # Evaluate those peaks
            self.update_status(f"Evaluating {num_peaks} bright peaks...")
            self.logger.warning(f"evaluate_peaks: peaks={peaks}, data={data}, "
                                f"fwhm_radius={self.radius}, cb_fn={cb_fn}, "
                                f"ev_intr={self.ev_intr}, "
                                f"fwhm_method={self.fwhm_alg}, "
                                f"ee_total_radius={self.ee_total_radius}")
            objlist = self.iqcalc.evaluate_peaks(
                peaks, data,
                fwhm_radius=self.radius,
                cb_fn=cb_fn,
                ev_intr=self.ev_intr,
                fwhm_method=self.fwhm_alg)
            self.logger.warning(f"evaluate_peaks: objlist={objlist}")

            num_candidates = len(objlist)
            if num_candidates == 0:
                msg = "Error evaluating bright peaks: no candidates found"
                self.set_status(msg)
                self.clear_almost_all()

                raise Exception(msg)

            self.update_status("Selecting from %d candidates..." % (
                num_candidates))
            height, width = data.shape

            self.logger.warning("objlist_select: start")
            results = self.iqcalc.objlist_select(objlist, width, height,
                                                 minfwhm=self.min_fwhm,
                                                 maxfwhm=self.max_fwhm,
                                                 minelipse=self.min_ellipse,
                                                 edgew=self.edgew)
            self.logger.warning(f"objlist_select: results={results}")

            if len(results) == 0:
                msg = "All objects rejected: none match selection criteria"
                self.set_status(msg)
                self.clear_almost_all()
                # Plot countours regardless of whether we can fit it
                print("trying to plot contours...")

                raise Exception(msg)

            # Add back in offsets into image to get correct values with
            # respect to the entire image
            for qs in results:
                qs.x += x1
                qs.y += y1
                if qs.objx is not None:
                    qs.objx += x1
                    qs.objy += y1
                if qs.oid_x is not None:
                    qs.oid_x += x1
                    qs.oid_y += y1

            # pick main result
            self.logger.info("Calculated peak characteristics")
            qs = results[0]

            # If we have a good fit, pass it to pix2iq3
            if HAVE_PIX:
                hdrs = self.currentFrame[self.active].headers
                fitspath = self.currentFrame[self.active].fitspath
                results = self.pix.calculateIQ(hdrs, qs.fwhm, fitspath)
                self.IQ, self.fwhmDlv, self.fwhmCor, self.db_action = results
            else:
                self.IQ = "UNKNOWN"
                self.fwhmCor, self.fwhmDlv = np.NaN, np.NaN

            # save examine's fwhm in case we want to add it to the list
            self.fwhm_raw = qs.fwhm

            """
            except Exception as e:

                self.add_db_btn.set_enabled(False)
                self.update_hdr_btn.set_enabled(False)
                msg = str(e)
                print(f"search: There was a fault: msg = {msg}")
                self.update_status(msg)
            """

            self.fv.gui_do(self.update_pick, serialnum, objlist, qs, x1, y1,
                           wd, ht, data, pickobj, msg)

    def _make_report(self, vip_img, qs):
        d = Bunch.Bunch()
        try:
            x, y = qs.objx, qs.objy
            if (qs.oid_x is not None) and (self.center_alg == 'centroid'):
                # user wants RA/DEC calculated by centroid instead of fwhm
                x, y = qs.oid_x, qs.oid_y

            image, pt2 = vip_img.get_image_at_pt((x, y))
            equinox = float(image.get_keyword('EQUINOX', 2000.0))
            try:
                ra_deg, dec_deg = image.pixtoradec(x, y, coords='data')
                ra_txt, dec_txt = wcs.deg2fmt(ra_deg, dec_deg, 'str')

            except Exception as e:
                self.logger.warning(
                    "Couldn't calculate sky coordinates: %s" % (str(e)))
                ra_deg, dec_deg = 0.0, 0.0
                ra_txt = dec_txt = 'BAD WCS'

            # Calculate star size from pixel pitch
            try:
                header = image.get_header()
                ((xrot, yrot),
                 (cdelt1, cdelt2)) = wcs.get_xy_rotation_and_scale(header)

                starsize = self.iqcalc.starsize(qs.fwhm_x, cdelt1,
                                                qs.fwhm_y, cdelt2)
            except Exception as e:
                self.logger.warning(
                    "Couldn't calculate star size: %s" % (str(e)))
                starsize = 0.0

            rpt_x = x + self.pixel_coords_offset
            rpt_y = y + self.pixel_coords_offset

            """
            # EE at sampling radius
            ee_circ, ee_sq = 0, 0
            try:
                if qs.encircled_energy_fn is not None:
                    ee_circ = qs.encircled_energy_fn(self.ee_sampling_radius)
                if qs.ensquared_energy_fn is not None:
                    ee_sq = qs.ensquared_energy_fn(self.ee_sampling_radius)
            except Exception as e:
                self.logger.warning("Couldn't calculate EE at %.2f: %s" %
                                    (self.ee_sampling_radius, str(e)))
            """

            # make a report in the form of a dictionary
            d.setvals(x=rpt_x, y=rpt_y,
                      ra_deg=ra_deg, dec_deg=dec_deg,
                      ra_txt=ra_txt, dec_txt=dec_txt,
                      equinox=equinox,
                      fwhm=qs.fwhm,
                      fwhm_x=qs.fwhm_x, fwhm_y=qs.fwhm_y,
                      ellipse=qs.elipse, background=qs.background,
                      skylevel=qs.skylevel, brightness=qs.brightness,
                      starsize=starsize,
                      time_local=time.strftime("%Y-%m-%d %H:%M:%S",
                                               time.localtime()),
                      time_ut=time.strftime("%Y-%m-%d %H:%M:%S",
                                            time.gmtime()),
                      )
        except Exception as e:
            self.logger.error("Error making report: %s" % (str(e)))

        return d

    def update_pick(self, serialnum, objlist, qs, x1, y1, wd, ht, data,
                    pickobj, msg):

        try:
            # set the pick image to have the same cut levels and transforms
            self.fitsimage[self.active].copy_attributes(self.pickimage,
                                                        self.copy_attrs)
            self.pickimage.set_data(data)

            # Delete previous peak marks
            objs = self.canvas[self.active].get_objects_by_tag_pfx('peak')
            self.canvas[self.active].delete_objects(objs)

            vip_img = self.fitsimage[self.active].get_vip()
            shape_obj = pickobj.objects[0]
            point = pickobj.objects[1]
            text = pickobj.objects[2]
            text.text = self._textlabel
            _x1, _y1, x2, y2 = shape_obj.get_llur()
            text.x, text.y = x1, y2

            if msg is not None:
                print(f'update_pick: msg is not None: msg = {msg}')
                raise Exception(msg)

            # Mark new peaks, if desired
            if self.show_candidates:
                reports = [self._make_report(vip_img, x) for x in objlist]
                for obj in objlist:
                    self.canvas[self.active].add(self.dc.Point(
                        obj.objx, obj.objy, 50, linewidth=1,
                        color=self.candidate_color), tagpfx='peak')
            else:
                reports = [self._make_report(vip_img, qs)]

            # Calculate X/Y of center of star
            obj_x = qs.objx
            obj_y = qs.objy
            fwhm = qs.fwhm
            fwhm_x, fwhm_y = qs.fwhm_x, qs.fwhm_y
            point.x, point.y = obj_x, obj_y
            text.color = COLOR_A

            if self.center_on_pick:
                shape_obj.move_to_pt((obj_x, obj_y))
                # reposition label above moved shape
                _x1, _y1, x2, y2 = shape_obj.get_llur()
                text.x, text.y = _x1, y2

            # Make report
            self.last_rpt = reports
            if self.do_record:
                self.add_reports(reports)
                self.last_rpt = []

            d = reports[0]

            self.wdetail.fwhm_x.set_text('%.3f' % fwhm_x)
            self.wdetail.fwhm_y.set_text('%.3f' % fwhm_y)
            self.wdetail.fwhm.set_text('%.3f' % fwhm)
            self.wdetail.ellipticity.set_text(f'{d.ellipse:0.3f}')
            self.wdetail.object_x.set_text('%.3f' % d.x)
            self.wdetail.object_y.set_text('%.3f' % d.y)
            self.wdetail.sky_level.set_text('%.1f' % qs.skylevel)
            self.wdetail.background.set_text('%.1f' % qs.background)
            self.wdetail.brightness.set_text('%.1f' % qs.brightness)

            ra, dec = d.ra_txt[:-1], d.dec_txt[:-1]
            self.wdetail.ra.set_text(ra)
            self.wdetail.dec.set_text(dec)
            self.set_status(f'Object found at {ra} {dec}')

            self.wdetail.equinox.set_text(str(d.equinox))

            self.wdetail.star_size_cor.set_text(val2arcstring(self.fwhmCor))
            self.wdetail.star_size_dlv.set_text(val2arcstring(self.fwhmDlv))
            self.wdetail.iq.set_text(self.IQ)

            # Display Poisson SNR
            self.wdetail.snr.set_text(f"{np.sqrt(qs.brightness):0.1f}")

            self.w.btn_bg_cut.set_enabled(True)
            self.w.btn_sky_cut.set_enabled(True)
            self.w.btn_bright_cut.set_enabled(True)

            # Mark center of object on pick image
            i1 = obj_x - x1
            j1 = obj_y - y1
            self.pickcenter.x = i1
            self.pickcenter.y = j1
            self.pickcenter.color = COLOR_A
            self.pick_qs = qs
            self.pickimage.panset_xy(i1, j1)

            # Mark object center on image
            point.color = COLOR_A
            shape_obj.linestyle = 'solid'
            shape_obj.color = self.pickcolor

            self.plot_panx = float(i1) / wd
            self.plot_pany = float(j1) / ht
            if self.have_mpl:
                self.plot_contours(vip_img)
                self.plot_fwhm(qs, vip_img)
                self.plot_radial(qs, vip_img)

        except Exception as e:
            errmsg = "Error calculating quality metrics: %s" % (
                str(e))
            self.logger.error(errmsg)
            self.fv.show_error(errmsg, raisetab=False)
            try:
                (type, value, tb) = sys.exc_info()
                tb_str = "\n".join(traceback.format_tb(tb))
            except Exception:
                tb_str = "Traceback information unavailable."
            self.logger.error(tb_str)
            for key in ('sky_level', 'background', 'brightness',
                        'fwhm', 'fwhm_x', 'fwhm_y',
                        'ra', 'dec', 'ellipticity',
                        'star_size_cor', 'star_size_dlv', 'iq',
                        'airmass', 'snr'):
                self.wdetail[key].set_text('')
            self.wdetail.object_x.set_text('%d' % self.x)
            self.wdetail.object_y.set_text('%d' % self.y)

            # Try to calculate WCS coords
            try:
                image_hdr = self.currentFrame[self.active].headers[1]
                fits_wcs = ast_wcs.WCS(header=image_hdr)
                skycoords = fits_wcs.pixel_to_world(self.x, self.y)
                ra = skycoords.ra.to_string(precision=2, unit=ast_units.hour)
                ra = re.sub('[hms]', ':', ra)[0:-1]
                dec = skycoords.dec.to_string(precision=1)
                dec = re.sub('[dms]', ':', dec)[0:-1]
                if dec[0] != '-':
                    dec = f'+{dec}'
                self.wdetail.ra.set_text(ra)
                self.wdetail.dec.set_text(dec)
            except Exception as e:
                self.logger.warning("Could not interpret WCS")
                self.logger.warning(e)

            self.wdetail.background.set_text(f'{self.median_background:.1f}')

            #    self.wdetail[key].set_text('Failed')
            self.w.btn_bg_cut.set_enabled(False)
            self.w.btn_sky_cut.set_enabled(False)
            self.w.btn_bright_cut.set_enabled(False)
            self.pick_qs = None
            text.color = 'red'
            shape_obj.linestyle = 'dash'

            self.pickimage.center_image()
            self.plot_panx = self.plot_pany = 0.5
            if self.have_mpl:
                self.plot_contours(vip_img)
                # TODO: could calc background based on numpy calc
                self.clear_fwhm()
                self.clear_radial()

        hdr0 = self.currentFrame[self.active].headers[0]
        if 'AIRMASS' in hdr0:
            am = f"{hdr0['AIRMASS']:0.3f}" if hdr0['AIRMASS'] > 1 else "--"
            self.wdetail.airmass.set_text(am)

        self.w.btn_intr_eval.set_enabled(False)
        self.pickimage.redraw(whence=3)
        self.canvas[self.active].redraw(whence=3)

        self.fv.show_status("Click left mouse button to reposition pick")
        return True

    def add_fwhm_to_database(self):
        success = self.pix.updateDB()
        if success:
            self.add_db_btn.set_enabled(False)

    def update_fits_header(self):
        # Update the fits header with the measured IQ

        # currently unthreaded, may need to add thread if this takes a while

        sa = seq_utils.ServerAccess(seq_utils.SERVERNAME, logger=self.logger)
        fitsname = os.path.split(self.currentFrame[self.active].fitspath)[-1]

        results = sa.change(fitsname, 'RAWIQ', self.IQ)

        prefix = self.currentFrame[self.active].prefix
        if not results:
            msg = f"{prefix} RAWIQ state could not be set"
        else:
            msg = f"{prefix} RAWIQ set to {self.IQ}"
        self.set_status(msg, info=True)

    def eval_intr(self):
        self.ev_intr.set()

    def redo_quick(self):
        vip_img = self.fitsimage[self.active].get_vip()

        obj = self.pick_obj
        if obj is None:
            return
        shape = obj.objects[0]

        x1, y1, x2, y2, data = self.cutdetail(vip_img, shape)
        self.pick_x1, self.pick_y1 = x1, y1
        self.pick_data = data

    def calc_quick(self):
        if self.pick_data is None:
            return

        # examine cut area
        data, x1, y1 = self.pick_data, self.pick_x1, self.pick_y1
        ht, wd = data.shape[:2]
        xc, yc = wd // 2, ht // 2
        radius = min(xc, yc)
        peaks = [(xc, yc)]

        with_peak = self.w.from_peak.get_state()
        if with_peak:
            # find the peak in the area, if possible, and calc from that
            try:
                peaks = self.iqcalc.find_bright_peaks(data,
                                                      threshold=self.threshold,
                                                      radius=radius)
            except Exception:
                self.logger.debug("no peaks found in data--using center")

            if len(peaks) > 0:
                xc, yc = peaks[0]

        self.pickcenter.x = xc
        self.pickcenter.y = yc
        self.pickcenter.color = 'red'
        msg = qs = None

        try:
            radius = int(round(radius))
            objlist = self.iqcalc.evaluate_peaks(peaks, data,
                                                 fwhm_radius=radius,
                                                 fwhm_method=self.fwhm_alg)

            num_candidates = len(objlist)
            if num_candidates == 0:
                raise Exception("Error calculating image quality")

            # Add back in offsets into image to get correct values with
            # respect to the entire image
            qs = objlist[0]
            qs.x += x1
            qs.y += y1
            if qs.objx is not None:
                qs.objx += x1
                qs.objy += y1
            if qs.oid_x is not None:
                qs.oid_x += x1
                qs.oid_y += y1

        except Exception as e:
            msg = str(e)
            self.update_status(msg)

        self.fv.gui_do(self.update_pick, 0, objlist, qs,
                       x1, y1, wd, ht, data, self.pick_obj, msg)

    def draw_cb(self, canvas, tag):
        obj = canvas.get_object_by_tag(tag)
        canvas.delete_object_by_tag(tag)

        if obj.kind not in self.drawtypes:
            return True

        self.create_pick_box(obj)

        self.redo_manual()

    def edit_cb(self, canvas, obj):
        if obj.kind not in self.drawtypes:
            return True

        if self.pick_obj is not None and self.pick_obj.has_object(obj):
            self.redo_manual()

    def reset_region(self):
        self.dx = region_default_width
        self.dy = region_default_height
        self.set_drawtype('circle')

        obj = self.pick_obj
        if obj.kind != 'compound':
            return
        shape = obj.objects[0]

        # determine center of shape
        data_x, data_y = shape.get_center_pt()

        # replace shape
        Shape = self.dc.Circle
        tag = self.canvas[self.active].add(Shape(data_x, data_y, self.dx // 2,
                                                 self.dy // 2,
                                                 color=self.pickcolor))
        self.draw_cb(self.canvas[self.active], tag)

    def set_active_cb(self, canvas, button_pressed, x_pos, y_pos):
        # set the active and inactive channels based on a button press
        if self.active != canvas.name:  # and check_canvas:
            chname = canvas.name
            self.active = chname

    def create_pick_box(self, obj):
        pick_obj = self.pick_obj
        canvas = self.canvas[self.active]
        if pick_obj is not None and canvas.has_object(pick_obj):
            canvas.delete_object(pick_obj)

        # determine center of object
        x1, y1, x2, y2 = obj.get_llur()
        x, y = obj.get_center_pt()

        obj.color = self.pickcolor
        args = [obj,
                self.dc.Point(x, y, 15, color='red', style='plus'),
                self.dc.Text(x1, y2, '{0}: calc'.format(self._textlabel),
                             color=self.pickcolor)
                ]

        self.pick_obj = self.dc.CompoundObject(*args)
        canvas.add(self.pick_obj)

    def pan_to_pick_cb(self):
        if not self.pick_qs:
            self.fv.show_status("Please pick an object to set the sky level!")
            return
        pan_x, pan_y = self.pick_qs.objx, self.pick_qs.objy

        # TODO: convert to WCS coords based on user preference
        self.fitsimage[self.active].set_pan(pan_x, pan_y, coord='data')
        return True

    def bg_cut(self):
        if not self.pick_qs:
            self.fv.show_status("Please pick an object to set the bg level!")
            return
        loval = self.pick_qs.background
        oldlo, hival = self.fitsimage[self.active].get_cut_levels()
        try:
            loval += self.delta_bg
            self.fitsimage[self.active].cut_levels(loval, hival)

        except Exception:
            self.fv.show_status("No valid bg level: '%s'" % (loval))

    def sky_cut(self):
        if not self.pick_qs:
            self.fv.show_status("Please pick an object to set the sky level!")
            return
        loval = self.pick_qs.skylevel
        oldlo, hival = self.fitsimage[self.active].get_cut_levels()
        try:
            loval += self.delta_sky
            self.fitsimage[self.active].cut_levels(loval, hival)

        except Exception:
            self.fv.show_status("No valid sky level: '%s'" % (loval))

    def bright_cut(self):
        if not self.pick_qs:
            self.fv.show_status("Please pick an object to set the brightness!")
            return
        skyval = self.pick_qs.skylevel
        hival = self.pick_qs.brightness
        loval, oldhi = self.fitsimage[self.active].get_cut_levels()
        try:
            # brightness is measured ABOVE sky level
            hival = skyval + hival + self.delta_bright
            self.fitsimage[self.active].cut_levels(loval, hival)

        except Exception:
            self.fv.show_status("No valid brightness level: '%s'" % (hival))

    def zoomset(self, setting, zoomlevel, fitsimage):
        scalefactor = fitsimage.get_scale()
        self.logger.debug("scalefactor = %.2f" % (scalefactor))

    def detailxy(self, canvas, event, data_x, data_y):
        """Motion event in the pick fits window.  Show the pointing
        information under the cursor.
        """
        chviewer = self.fv.getfocus_viewer()
        # Don't update global information if our chviewer isn't focused
        if chviewer != self.fitsimage[self.active]:
            return True

        # Add offsets from cutout
        data_x = data_x + self.pick_x1
        data_y = data_y + self.pick_y1

        return self.fv.showxy(chviewer, data_x, data_y)

    def cutdetail(self, image, shape_obj):
        view, mask = image.get_shape_view(shape_obj)
        data = image._slice(view)

        y1, y2 = view[0].start, view[0].stop
        x1, x2 = view[1].start, view[1].stop

        # mask non-containing members
        mdata = np.ma.array(data, mask=np.logical_not(mask))

        return (x1, y1, x2, y2, mdata)

    def pan_plot(self, xdelta, ydelta):
        x1, x2 = self.w.ax.get_xlim()
        y1, y2 = self.w.ax.get_ylim()

        self.w.ax.set_xlim(x1 + xdelta, x2 + xdelta)
        self.w.ax.set_ylim(y1 + ydelta, y2 + ydelta)
        self.w.canvas.draw()

    def write_pick_log(self, filepath):
        if len(self.rpt_dict) == 0:
            return

        # Save the table as a binary table HDU
        from astropy.table import Table

        try:
            self.logger.debug("Writing modified pick log")
            tbl = Table(rows=list(self.rpt_dict.values()))
            tbl.meta['comments'] = ["Written by ginga Pick plugin"]
            if filepath.lower().endswith('.txt'):
                fmt = 'ascii.commented_header'
            else:
                fmt = None
            tbl.write(filepath, format=fmt, overwrite=True)
            self.rpt_wrt_time = time.time()

        except Exception as e:
            self.logger.error("Error writing to pick log: %s" % (str(e)))

    def write_pick_log_cb(self, w):
        path = self.w.report_log.get_text().strip()
        self.write_pick_log(path)

    def add_pix(self):

        self.fwhm_arr.append(self.fwhm_raw)

        fwhm = np.median(self.fwhm_arr)

        hdrs = self.currentFrame[self.active].headers
        fitspath = self.currentFrame[self.active].fitspath
        results = self.pix.calculateIQ(hdrs, fwhm, fitspath)
        self.IQ_img, self.fwhmDlv_img, self.fwhmCor_img, db_action = results

        num_sources = len(self.fwhm_arr)

        # remove the target and create a 'new' one.
        # self.pick_obj exists, and the canvas has it.
        canvas = self.canvas[self.active]
        circle = self.pick_obj.objects[0]
        cx, cy, cr = circle.x, circle.y, circle.radius

        text = self.pick_obj.objects[2]
        tx, ty = text.x, text.y

        canvas.delete_object(self.pick_obj)

        n = len(self.fwhm_arr)

        tag = f'examine_{n}'
        label = f'{n}: {val2arcstring(self.fwhmCor)}'

        col = self.pickcolor
        obj = self.dc.CompoundObject(self.dc.Circle(cx, cy, cr, color=col),
                                     self.dc.Text(tx, ty, label, color=col))
        canvas.add(obj, tag=tag)

        # update the display
        self.wdetail.img_fwhm_cor.set_text(val2arcstring(self.fwhmCor))
        self.wdetail.img_fwhm_dlv.set_text(val2arcstring(self.fwhmDlv))
        self.wdetail.img_iq.set_text(self.IQ)
        unit = 'source' if num_sources == 1 else 'sources'
        self.wdetail.num_sources.set_text(f"{num_sources} {unit}")

        HAVE_DB = True

        # if we're here, we have a good IQ value, so
        # enable the db and FITS update buttons as appropriate
        if db_action is not None:
            btn_text = f'{db_action} DB'
            btn_enabled = True
        else:
            btn_text = 'Add to DB'
            btn_enabled = False
        self.add_db_btn.widget.setText(btn_text)
        self.add_db_btn.set_enabled(HAVE_DB and btn_enabled)
        self.update_hdr_btn.set_enabled(HAVE_DB)

        self.set_status(f'Object {num_sources} added to average.')

        return

    def add_reports(self, reports):
        for rpt in reports:
            self.rpt_cnt += 1
            # Hack to insure that we get the columns in the desired order
            d = OrderedDict([(key, rpt[key])
                             for col, key in self.rpt_columns])
            self.rpt_dict[self.rpt_cnt] = d
            self.rpt_mod_time = time.time()

            self.rpt_tbl.set_tree(self.rpt_dict)

    def add_pick_cb(self):
        if len(self.last_rpt) > 0:
            self.add_reports(self.last_rpt)
            self.last_rpt = []

            # add to the pix list
            self.add_pix()

    def clear_pick_log_cb(self):
        self.rpt_dict = OrderedDict({})
        self.rpt_tbl.set_tree(self.rpt_dict)

    def set_drawtype(self, shapename):
        if shapename not in self.drawtypes:
            raise ValueError("shape must be one of %s not %s" % (
                str(self.drawtypes), shapename))

        self.pickshape = shapename
        self.w.xlbl_drawtype.set_text(self.pickshape)
        self.canvas[self.active].set_drawtype(self.pickshape, color=COLOR_A,
                                              linestyle='dash')

    def edit_select_pick(self):
        obj = self.pick_obj
        if obj is not None and self.canvas[self.active].has_object(obj):
            if obj.kind != 'compound':
                return True
            # drill down to reference shape
            bbox = obj.objects[0]
            self.canvas[self.active].edit_select(bbox)
            self.canvas[self.active].update_canvas()
            return

        self.canvas[self.active].clear_selected()
        self.canvas[self.active].update_canvas()

    def btn_down(self, canvas, event, data_x, data_y, viewer):

        if self.pick_obj is not None:
            if not canvas.has_object(self.pick_obj):
                self.canvas[self.active].add(self.pick_obj)

            obj = self.pick_obj
            shape = obj.objects[0]
            shape.color = COLOR_A
            shape.linestyle = 'dash'
            point = obj.objects[1]
            point.color = 'red'
            # Set object radius to search radius
            shape.radius = self.radius
            shape.move_to(data_x, data_y)

            self.canvas[self.active].update_canvas()

            if self.quick_mode:
                self.redo_quick()

        else:
            # No object yet? Add a default one.
            self.set_drawtype(self.pickshape)
            rd_x, rd_y = self.dx // 2, self.dy // 2
            radius = max(rd_x, rd_y)
            Shape = self.canvas[self.active].get_draw_class(self.pickshape)
            tag = self.canvas[self.active].add(Shape(data_x, data_y, radius,
                                                     color=self.pickcolor))
            self.draw_cb(self.canvas[self.active], tag)

        return True

    def btn_drag(self, canvas, event, data_x, data_y, viewer):

        if self.pick_obj is not None:
            obj = self.pick_obj
            if obj.kind != 'compound':
                return False

            shape = obj.objects[0]
            shape.move_to(data_x, data_y)
            self.canvas[self.active].update_canvas()

            if self.quick_mode:
                self.redo_quick()
            return True

        return False

    def btn_up(self, canvas, event, data_x, data_y, viewer):

        if self.pick_obj is not None:
            obj = self.pick_obj
            if obj.kind != 'compound':
                return False

            shape = obj.objects[0]
            shape.move_to(data_x, data_y)
            self.canvas[self.active].update_canvas()

            self.redo_manual()

        return True

    def set_mode_cb(self, mode, tf):
        if tf:
            self.canvas[self.active].set_draw_mode(mode)
            if mode == 'edit':
                self.edit_select_pick()
        return True

    def quick_mode_cb(self, w, tf):
        self.quick_mode = tf
        return True

    def from_peak_cb(self, w, tf):
        self.from_peak = tf
        return True

    def center_on_pick_cb(self, w, tf):
        self.center_on_pick = tf
        return True

    def drag_only_cb(self, w, tf):
        self.drag_only = tf
        return True

    def __str__(self):
        return 'pick'


def val2arcstring(val):

    if val is np.NaN:
        return "--"

    intpart = str(int(val)) if int(val) != 0 else '0'
    decpart = f"{val%1:.3f}"[2:]
    return f'{intpart}.\"{decpart}'


def print_dir(obj):
    # little coding tool
    print(f"\n\n type = {type(obj)}")
    a = dir(obj)
    for item in a:
        print(item)
    print("\n")


# Append module docstring with config doc for auto insert by Sphinx.
from ginga.util.toolbox import generate_cfg_example  # noqa
if __doc__ is not None:
    __doc__ += generate_cfg_example('plugin_Pick', package='ginga')

# END
