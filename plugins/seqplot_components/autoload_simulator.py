import os
import glob
import sys
import random
import time
from datetime import datetime
from pytz import timezone
import shutil
import re

READ_DATAPATH = "/Users/cfigura/FITS/dataflow"
WRITE_DATAPATH = "/Users/cfigura/FITS/dhs"


GMOS_FILEPATH = '/Users/cfigura/FITS/dhs/N20230406S0001.fits'


def simulate_new_images():

    filelist = glob.glob(os.path.join(READ_DATAPATH, "*.fits"))

    # Start after the most recent frame for this date
    UTCdate = datetime.now(timezone('UTC')).strftime("%Y%m%d")
    searchkey = os.path.join(WRITE_DATAPATH, f"*{UTCdate}*.fits")
    ondate = glob.glob(searchkey)
    ondate.sort()

    if ondate is not None and len(ondate) > 0:
        lastondate = ondate[-1]
        items = os.path.split(lastondate)
        prefix = items[-1].split('.')[0]
        date, frame = re.sub('[_a-zA-Z]', ' ', prefix).lstrip().split()[0:2]
        counter = int(frame) + 1
    else:
        counter = 1

    print(f"Starting with counter {counter}")

    while True:

        idx = random.randrange(len(filelist))

        filename = filelist.pop(idx)
        filename = GMOS_FILEPATH

        newfilename = f"N{UTCdate}X{counter:04d}.fits"
        newpath = os.path.join(WRITE_DATAPATH, newfilename)

        shutil.copy(filename, newpath)

        now = datetime.strftime(datetime.now(), "%H:%M:%S")
        fracsecs = datetime.strftime(datetime.now(), ".%f")
        tenthsecs = str(round(float(fracsecs), 1))[1:]
        now = f"{now}{tenthsecs}"
        print(f"{now} write {newpath}")

        counter += 1
        time.sleep(5)


if __name__ == "__main__":

    if len(sys.argv) > 1:
        exists = os.path.exists(sys.argv[1])
        if exists:
            datapath = sys.argv[1]

    simulate_new_images()
