"""
This is a collection of assorted classes/methods used in Seqplot2 plugin for
Ginga.

TBD
-----
- See C Figura's evernote page

Changelog
___________
- 2023.05.15:  initial release candidate
- 2023.08.23:  made compliant to PEP-8 standards
"""

__version__ = '20230823'
__author__ = 'cfigura'


import os
import requests
import pandas as pd

SERVERNAME = 'fits'
GEM_AUTH = os.environ.get('GEMINI_API_AUTH', '')
COOKIES = {'gemini_api_authorization': GEM_AUTH}


class ServerAccess(object):
    """
    Class that abstracts queries to the archive server
    """

    # ServerAccess allows access to fits database for setting QA & IQ states.
    # Adapted/simplified from fixHead.py

    def __init__(self, host, port='80', logger=None, set_status=None):
        self.server = '{}:{}'.format(host, port)
        self.logger = logger
        self.set_status = set_status

    def uri(self, *extra):
        return '/'.join(['http://{}'.format(self.server)] + list(extra))

    def change(self, fitsname, card, value):

        # fits db wants 'qa_state'

        if card == 'RAWIQ':
            actions = {'generic':
                       [(card, validate_raw(value, (20, 70, 85), 'RAWIQ'))]}
        elif card == 'QA':
            actions = {'qa_state': value}

        arguments = {'request': [{'filename': fitsname, 'values': actions,
                                  'reject_new': True}], 'batch': False}
        uri = self.uri('update_headers')
        try:
            ret = requests.post(uri, json=arguments, cookies=COOKIES)
        except ConnectionError:
            msg = "Cannot connect to the archive server"
            print(msg)
            if self.set_status:
                self.set_status(msg, error=True)
            if self.logger:
                self.logger.error(msg)
            return False
        except ValueError as e:
            msg = str(e)
            print(msg)
            if self.set_status:
                self.set_status(msg, error=True)
            if self.logger:
                self.logger.error(msg)

            return False

        for response in ret.json():
            if not response['result']:

                error_txt = (f'{response.get("id", "UNKNOWN")}: '
                             f'{response["error"]}')
                self.logger.error(error_txt)
                return False
            else:
                msg = (f"{fitsname} {card} successfully set to {value}")
                if self.logger:
                    self.logger.info(msg)
                return True


def readQA(fitsname):

    pagename = f'http://fits/summary/{fitsname}/'

    try:
        t = pd.read_html(pagename)[0]
        qa = t['QA↑↓'].values[0]
    except Exception:
        qa = 'Undefined'

    return qa.upper()

###############################################################################


def validate_raw(inp, accepted, keyw):
    if inp.lower() == 'any':
        return 'Any'
    elif inp.upper().startswith('UNK'):
        return 'UNKNOWN'
    else:
        for number in accepted:
            if str(number) in inp:
                return '{}-percentile'.format(number)

    error_txt = f'{keyw} does not accept {inp} as an input'
    valid = (tuple('{}-percentile'.format(x) for x in accepted)
             + ('Any', 'UNKNOWN'))
    raise NotInRangeError(error_txt, valid=valid)
