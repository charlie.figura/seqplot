import os, time, shutil
from datetime import datetime
from pytz import timezone

datapath = '/Users/cfigura/FITS/dhs'

if __name__ == "__main__":

    utcNow = datetime.now(timezone('UTC'))
    date = utcNow.strftime('%Y%m%d')
    counter = 0

    oldfilepath = '/Users/cfigura/FITS/dhs/N20230406S0001.fits'
    
    while True:
        counter += 1
        
        now = datetime.strftime(datetime.now(),"%H:%M:%S")
        fracsecs = datetime.strftime(datetime.now(),".%f")
        tenthsecs = str(round(float(fracsecs),1))[1:]
        timestamp = f"{now}{tenthsecs}"

        
        newfilename = f'N{date}S{counter:03d}.fits'
        shutil(oldfilepath,newfilepath)
        print(f'{timestamp}: {newfilepath}' written)
        

        time.sleep(2)
