from ginga.misc.Bunch import Bunch

"""
If Seqplot is included in plugins here, it will autostart and interfere with 
'normal' ginga operation for dragons.  I've commented it out, and it can be run
with the flag on execution
$ ginga --modules=Seqplot
"""

plugins = [
           #Bunch(module='Seqplot', workspace='dialogs', category='Analysis',
           #      ptype='global'),
          ]

def init_config(main):
    for spec in plugins:
        main.add_plugin_spec(spec)

def post_gui_config(ginga_shell):

    try:
        pl_obj = ginga_shell.gpmon.get_plugin("Seqplot")
        pl_obj.post_channel_initialisation()
        ginga_shell.ds.raise_tab("Seqplot")
    except:
        pass
    
